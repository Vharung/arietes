
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql = require('mysql');
var clients = {};
var con = mysql.createConnection({
    host: "localhost",
    user: "liberchronicles",
    password: "Arietes261118",
    database: "arietes"
});
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

http.listen(3000, function(){
  var d = new Date();
  console.log(d+'listening on *:3000');
});

io.on('connection', function(socket){
  //enregistrement d'une session
  socket.on('add-user', function(data){
  var d = new Date();

  console.log(d+'- id:' + data.idplayer + ' "'+ data.user+'" a rejoins la partie id:'+data.idpartie+',  nouvelle id:' + socket.id);
    clients[data.idplayer] = {
      "socket": socket.id,
      "nom": data.user,
      "idplayer": data.idplayer,
      "idpartie":data.idpartie
    };
  });


socket.on('disconnect', function(){
  var d = new Date();

  //on supprime la session des sessions en ligne
  for(var name in clients) {
      if(clients[name].socket === socket.id) {
        console.log(d+clients[name].nom +' a quitté la partie ' + clients[name].idpartie);
        delete clients[name];
        break;
      }
    }
  });

/***************************************************************************/
/**************************Reception infomartion****************************/
/***************************************************************************/

//reception tours
socket.on('tours', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' lance tours  message '+data.id+ 'pour idpartie:' + data.idgame );
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('tour', data.id);
      }
    }
});

//reception event lire et renvoie d'un event lecture
socket.on('tchat', function(data){
  var d = new Date();
  for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' ecrit dans le tchat pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('chat');
      }
    }
}); 

//reception des point d'equipe 
socket.on('point', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' modifie les points d equipe pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('pt', data.msg);
      }
    }
});

//reception des modification des dés
socket.on('des', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' modifie les dès pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('modifdes', data);
      }
    }
});

//reception des lanceur des dés
socket.on('lanceurd', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' lance les dès pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('lanceurds', data);
      }
    }
});

//reception lancer retour
socket.on('roll', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' a le resultat des dès pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('resultroll', data);
      }
    }
});

//reception message
socket.on('textuel', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' textuel des dès pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('textuels', data);
      }
    }
});

//reception mjvaleur
socket.on('mjvaleur', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' mets à jour valeurs pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('mjvaleurs', data);
      }
    }
});

//reception posture valeur
socket.on('posturecolonne', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' mets à jour posture pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('posturecolonnes', data);
      }
    }
});

//reception cacher
socket.on('cacher', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' fait un jet caché pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('cache');
      }
    }
});

//reception nom
socket.on('noms', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change de nom pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('nom', data);
      }
    }
});

//reception bar
socket.on('bar', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change ses bars pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('bars', data);
      }
    }
});

//reception bouclier
socket.on('boucl', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change son bouclier noir pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('boucls', data);
      }
    }
});

//reception bouclier
socket.on('bouclsec', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change son bouclier bleu pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('bouclsecs', data);
      }
    }
});

//reception docvis
socket.on('docvis', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' modif les docs pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('docviss');
      }
    }
});

//reception pnjvis
socket.on('pnjvis', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' modif les pnj pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('pnjviss');
      }
    }
});

//reception avant
socket.on('avant', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' a avantage pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('avants', data);
      }
    }
});

//reception etat
socket.on('etat', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change etat pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('etats', data);
      }
    }
});

//reception fiche
socket.on('fiche', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change fiche pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('fiches', data);
      }
    }
});

//reception edit
socket.on('edit', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change edit pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('edits', data);
      }
    }
});

//reception color
socket.on('color', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change color pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('colors', data);
      }
    }
});

//reception musique
socket.on('musique', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change musique pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('music', data);
      }
    }
});

//reception temp
socket.on('temp', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change temp pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('temps', data);
      }
    }
});

//reception Musique
socket.on('musicale', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' utilise musique idpartie:' + data.idgame+' idmusic'+data.idvideo+':'+data.action);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('musicales', data);
      }
    }
});
//mise a jour musique
socket.on('updatevid', function(data){
  var d = new Date();
  for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change de musique idpartie:' + data.idgame+' idmusic'+data.idvideo+':'+data.value);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('updatevids', data);
      }
    }
});

//reception plataddimg
socket.on('plataddimg', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change plataddimg pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('plataddimgs', data);
      }
    }
  });

//reception imgbibli
socket.on('imgbibli', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change imgbibli pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('imgbiblis', data);
      }
    }
  });

//reception plataddimg
socket.on('plateaunode', function(data){
  var d = new Date();
for(var name in clients) {
      if(clients[name].idplayer === data.idplayer) {
        console.log(d+clients[name].nom +' change plateaunode pour idpartie:' + data.idgame);
      }
      if(clients[name].idpartie === data.idgame ) {
        io.sockets.connected[clients[name].socket].emit('plateaunodes', data);
      }
    }
  });
});

