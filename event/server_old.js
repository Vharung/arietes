var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql = require('mysql');
var clients = {};
var con = mysql.createConnection({
    host: "localhost",
    user: "liber",
    password: "liber",
    database: "liber"
});


app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  console.log('a user connected');
  
  socket.on('disconnect', function(){
    console.log('user disconnected');
	//on supprime la session des sessions en ligne
	for(var name in clients) {
  		if(clients[name].socket === socket.id) {
  			delete clients[name];
  			break;
  		}
  	}
  });
  
  //enregistrement d'une session
  socket.on('add-user', function(data){
	console.log('Ajout joueur id : ' + data.idplayer + ' as ' + socket.id);
    clients[data.idplayer] = {
      "socket": socket.id
    };
  });
  
  //reception event lire et renvoie d'un event lecture
  socket.on('lire', function(msg){
    console.log('lecture ' + msg);
	 io.emit('lecture', msg);
  });

  //reception event lire et renvoie d'un event lecture
  socket.on('roll', function(msg){
    console.log('resultroll ' + msg);
    io.emit('resultroll', msg);
  });

  socket.on('tchat', function(){
    console.log('chat ');
    io.emit('chat');
  });
  socket.on('PNJs', function(){
    console.log('pnj');
    io.emit('pnj');
  });
  socket.on('DOCs', function(){
    console.log('doc');
    io.emit('doc');
  });
  //reception event maj Armure et renvoie d'un event Armure
socket.on('majArmure', function(data){
  console.log('majArmure  ' + data.idplayer + ' / ' + data.arm + ' / ' + data.armm + ' / ' + data.idgame);
	//envoi de Armure a l'ensemble des clients
	//io.emit('Armure', data);
	//envoi de Armure aux joueurs de la table.
	//recup dans results des joueurs de la partie.
  con.query(`SELECT id_player FROM roll_table where id_game =`+data.idgame, function (error, results, fields) {
    if (error) {
      console.log(error);
      return;
    }else {
			//pour chaque joueur (dest) de la partie
			results.forEach(function(dest) {
  			console.log(dest.id_player);
  			//si le joueur est inscrit dans le tableau clients donc connecté
  			if (typeof clients[dest.id_player] !== 'undefined'){
  				console.log(dest.id_player + ' envoi Armure');
  				io.sockets.connected[clients[dest.id_player].socket].emit('Armure', data);
  			} else {//si le client n'est pas connecté
  				console.log(dest.id_player + ' non connecté');
  			}
			});
    }
  });
});
  
  //reception event maj Destin et renvoie d'un event Destin
socket.on('majDestin', function(data){
  console.log('majDestin  ' + data.ptdestin + ' / ' + data.idgame);
	//envoi de Destin aux joueurs de la table.
	//recup dans results des joueurs de la partie.
	con.query(`SELECT id_player FROM roll_table where id_game =` + data.idgame, function (error, results, fields) {
                        if (error) {
                            console.log(error);
                            return;
                        }else {
							//pour chaque joueur (dest) de la partie
							results.forEach(function(dest) {
								console.log(dest.id_player);
								//si le joueur est inscrit dans le tableau clients donc connecté
								if (typeof clients[dest.id_player] !== 'undefined')
								{
									console.log(dest.id_player + ' envoi Destin');
									io.sockets.connected[clients[dest.id_player].socket].emit('Destin', data);
								}
								//si le client n'est pas connecté
								else
								{
									console.log(dest.id_player + ' non connecté');
								}
							});
                        }
                    });
  });
  
  //reception event maj Comp et renvoie d'un event Comp
  socket.on('majComp', function(data){
    console.log('majComp  ' + data.fiche + ' / ' + data.comp + ' / ' +data.valeur1_comp + ' / ' +data.valeur2_comp + ' / ' +data.desc_comp + ' / ' +data.cmd_comp + ' / ' +data.groupe + ' / ' +data.nom_comp + ' / ' + data.idgame);
	//envoi de Comp aux joueurs de la table.
	//recup dans results des joueurs de la partie.
	con.query(`SELECT id_player FROM roll_table where id_game =` + data.idgame, function (error, results, fields) {
                        if (error) {
                            console.log(error);
                            return;
                        }else {
							//pour chaque joueur (dest) de la partie
							results.forEach(function(dest) {
								console.log(dest.id_player);
								//si le joueur est inscrit dans le tableau clients donc connecté
								if (typeof clients[dest.id_player] !== 'undefined')
								{
									console.log(dest.id_player + ' envoi Destin');
									io.sockets.connected[clients[dest.id_player].socket].emit('Comp', data);
								}
								//si le client n'est pas connecté
								else
								{
									console.log(dest.id_player + ' non connecté');
								}
							});
                        }
                    });
  });
  
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
