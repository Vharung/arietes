<!-- viewmoduleavatar -->
<?php
$result = count($id);
for($i=0;$i<$result;$i++){
	$modifibar='';
	$bar='';
	foreach ($barfinal[$i] as $nombar=>$valuer) {
        if($valuer['max']>0){
            $total=$valuer['min']*100/$valuer['max'];
            if($total<=15){
                $newcolor='#8B0000';
            }else {
                $newcolor=$barcolor[$nombar];
            }
        }else {
           $newcolor=$barcolor[$nombar]; 
        }
		if($_SESSION['iduser']== $_SESSION['idmj'] || $_SESSION['iduser']==$id[$i]){            
			$modifibar.='<input id="'.$nombar.'max'.$id[$i].'" class="info_pv" type="text" value="'.$valuer['max'].'" style="color:'.$barcolor[$nombar].'" onchange="modifbar(\''.$nombar.'\','.$id[$i].')">
            <input id="'.$nombar.$id[$i].'" class="info_pv" type="text" value="'.$valuer['min'].'" style="color:'.$newcolor.'" onchange="modifbar(\''.$nombar.'\','.$id[$i].')">
            <input value="+" type="button" class="augmente" onclick="augmente(\''.$nombar.'\','.$id[$i].');"><input value="-" type="button" class="diminue" onclick="diminue(\''.$nombar.'\','.$id[$i].');">
            <div class="clear"></div>';
		}
		if($valuer['max']==0){
			$max=1;
		}else {
			$max=$valuer['max'];
		}
		$pourcent=$valuer['min']*100/$max;
        $pourcent=$pourcent*280/100;
        if($pourcent>280){$pourcent=280;}
        $bar.='<div id="bar'.$nombar.$id[$i].'" class="skills" style="height: 3px; width: '.$pourcent.'px; background-color:'.$newcolor.';margin-bottom:3px;"></div>';//valeur min et max
	}
	if(!isset($image[$i])){
		$images='img/image.jpg';
	}else{
		$images=$image[$i];
	}
	if(isset($couleurprincipale[$i])){
        $couleurprin=$couleurprincipale[$i];
    }else {
        $couleurprin='#4A90E2';
    }
	echo '<div id="joueur'.$id[$i].'" class="avatar" onmouseup="mouseButton(event,'.$id[$i].')">
		<div id="tour'.$id[$i].'" class="tourinfo" style="background:'.$couleurprin.'"></div>
		<div class="image" style="background:url(\''.$images.'\') no-repeat top center; background-size:cover;">
            <h1><input name="nom" class="nom" ';
            if($_SESSION['iduser']== $_SESSION['idmj'] || $_SESSION['iduser']==$id[$i]){
            	echo 'type="text" ';
            }else {
             	echo 'type="button" ';
            }
            echo 'value="'.$nom[$i].'" onchange="modifnom('.$id[$i].')"></h1>
            <div class="clear"></div>
            <span class="infoavatar">
                '.$modifibar.'
            </span>
            <div class="clear"></div>
            <div class="infoav">
                <span class="posture" style="background:url(\'img/posture-'.$posture[$i].'.png\'); background-size:cover;"></span>
                <span class="avantage"  style="background:url(\'img/avantage-'.$avantage[$i].'.png\'); background-size:cover;"></span>
                <span class="effet"  style="background:url(\'img/etat-'.$etat[$i].'.png\'); background-size:cover;"></span>
            </div>';
            if(!isset($bouclier[$i])){
            	$bouclierdisplay='none';
            }else {
            	$bouclierdisplay='';
            }
            if(!isset($boucliersec[$i])){
            	$boucliersecdisplay='none';
            }else {
            	$boucliersecdisplay='';
            }
            if($_SESSION['iduser']== $_SESSION['idmj'] || $_SESSION['iduser']==$id[$i]){
                $type='text';
            }else {
                $type='button';
            }
            echo '<div class="infoarmure">
                <input id="bouclier-'.$id[$i].'" name="armure" class="armure" type="'.$type.'" value="'.$bouclier[$i].'" style="display:'.$bouclierdisplay.'" onchange="modifbouclier('.$id[$i].')">
                <input id="boucliersec-'.$id[$i].'" name="armure_magique" class="armuremagique" type="'.$type.'" value="'.$boucliersec[$i].'" style="display:'.$boucliersecdisplay.'"  onchange="modifboucliersec('.$id[$i].')">
            </div>
            <div class="diceindic"></div>
        </div>	
	</div>
	<div class="clear"></div>
    <div class="bar">
       '.$bar.'
    </div>
    <div class="clear"></div>';
}
echo '<script src="js/moduleavatar.js" type="text/javascript"></script>';