<!-- viewmod -->
<?php  
class viewmod{
	private $_file;
	public $currentUrl;
	public function generate($module,$donnees){
            if (file_exists('views/viewmodule'.$module.'.php')) {
            // Rend les éléments du tableau $donnees accessibles dans la vue
            extract($donnees);
            // Démarrage de la temporisation de sortie
            ob_start();
            // Inclut le fichier vue
            // Son résultat est placé dans le tampon de sortie
            require 'views/viewmodule'.$module.'.php';
            // Arrêt de la temporisation et renvoi du tampon de sortie
            return ob_get_clean();
            }
            else {
                throw new Exception("Fichier '$fichier' introuvable");
            }
		
	}
}
