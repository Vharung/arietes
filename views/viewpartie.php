<!-- viewmodulepartie -->
<?php
$modulehelp='<div id="info-0" class="regle" style="display:none"><h1>Aide</h1>';
$collone=0;
$contentmenufiche="";
$moduleediteurdefiche="";
$moduleediteuractive=false;
$menutop="";
extract($data);

//--------------------------------------------------------------------------- module du compteur de point -----------------------------------------------------------------------------------\\

if ($modulepointdestin['enable']){
    $moduledestin = new ViewMod();
    $contentmoduledestin=$moduledestin->generate('point',$modulepointdestin['data']);
}else{
    $contentmoduledestin="";
}

//----------------------------------------------------------------------------- module du lanceur de dés -------------------------------------------------------------------------------------\\

if ($modulelanceur['enable']){
    $moduledes = new ViewMod();
    $contentmodulelanceur=$moduledes->generate('des',$modulelanceur['data']);
    $collone=1;
}else{
    $contentmodulelanceur="";
}

//------------------------------------------------------------------------------- module des postures ---------------------------------------------------------------------------------------\\

if ($moduleposture['enable']){
    $modulepos = new ViewMod();
    $contentmoduleposture=$modulepos->generate('posture',$moduleposture['data']);
    $collone=1;
}else{
    $contentmoduleposture="";
}

//-------------------------------------------------------------------------------- module des avatars ----------------------------------------------------------------------------------------\\

if ($moduleavatar['enable']){
    $moduleavatars = new ViewMod();
    $contentmoduleavatar=$moduleavatars->generate('avatar',$moduleavatar['data']);
    $moduleediteurdefiche.=$moduleavatars->generate('editavatar',$moduleavatar['data']);
    $moduleediteuractive=true;
    $collone=1;
}else{
    $contentmoduleavatar="";
}

//---------------------------------------------------------------------------------- module du tchat ------------------------------------------------------------------------------------------\\

if ($moduletchat['enable']){
    $modulechat = new ViewMod();
    $contentmoduletchat=$modulechat->generate('tchat',$moduletchat['data']);
    $contentmenufiche.='<li id="ouverturefiche" ><span class="agrandissement" onclick="chargerfiche(\'c\');"></span></li>';
    $modulehelp.='<h2>Tchat</h2>
    <p>Pour parler en MP appuyer sur le nom et taper ensuite votre message</p>
    <p>Pour faire un lancer de dés personnalisé faire /r suivit de la commande du dés</p>
    <p>Pour faire un dés caché faite /h puis la commande du dés</p>';
}else{
    $contentmoduletchat="";
}

//-------------------------------------------------------------------------------- module des note du mj ---------------------------------------------------------------------------------------\\

if ($modulenotemj['enable']){
    $modulenotes = new ViewMod();
    $contentmodulenote=$modulenotes->generate('note',$modulenotemj['data']);
    $contentmenufiche.='<li style="background: #ffffff" id="fiche_joueur0"><input type="button"  class="fiche" style="color:#000000" value="Note MJ" onclick="chargerfiche(0);"></li>';
}else{
    $contentmodulenote="";
}

//---------------------------------------------------------------------------------- module des fiches ------------------------------------------------------------------------------------------\\

if ($modulemenufiche['enable']){
    $modulemenufiches = new ViewMod();
    $contentmenufiche.=$modulemenufiches->generate('menufiche',$modulemenufiche['data']);
}else{
    $contentmenufiche.="";
}
if($modulecontentfiche['enable']){
    $modulecontentfiches = new ViewMod();
    $contentfiche=$modulecontentfiches->generate('fiche',$modulecontentfiche['data']);
    $moduleediteurdefiche.=$modulemenufiches->generate('edit',$modulecontentfiche['data']);
    $moduleediteuractive=true;
}else{
    $contentfiche="";
} 


//---------------------------------------------------------------------------------- module de régle --------------------------------------------------------------------------------------------\\

if ($moduleregle['enable']){
    $moduleregles = new ViewMod();
    $contentmoduleregle=$moduleregles->generate('regle',$moduleregle['data']);
    $menutop.='<a href="#" id="regle" title="Regle du jeu" onclick="infodroite(1);">Regle du jeu</a>';
}else{
    $contentmoduleregle="";
}

//----------------------------------------------------------------------------------- module de doc ----------------------------------------------------------------------------------------------\\

if ($moduledoc['enable']){
    $moduledocs = new ViewMod();
    $contentmoduledoc=$moduledocs->generate('doc',$moduledoc['data']);
    $menutop.='<a href="#" id="doc" title="Documents" onclick="infodroite(2);">Gestion des documents</a>';
}else{
    $contentmoduledoc="";
}

//----------------------------------------------------------------------------------- module de pnj ----------------------------------------------------------------------------------------------\\

if ($modulepnj['enable']){
    $modulepnjs = new ViewMod();
    $contentmodulepnj=$modulepnjs->generate('pnj',$modulepnj['data']);
    $menutop.='<a href="#" id="pnj" title="PNJ" onclick="infodroite(3);">Gestion des PNJ</a>';
}else{
    $contentmodulepnj="";
}

//---------------------------------------------------------------------------------- module de musique ---------------------------------------------------------------------------------------------\\

if ($modulemusic['enable']){
    $modulemusics = new ViewMod();
    $contentmodulemusic=$modulemusics->generate('music',$modulemusic['data']);
    $menutop.='<a href="#" id="musique" title="Musique" onclick="infodroite(4);">Gestion des Musiques</a>';
}else{
    $contentmodulemusic="";
}

//---------------------------------------------------------------------------------- module de plateau ---------------------------------------------------------------------------------------------\\
$menutop.='<a href="#" id="plateau" title="Plateau" onclick="infodroite(5);">Plateau</a>';
$plateau = new ViewMod();
$modulemenuplateau=$plateau->generate('menuplateau',$moduleinfoplateau['data']);
$contentplateau=$plateau->generate('plateau',$moduleinfoplateau['data']);

//echo $moduleplateau['contenu'];
//$contentplateau=$plateau->generate('plateau',$moduleplateau['contenu']);

//----------------------------------------------------------------------------------- module de aide -----------------------------------------------------------------------------------------------\\

$modulehelp.='</div>';
if($moduleediteuractive==true){
    $moduleediteurdefiche.='<div class="clear"></div>';  
    /*<input value="Valider" class="bouton_save" type="button" onclick="fermermodif('.$_SESSION['iduser'].');"> <input value="Importer" class="bouton_save" type="button" onclick="importation('.$_SESSION['iduser'].')"> <input value="Exporter" class="bouton_save" type="button" onclick="exportation('.$_SESSION['iduser'].')">';*/
}

echo '
<script type="text/javascript">
    var idplayer="'.$_SESSION['iduser'].'";
    var idmj="'.$_SESSION['idmj'].'";
    var idplateau=0;
    var iddelapartie="'.$_SESSION['idpartie'].'";
    var user="'.$_SESSION['login_user'].'";
</script>
<script src="'.$_SESSION['loc'].'socket.io/socket.io.js"></script>
<!--script src="socket.io/socket.io.js"></script-->
<script src="js/synchronisation.js" type="text/javascript"></script>
<script src="js/refresh.js" type="text/javascript"></script>
   <div id="menutop">
        <h1 class="title"><img src="img/icone_partie.png">Arietes</h1>'.$contentmoduledestin.'
        <div id="menu">                
            <a href="index.php?url=liste" id="creer_fiche" title="Retour au liste fiche">Fiche</a>
            '.$menutop.'
            <a href="#" id="help" title="Aide"  onclick="infodroite(0);">Aide</a>
            <span class="nomlog">'.$_SESSION['login_user'].'</span> 
            <a href="index.php?url=logout"><span><img src="img/logout.png" class="logout"></span></a>
        </div>
</div>'.$contentplateau;
if($collone==1){
echo'
<div id="supercollone">
    <div id="collone">
        <div id="roll">'.$contentmodulelanceur.'
        	<div class="clear"></div>
        	'.$contentmoduleposture.'
            <div class="clear"></div>
        </div>
        <div id="contentavatar">
            <div id="avatar">'
            .$contentmoduleavatar.'
            </div>
        </div>
    </div>
</div>';
}
echo'
<div id="fiches">
    <ul class="menu-fiche">'.$contentmenufiche.'
    </ul>
    '.$contentmodulenote.'
    '.$contentmoduletchat.'
    '.$contentfiche.'
</div>
<div id="infocollone">
    <input type="button" id="reduire" value="Agrandir" onclick="agrandirinfo()">
    <div id="infocollone2">
    '.$modulehelp.'
    '.$contentmodulepnj.'
    '.$contentmoduledoc.'
    '.$contentmoduleregle.'
    '.$contentmodulemusic.'
    '.$modulemenuplateau.'
    </div>
</div>
<div id="editfiche">
    '.$moduleediteurdefiche.'
</div>
</div>
<div id="exportation"></div>';

