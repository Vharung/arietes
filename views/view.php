<?php  
class View{
	private $_file;
	public $currentUrl;
	public function generate($page,$erreur,$data){
		$protocol = 'https';//strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
		$host     = $_SERVER['HTTP_HOST'];
		$currentUrl = $protocol.'://'. $host.'/';
		$lien=$currentUrl.'index.php?url=';
		require ('views/viewheader.php');
		$page = strtolower($page);
		require ('views/view'.$page.'.php');
		require ('views/viewfooter.php');
	}
}