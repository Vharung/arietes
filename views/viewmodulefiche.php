<!-- viewmodulefiche -->
<?php
foreach ($fiche as $value => $cle) {
	if($_SESSION['iduser']==$value || $_SESSION['iduser']==$_SESSION['idmj']){
		echo '<article id="fiche-'.$value.'" class="id_fiche" style="display: block">';
		for($x=1;$x<5;$x++){
			echo '<div class="fichecolonne">';
			foreach ($cle['colone'.$x] as $key ) {
				if($key['type']=='stat'){
					require('views/viewmodulefichestat.php');
					echo $table;
				}else if($key['type']=='stat_sec'){
					require('views/viewmodulefichestat.php');
					echo $table;
				}else if($key['type']=='text'){
					require('views/viewmodulefichetext.php');
					echo $text;
				}else if($key['type']=='competence'){
					require('views/viewmodulefichecomp.php');
					echo $comp;
				}else if($key['type']=='inventaire'){
					require('views/viewmoduleficheinve.php');
					echo $inve;
				}else if($key['type']=='encombrement'){
					require('views/viewmoduleficheenco.php');
					echo $enco;
				}else if($key['type']=='connaissance'){
					require('views/viewmoduleficheconn.php');
					echo $conn;
				}else if($key['type']=='posture'){
					require('views/viewmodulefichepost.php');
					echo $post;
				}else if($key['type']=='avantage'){
					require('views/viewmoduleficheavan.php');
					echo $avan;
				}else if($key['type']=='etat'){
					require('views/viewmoduleficheetat.php');
					echo $etats;
				}else if($key['type']=='editeur'){
					require('views/viewmoduleficheedit.php');
					echo $edit;
				}
				if($x==4){
					echo '<div class="clear"></div>
					<input value="Modifier" class="bouton_save" type="button" onclick="ouvremodif('.$value.');">
                <div class="clear"></div>';
				}
				
			}
			echo '</div>';
		}
		echo '</article>';
	}
}
