<!-- viewmoduleeditavatar -->
<?php
$result = count($id);
for($i=0;$i<$result;$i++){
    if($_SESSION['iduser']== $_SESSION['idmj'] || $_SESSION['iduser']==$id[$i]){
        if(!isset($image[$i])){
            $images='';
            $imageurl='img/image.jpg';
        }else{
            $images=$image[$i];
            $imageurl=$image[$i];
        }
        echo '<div id="joueuredit'.$id[$i].'" class="avatar_edit" style="display:none;">
            <div class="block-edition"><h2>Edition de ma fiche</h2><h1 style="text-transform:capitalize;font-family: \'MedievalSharp\', cursive;"><input type="text" value="'.$nom[$i].'" class="sans" id="nomedit'.$id[$i].'" onchange="modifeditnom('.$id[$i].')"></h1></div>
            <div class="block-edition"></div>
            <div class="block-edition">
                <h2>Image</h2>
                <p><input type="text" value="'.$images.'" class="sans" style="width:100%;font-style: italic;" placeholder="url image" id="imgedit'.$id[$i].'" onchange="modifeditimage('.$id[$i].')"></p>
            </div>
            <div class="block-edition">
                <div class="image" style="background:url(\''.$imageurl.'\') no-repeat top center; background-size:cover;">
                </div>
            </div>
        </div>';
    }
}