<!-- viewmodulemusicplaylist -->
<?php
$listmusics='<optgroup label="YouTube">';
    for($i = 0; $i < count($iduser); $i++){
        if($iduser[$i]==$_SESSION['idmj'] && isset($url[$i]) && $scr[$i]=='youtube'){
            $listmusics.= '<option value="iframe|https://www.youtube.com/embed/'.$url[$i].'?enablejsapi=1" type="youtube" url="'.$url[$i].'" idmusic="'.$idmusic[$i].'">'.$nommusique[$i].'</option>';
        }
    }
$listmusics.= '<!--option value="iframe|https://www.youtube.com/embed/pOm7g9Awm4I?enablejsapi=1" type="youtube" url="pOm7g9Awm4I" selected>D&D Ambience</option>
    <option value="iframe|https://www.youtube.com/embed/eRsGyueVLvQ?enablejsapi=1" type="youtube" url="eRsGyueVLvQ">Sintel</option>
    <option value="iframe|https://www.youtube.com/embed/YE7VzlLtp-4?list=PL6B3937A5D230E335&amp;enablejsapi=1">Playlist</option-->
</optgroup>
<optgroup label="Vimeo">';
    for($i = 0; $i < count($iduser); $i++){
        if($iduser[$i]==$_SESSION['idmj'] && isset($url[$i]) && $scr[$i]=='vimeo'){
            $listmusics.= '<option value="iframe|https://player.vimeo.com/video/'.$url[$i].'?api=1" type="vimeo" url="'.$url[$i].'" idmusic="'.$idmusic[$i].'">'.$nommusique[$i].'</option>';
        }
    }
$listmusics.= '<option value="iframe|https://player.vimeo.com/video/1084537?api=1">Big Buck Bunny</option>
</optgroup>
<optgroup label="SoundCloud">';
    for($i = 0; $i < count($iduser); $i++){
        if($iduser[$i]==$_SESSION['idmj'] && isset($url[$i]) && $scr[$i]=='soundCloud'){
            $listmusics.= '<option value="iframe|https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/'.$url[$i].'&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"" type="soundCloud" url="'.$url[$i].'"  idmusic="'.$idmusic[$i].'">'.$nommusique[$i].'</option>';
        }
    }
$listmusics.= '
    <option value="iframe|https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/113795767&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true">Theophany - Molgera (2013)</option>
</optgroup>
<optgroup label="Twitch">';
    for($i = 0; $i < count($iduser); $i++){
        if($iduser[$i]==$_SESSION['idmj'] && isset($url[$i]) && $scr[$i]=='twitch'){
            $listmusics.= '<option value="iframe|https://player.twitch.tv/?allowfullscreen&video='.$url[$i].'" type="twitch" url="'.$url[$i].'" idmusic="'.$idmusic[$i].'">'.$nommusique[$i].'</option>';
        }
    }
$listmusics.= '
    <option value="iframe|https://player.twitch.tv/?allowfullscreen&channel=geekandsundry">Geek and Sundry</option>
    <option value="iframe|https://player.twitch.tv/?allowfullscreen&video=v92780016">Felicia Day\'s Monologue Montage!!</option>
</optgroup>
<optgroup label="Dailymotion">';
    for($i = 0; $i < count($iduser); $i++){
        if($iduser[$i]==$_SESSION['idmj'] && isset($url[$i]) && $scr[$i]=='dailymotion'){
            $listmusics.= '<option value="iframe|https://www.dailymotion.com/embed/video/'.$url[$i].'?api=postMessage" type="dailymotion" url="'.$url[$i].'" idmusic="'.$idmusic[$i].'">'.$nommusique[$i].'</option>';
        }
    }
$listmusics.= '
    <option value="iframe|https://www.dailymotion.com/embed/video/xyh8cw?api=postMessage">EP Daily &amp; Dailymotion @ GDC 2013: Star Trek: The Video Game</option>
</optgroup>
<optgroup label="HTML 5">';
    for($i = 0; $i < count($iduser); $i++){
        if($iduser[$i]==$_SESSION['idmj'] && isset($url[$i]) && $scr[$i]=='HTML5'){
            $listmusics.= '<option value="iframe|'.$url[$i].'" type="html5" url="'.$url[$i].'" idmusic="'.$idmusic[$i].'">'.$nommusique[$i].'</option>';
        }
    }
$listmusics.= '
    <option value="video|https://upload.wikimedia.org/wikipedia/commons/9/96/Curiosity%27s_Seven_Minutes_of_Terror.ogv">Curiosity\'s Seven Minutes of Terror</option>
    <option value="audio|https://people.xiph.org/~giles/2012/opus/ehren-paper_lights-96.opus">Paper Lights by Ehren Starks</option>
</optgroup>
<optgroup label="Errors">
    <option value="iframe|https://www.youtube.com/embed/mqFNUMhmNCI?enablejsapi=1">YouTube: Private</option>
    <option value="iframe|https://www.youtube.com/embed/00000000000?enablejsapi=1">YouTube: Not Found</option>
    <option value="video|https://example.com/">HTML5: Unsupported</option>
    <option value="iframe|http://example.com/">IFRAME: Unsupported</option>
</optgroup>';