<!-- viewmoduledoc -->
<?php
echo '<div id="info-2" class="regle" style="display:none"><h1>Document</h1>';
$result = count($titredoc);
for($i=0;$i<$result;$i++){
    $joueurvue=(explode("|",$visible[$i]));
    $vue=false;
    if($joueurvue[0]==null){
        $opacity='0.5';
    }else{
        $opacity='1';
    }
    if($idcreateur[$i]!=$_SESSION['idmj']){
        $opacity='1';
    }
    for($j=0;$j<count($joueurvue);$j++){
        if($_SESSION['iduser']==$joueurvue[$j]){
            $vue=true;
        }
    }
    $listeplayer='';
    $first=0;
    foreach ($listejoueur as $key => $value) {
        if($value!=$_SESSION['idmj']){
            if($first>0){
                 $listeplayer.="|";
            }
            $listeplayer.=$value;
            $first++;
        }
    }
    if($_SESSION['iduser']==$_SESSION['idmj']||$_SESSION['iduser']==$idcreateur[$i]||$vue==true){
        echo '<h2 class="titredoc" style="text-transform:capitalize">';
        if($_SESSION['iduser']==$_SESSION['idmj']||$_SESSION['iduser']==$idcreateur[$i]){
            echo '<input type="text" class="sans" value="'.$titredoc[$i].'" id="titredoc-'.$id[$i].'" onchange="changecontenu('.$id[$i].')"><span class="suprimer" onclick="validsupprdoc('.$id[$i].')">X</span>
            <span style="background:url(\'img/visible.png\'); background-size:cover;display:block; width:20px;height:20px; float:right; margin-right:10px; opacity:'.$opacity.'" onclick="visibledoc(\''.$visible[$i].'\',\''.$listeplayer.'\','.$id[$i].')"></span>';
        }else {
            echo $titredoc[$i];
        }
        echo '</h2>';
        if($_SESSION['iduser']==$_SESSION['idmj']){
            echo '<span class="infogenerale">Info générale :</span>';
        }
        if($_SESSION['iduser']==$_SESSION['idmj']||$_SESSION['iduser']==$idcreateur[$i]){
        	echo '<textarea id="doccontenu-'.$id[$i].'" class="doc" onchange="changecontenu('.$id[$i].')">'.$contenudoc[$i].'</textarea>';
        }else {
        	echo '<textarea class="doc" readonly="readonly">'.$contenudoc[$i].'</textarea>';
        }
       
        if($_SESSION['iduser']==$_SESSION['idmj']){
            echo '<span class="infogenerale">Vu que par le MJ :</span><textarea id="doccontenumj-'.$id[$i].'"  class="doc" onchange="changecontenu('.$id[$i].')">'.$contenumjdoc[$i].'</textarea>';
        }
    }
}

echo '<input type="button" class="bouton_save" value="Nouveau" onclick="nouveaudoc()">
<script src="js/moduledoc.js" type="text/javascript"></script></div>';
