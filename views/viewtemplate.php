<!-- viewtemplate -->
<?php //sql-id-type-colone
$listetemplate='';
$nomtemplate='';
if(isset($_GET['template'])){
    $idtemplate=$_GET['template'];
}else{
    $idtemplate=0;
}
foreach($data['listetemplate'] as $x => $x_value) {
    $id_template=$x;
    $name_template=$x_value;
    if($id_template==$data['idtemplate']){
        $nomtemplate=$name_template;
    }
    $listetemplate.= "<option nom='".$name_template."' value='".$id_template."'>".$name_template."</option>";
}

//--------------------------------------------------------------------------- module du compteur de point -----------------------------------------------------------------------------------\\

$checkbox_compteur=null; $id_point=0; $name_point=null; $nb_point=null;
if ($data['modulepointdestin']['enable']){
    $checkbox_compteur='checked="checked"';
    $id_point=$data['modulepointdestin']['value']['id'];
    $name_point=$data['modulepointdestin']['value']['name'];
    $nb_point=$data['modulepointdestin']['value']['valeur'];
}

//----------------------------------------------------------------------------- module du lanceur de dés -------------------------------------------------------------------------------------\\

$checkbox_lanceur=null;$valeurdesmax=null;$valeurreussite=null;$valeurcritique=null;$valeurbonusoffensif=null;$valeurmalusoffensif=null;$default_des=null;
$d6=null;$d8=null;$d10=null;$d12=null;$d20=null;$d100=null;$dperso=null;$d=null;
if ($data['modulelanceur']['enable']){
    $checkbox_lanceur='checked="checked"';
    $valeurdesmax=$data['modulelanceur']['valeurdesmax'];
    $default_des=$data['modulelanceur']['default_des'];
    if($valeurdesmax==6){
         $d6='checked';
    }else if($valeurdesmax==8){
         $d8='checked';
    }else if($valeurdesmax==10){
         $d10='checked';
    }else if($valeurdesmax==12){
         $d12='checked';
    }else if($valeurdesmax==20){
         $d20='checked';
    }else if($valeurdesmax==100){
         $d100='checked';
    }else {
        $d='checked';
         $dperso=$default_des;
    }    
    $valeurreussite=$data['modulelanceur']['valeurreussite'];
    $valeurcritique=$data['modulelanceur']['valeurcritique'];
    $valeurbonusoffensif=$data['modulelanceur']['valeurbonusoffensif'];
    $valeurmalusoffensif=$data['modulelanceur']['valeurmalusoffensif'];
}

//-------------------------------------------------------------------------------- module des avatars ----------------------------------------------------------------------------------------\\

$checkbox_avatar=null;$checkbox_bouc=null;$checkbox_boucsec=null;$barliste=null;$checkbox_bar=null;
if ($data['moduleavatar']['enable']){
    $checkbox_avatar='checked="checked"';
}
if ($data['modulebouclier']['enable']){
    $checkbox_bouc='checked="checked"';
}
if ($data['moduleboucliersec']['enable']){
    $checkbox_boucsec='checked="checked"';
}
$ligne=1;
foreach ($data['bar'] as $key => $value) {
    $barliste.='<div id="ligne'.$ligne.'"><div class="option_2colonne">
        <span>Nom : </span><input type="text" name="bar-name" placeholder="nom de la bar" value="'.$value['name'].'">
    </div>
    <div class="option_2colonne">
        <span>Couleur : </span> <span class="supprligne" onclick="supprligne('.$ligne.')">X</span><input type="color" name="bar-color" value="'.$value['color'].'"> 
    </div><div class="clear"></div></div>';
    $ligne++;
}
if($barliste){
    $checkbox_bar='checked="checked"';
}

//---------------------------------------------------------------------------------- module du tchat ------------------------------------------------------------------------------------------\\

$checkbox_tchat=null;
if ($data['moduletchat']['enable']){
    $checkbox_tchat='checked="checked"';
}

//-------------------------------------------------------------------------------- module des note du mj ---------------------------------------------------------------------------------------\\

$checkbox_mj=null;$nonnote=null;
for($i=0; $i < 3 ; $i++) { $checkbox_note[$i]=null;$nonnote[$i]=null; }
if ($data['modulenotemj']['enable']){
    $checkbox_mj='checked="checked"';
    foreach ($data['modulenotemj']['nomnotemj'] as $key => $value) {
        $checkbox_note[$key]='checked="checked"';
        $nonnote[$key]=$value;
    }
}

//---------------------------------------------------------------------------------- module des fiches ------------------------------------------------------------------------------------------\\

$checkbox_fiche=null;$checkbox_stat=null;$checkbox_stat_sec=null;$checkbox_enco=null;$checkbox_post=null;$checkbox_avan=null;$listemoduletpl=null;$nomstat=null;$nomstat_sec=null;
$nomenco=null;$nometat=null;$nompost=null;$nomavan=null;$divstats=null;$divstats_sec=null;$checkbox_etat=null;$checkbox_module=null;$checkbox_iframe=null;
for($i=0;$i<5;$i++){ $colenco[$i]=null; $colpost[$i]=null; $colavan[$i]=null; $coletat[$i]=null;$colsta[$i]=null;$colsta_sec[$i]=null; }
for($i=0;$i<10;$i++){$unst[$i]=null;$unity[$i]=null;$unen[$i]=null;$unpo[$i]=null;$unav[$i]=null;$unet[$i]=null;$unst_sec[$i]=null;}
if($data['modulemenufiche']['enable']){
    $checkbox_fiche='checked';
    if($data['modulemenufiche']['stat']){
        $checkbox_stat='checked="checked"'; 
        $nomstat=$data['modulemenufiche']['stat']['name'];
        $nb=substr($data['modulemenufiche']['stat']['block'], 0, 1) ;
        $colsta[$nb]='selected';
        $nb=substr($data['modulemenufiche']['stat']['block'], 1, 2);
        $unst[$nb]='selected';
        $stats=$data['modulemenufiche']['stat']['nombre'];
        $stats=explode('|', $stats);
        for($i=0;$i<count($stats);$i++){
            $divstats.='<div id="ligne'.$ligne.'" class="option_3colonne">Nom : <input class="template-stats-text" type="text" name="stats-name" value="'.$stats[$i].'"><span class="supprligne" onclick="supprligne('.$ligne.')">X</span></div>';
            $ligne++;
        }
    }
    if($data['modulemenufiche']['stat_sec']){
        $checkbox_stat_sec='checked="checked"'; 
        $nomstat_sec=$data['modulemenufiche']['stat_sec']['name'];
        $nb=substr($data['modulemenufiche']['stat_sec']['block'], 0, 1) ;
        $colsta_sec[$nb]='selected';
        $nb=substr($data['modulemenufiche']['stat_sec']['block'], 1, 2);
        $unst_sec[$nb]='selected';
        $stats_sec=$data['modulemenufiche']['stat_sec']['nombre'];
        $stats_sec=explode('|', $stats_sec);
        for($i=0;$i<count($stats_sec);$i++){
            $divstats_sec.='<div id="ligne'.$ligne.'" class="option_3colonne">Nom : <input class="template-stats-text" type="text" name="stats-name_sec" value="'.$stats_sec[$i].'"><span class="supprligne" onclick="supprligne('.$ligne.')">X</span></div>';
            $ligne++;
        }
    }
    if($data['modulemenufiche']['encombrement']){
        $checkbox_enco='checked="checked"';
        $nb= substr($data['modulemenufiche']['encombrement']['block'], 0, 1) ;
        $colenco[$nb]='selected';
        $nb=substr($data['modulemenufiche']['encombrement']['block'], 1, 2) ;
        $unen[$nb]='selected';
        $nomenco=$data['modulemenufiche']['encombrement']['name'];
        
    }
    if($data['modulemenufiche']['posture']){
        $checkbox_post='checked="checked"';
        $nb= substr($data['modulemenufiche']['posture']['block'], 0, 1) ;
        $colpost[$nb]='selected';
        $nb=substr($data['modulemenufiche']['posture']['block'], 1, 2) ;
        $unpo[$nb]='selected';
        $nompost=$data['modulemenufiche']['posture']['name'];
        
    }
    if($data['modulemenufiche']['avantage']){
        $checkbox_avan='checked="checked"';
        $nb= substr($data['modulemenufiche']['avantage']['block'], 0, 1) ;
        $colavan[$nb]='selected';
        $nb=substr($data['modulemenufiche']['avantage']['block'], 1, 2) ;
        $unav[$nb]='selected';
        $nomavan=$data['modulemenufiche']['avantage']['name'];
       
    }
    if($data['modulemenufiche']['etat']){
        $checkbox_etat='checked="checked"';
        $nb= substr($data['modulemenufiche']['etat']['block'], 0, 1) ;
        $coletat[$nb]='selected';
        $nb=substr($data['modulemenufiche']['etat']['block'], 1, 2) ;
        $unet[$nb]='selected';
        $nometat=$data['modulemenufiche']['etat']['name'];
        
    }
    foreach ($data['modulemenufiche']['module'] as $key => $value) { 
        $checkbox_module='checked="checked"';
        for($i=1;$i<9;$i++){
           $opt[$i]=null; 
        }
        if($value['type']=='text'){ $opt[1]='selected'; }
        if($value['type']=='comp'){ $opt[2]='selected'; }
        if($value['type']=='conn'){ $opt[3]='selected'; }
        if($value['type']=='inve'){ $opt[4]='selected'; }
        if($value['block']>9 && $value['block']<20){ $opt[5]='selected'; }
        if($value['block']>19 && $value['block']<30){ $opt[6]='selected'; }
        if($value['block']>29 && $value['block']<40){ $opt[7]='selected'; }
        if($value['block']>39 &&$value['block']<50){ $opt[8]='selected'; }
        $nb= substr($value['block'], 1, 2) ;
        $unity[$nb]='selected';
        $listemoduletpl.='<div id="ligne'.$ligne.'">
        <div class="option_3colonne"><span>Nom : </span><input class="name-12-13-14-16" type="text" value="'.$value['name'].'" size="10"></div>
        <div class="option_3colonne">
            <span> Type : </span><select class="type-12-13-14-16">
                <option '.$opt[1].' value="12">Text</option>
                <option '.$opt[2].' value="13">Comp.</option>
                <option '.$opt[3].' value="16">Liste</option>
                <option '.$opt[4].' value="14">Inven.</option>
            </select></div>
        <div class="option_3colonne"><span> Col. : </span>
        <select class="colonne-12-13-14-16">
            <option '.$opt[5].'>1</option>
            <option '.$opt[6].'>2</option>
            <option '.$opt[7].'>3</option>
            <option '.$opt[8].'>4</option>
        </select>
        <select class="position-12-13-14-16">
            <option '.$unity[0].'>0</option>
            <option '.$unity[1].'>1</option>
            <option '.$unity[2].'>2</option>
            <option '.$unity[3].'>3</option>
            <option '.$unity[4].'>4</option>
            <option '.$unity[5].'>5</option>
            <option '.$unity[6].'>6</option>
            <option '.$unity[7].'>7</option>
            <option '.$unity[8].'>8</option>
            <option '.$unity[9].'>9</option>
        </select><span class="supprligne" onclick="supprligne('.$ligne.')">X</span>
        </div></div>';
        $ligne++;
    }
}

//---------------------------------------------------------------------------------- module de régle --------------------------------------------------------------------------------------------\\

$checkbox_regle=null;$textregle=null;
if ($data['moduleregle']['enable']){
    $checkbox_regle='checked="checked"';
    $textregle=$data['moduleregle']['regle'];
}

//----------------------------------------------------------------------------------- module de doc ----------------------------------------------------------------------------------------------\\
                
$checkbox_doc=null;
if ($data['moduledoc']['enable']){
    $checkbox_doc='checked="checked"';
}               

//----------------------------------------------------------------------------------- module de pnj ----------------------------------------------------------------------------------------------\\

$checkbox_pnj=null;
if ($data['modulepnj']['enable']){
    $checkbox_pnj='checked="checked"';
}              

//---------------------------------------------------------------------------------- module de musique ---------------------------------------------------------------------------------------------\\

$checkbox_mus=null;
if ($data['modulemusic']['enable']){
    $checkbox_mus='checked="checked"';
}          

//----------------------------------------------------------------------------------------- Viewer --------------------------------------------------------------------------------------------------\\


echo '<script src="'.$currentUrl .'js/template.js" type="text/javascript"></script>
    <h1 id="titre"><img src="'.$currentUrl.'img/titre.png">Arietes</h1>
    <div id="log">
        <h1>Bienvenue '.$_SESSION['login_user'].' <a href="'.$lien.'liste"><span><img src="'.$currentUrl.'img/rafraichir.png" title="rafraichir"></span></a> <a href = "'.$lien.'logout"><span><img src="'.$currentUrl.'img/logout.png"></span></a></h1><br>
        <a href="'.$lien.'liste" class="newpartie" title="Nouvelle partie">Retour liste</a>
    </div>
    

    <div id="template">
        <h2 id="titreh2">Template personnalisée</h2>
        <div id="infotemplate">
            <input id="name_template" type="text" placeholder="nom de la template" value="'.$nomtemplate.'">
            <select id="mestpl" onchange="chargetemplate()">
                <option value="0">mes templates</option>'.
            $listetemplate.
            '<option  value="0">Nouvelle template</option>
            </select>             
        </div>
        <div class="colonne">
            
            <div class="template-div">
                <input type="checkbox" id="type-2" name="checkbox-dice" '.$checkbox_lanceur.' onchange="moduledes()"> <label class="orange">Module dés</label>
                <div class="option">
                    <input type="radio" name="desname" value="1d6" data-valeur="" '.$d6.'> <label>D6</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="desname" value="1d8" '.$d8.'> <label>D8</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="desname" value="1d10" '.$d10.'> <label>D10</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="desname" value="1d12" '.$d12.'> <label>D12</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="desname" value="1d20" '.$d20.'> <label>D20</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="desname" value="1d100+1d10" '.$d100.'> <label>D100</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="desname" value="perso" '.$d.'> <label><input type="text" id="desperso" placeholder="Dés perso" size="10" value="'.$dperso.'"></label>
                    <div class="seperateur"></div>
                    <div class="option_2colonne">
                        <span>Reussite critique : </span><input type="text" id="desreussite" size="2" placeholder="nb" value="'.$valeurreussite.'">
                    </div>
                    <div class="option_2colonne">
                        <span>Echec critique : </span><input type="text" id="descritique" size="2" placeholder="nb" value="'.$valeurcritique.'">
                    </div>
                    <div class="option_2colonne">
                        <span>Offensif Reussite critique : </span><input type="text" id="offreussite" size="2" placeholder="nb" value="'.$valeurbonusoffensif.'">
                    </div>
                    <div class="option_2colonne">
                        <span>Offensif Echec critique : </span><input type="text" id="offcritique" size="2" placeholder="nb" value="'.$valeurmalusoffensif.'">
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="template-div">
                <input type="checkbox" id="type-4" '.$checkbox_avatar.' onchange="avatr()"> <label class="orange">Avatar</label>
                <div class="option">
                    <input type="checkbox" '.$checkbox_bar.' id="type-7" onchange="barav()"> <label>Bar</label><input type="button" onclick="addbar()" value="+">
                    <div class="div-bar">'.$barliste.'</div>
                    <div class="seperateur"></div>
                    <div class="option_2colonne">
                        <input type="checkbox" '.$checkbox_bouc.' id="type-5" onchange="afbc()"> <label>Bouclier</label>
                    </div>
                    <div class="option_2colonne">
                        <input type="checkbox" '.$checkbox_boucsec.' id="type-6" onchange="afbcs()"> <label>Bouclier secondaire</label>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="template-div">
                <span class="orange" style="width:100%;display:block">Autre module</span>
                <div class="option_4colonne">
                    <input type="checkbox" '.$checkbox_tchat.' id="type-8" onchange="chataf()"> <label>Chat</label>
                </div>
                <div class="option_4colonne">
                    <input type="checkbox" '.$checkbox_pnj.' id="type-52" onchange="pnjaf()"> <label>PNJ</label>
                </div>
                <div class="option_4colonne">
                    <input type="checkbox" '.$checkbox_doc.' id="type-51" onchange="docaf()"> <label>Doc</label>
                </div>
                <div class="option_4colonne">
                    <input type="checkbox" '.$checkbox_mus.' id="type-53" onchange="musaf()"> <label>Musique</label>
                </div>
                <div class="seperateur"></div>
                <input type="checkbox" style="margin-left: 1%;" '.$checkbox_regle.'  onchange="regaf()"> <label>Règles du jeu</label>
                <div class="option">
                    <textarea class="template-textarea-regle">'.$textregle.'</textarea>
                </div>
                <div class="clear"></div>
             </div>
        </div>
        <div class="colonne">
            <div class="template-div">
                <input type="checkbox" id="type-1" '.$checkbox_compteur.' onchange="compt()"> <label class="orange">Compteur de points</label> 
                <div class="option">
                    <div class="option_2colonne">
                        <span>Nom : </span><input type="text" id="name-1" placeholder="nom des points" value="'.$name_point.'">
                    </div>
                    <div class="option_2colonne">
                        <span>Point max : </span><input type="text" id="valeur-1" size="2" placeholder="nb" value="'.$nb_point.'">
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="template-div">
                <input type="checkbox" '.$checkbox_mj.' id="type-9" onchange="noteaf()"> <label class="orange">Note MJ</label>
                <div class="option">
                    <div class="option_4colonne">
                         <input type="checkbox" '.$checkbox_note[0].' id="type-9-1"> <label><input type="text" placeholder="Scénario" id="valeur-9-1" value="'.$nonnote[0].'"></label>
                    </div>
                    <div class="option_4colonne">
                         <input type="checkbox" '.$checkbox_note[1].' id="type-9-2"> <label><input type="text" placeholder="Rappel" id="valeur-9-2" value="'.$nonnote[1].'"></label>
                    </div>
                    <div class="option_4colonne">
                         <input type="checkbox" '.$checkbox_note[2].' id="type-9-3"> <label><input type="text" placeholder="Note" id="valeur-9-3" value="'.$nonnote[2].'"></label>
                    </div>
                    <!--div class="option_3colonne">
                         <input type="checkbox"> <label>Statut joueur</label>
                    </div-->
                </div>
                <div class="clear"></div>
            </div>
            <div class="template-div">
                <input type="checkbox" '.$checkbox_fiche.' id="type-10" onchange="ficheaf()"> <label class="orange">Fiche</label>
                <div class="option">
                    <div class="option_2colonne">
                        <input type="checkbox" '.$checkbox_stat.' id="type-11"> <label><input type="text" id="name-10" value="'.$nomstat.'" placeholder="Statistique" style="float:none"></label>
                    </div>
                    <div class="option_2colonne">
                        Colonne : <select id="colonne-11">
                                    <option '.$colsta[1].'>1</option>
                                    <option '.$colsta[2].'>2</option>
                                    <option '.$colsta[3].'>3</option>
                                    <option '.$colsta[4].'>4</option>
                                </select> 
                        Position : <select id="position-11">
                                    <option '.$unst[0].'>0</option>
                                    <option '.$unst[1].'>1</option>
                                    <option '.$unst[2].'>2</option>
                                    <option '.$unst[3].'>3</option>
                                    <option '.$unst[4].'>4</option>
                                    <option '.$unst[5].'>5</option>
                                    <option '.$unst[6].'>6</option>
                                    <option '.$unst[7].'>7</option>
                                    <option '.$unst[8].'>8</option>
                                    <option '.$unst[9].'>9</option>                                    
                                </select><input type="button" class="template-button" id="template-button-stats" name="button-stats" onclick="addstats()" value="+">
                    </div>
                    <div class="div-stats">
                        '.$divstats.'
                         <div class="clear"></div>
                    </div>
                    <div class="seperateur"></div>
                    <div class="option_2colonne">
                        <input type="checkbox" '.$checkbox_stat_sec.' id="type-21"> <label><input type="text" id="name-21" value="'.$nomstat_sec.'" placeholder="Statistique secondaire" style="float:none"></label>
                    </div>
                    <div class="option_2colonne">
                        Colonne : <select id="colonne-21">
                                    <option '.$colsta_sec[1].'>1</option>
                                    <option '.$colsta_sec[2].'>2</option>
                                    <option '.$colsta_sec[3].'>3</option>
                                    <option '.$colsta_sec[4].'>4</option>
                                </select> 
                        Position : <select id="position-21">
                                    <option '.$unst_sec[0].'>0</option>
                                    <option '.$unst_sec[1].'>1</option>
                                    <option '.$unst_sec[2].'>2</option>
                                    <option '.$unst_sec[3].'>3</option>
                                    <option '.$unst_sec[4].'>4</option>
                                    <option '.$unst_sec[5].'>5</option>
                                    <option '.$unst_sec[6].'>6</option>
                                    <option '.$unst_sec[7].'>7</option>
                                    <option '.$unst_sec[8].'>8</option>
                                    <option '.$unst_sec[9].'>9</option>                                    
                                </select><input type="button" class="template-button" id="template-button-stats" name="button-stats" onclick="addstats_sec()" value="+">
                    </div>
                    <div class="div-stats_sec">
                        '.$divstats_sec.'
                         <div class="clear"></div>
                    </div>
                    <div class="seperateur"></div>
                    <div class="option_2colonne">
                        <input type="checkbox" '.$checkbox_enco.' id="type-15"> <label><input id="name-15" type="text" value="'.$nomenco.'" placeholder="Encombrement" style="float:none;"></label>
                    </div>
                    <div class="option_2colonne">
                        Colonne : <select  id="colonne-15">
                            <option '.$colenco[1].'>1</option>
                            <option '.$colenco[2].'>2</option>
                            <option '.$colenco[3].'>3</option>
                            <option '.$colenco[4].'>4</option>
                        </select> Position :
                        <select id="position-15">
                            <option '.$unst[0].'>0</option>
                            <option '.$unst[1].'>1</option>
                            <option '.$unst[2].'>2</option>
                            <option '.$unst[3].'>3</option>
                            <option '.$unst[4].'>4</option>
                            <option '.$unst[5].'>5</option>
                            <option '.$unst[6].'>6</option>
                            <option '.$unst[7].'>7</option>
                            <option '.$unst[8].'>8</option>
                            <option '.$unst[9].'>9</option>                                    
                        </select>
                    </div>
                    <div class="seperateur"></div>
                    <div class="option_2colonne">
                        <input type="checkbox" '.$checkbox_post.' id="type-19"> <label><input type="text" id="name-19" value="'.$nometat.'" placeholder="Etat" style="float:none"></label>
                    </div>
                    <div class="option_2colonne">
                        Colonne : <select  id="colonne-19">
                            <option '.$colpost[1].'>1</option>
                            <option '.$colpost[2].'>2</option>
                            <option '.$colpost[3].'>3</option>
                            <option '.$colpost[4].'>4</option>
                        </select> Position : <select id="position-19">
                            <option '.$unpo[0].'>0</option>
                            <option '.$unpo[1].'>1</option>
                            <option '.$unpo[2].'>2</option>
                            <option '.$unpo[3].'>3</option>
                            <option '.$unpo[4].'>4</option>
                            <option '.$unpo[5].'>5</option>
                            <option '.$unpo[6].'>6</option>
                            <option '.$unpo[7].'>7</option>
                            <option '.$unpo[8].'>8</option>
                            <option '.$unpo[9].'>9</option>                                    
                        </select>
                    </div>
                    <div class="seperateur"></div>
                    <div class="option_2colonne">
                        <input type="checkbox" '.$checkbox_avan.' id="type-18"> <label><input type="text" id="name-18" value="'.$nomavan.'" placeholder="Avantage" style="float:none"></label>
                    </div>
                    <div class="option_2colonne">
                        Colonne : <select id="colonne-18">
                            <option '.$colavan[1].'>1</option>
                            <option '.$colavan[2].'>2</option>
                            <option '.$colavan[3].'>3</option>
                            <option '.$colavan[4].'>4</option>
                        </select>
                        Position : <select id="position-18">
                            <option '.$unav[0].'>0</option>
                            <option '.$unav[1].'>1</option>
                            <option '.$unav[2].'>2</option>
                            <option '.$unav[3].'>3</option>
                            <option '.$unav[4].'>4</option>
                            <option '.$unav[5].'>5</option>
                            <option '.$unav[6].'>6</option>
                            <option '.$unav[7].'>7</option>
                            <option '.$unav[8].'>8</option>
                            <option '.$unav[9].'>9</option>                                    
                        </select>
                    </div>
                    <div class="seperateur"></div>
                    <div class="option_2colonne">
                        <input type="checkbox" '.$checkbox_etat.' id="type-17"> <label><input  id="name-17" type="text" value="'.$nompost.'" placeholder="Posture" style="float:none"></label>
                    </div>
                    <div class="option_2colonne">
                        Colonne : <select id="colonne-17">
                            <option '.$coletat[1].'>1</option>
                            <option '.$coletat[2].'>2</option>
                            <option '.$coletat[3].'>3</option>
                            <option '.$coletat[4].'>4</option>
                        </select> Position : <select id="position-17">
                            <option '.$unet[0].'>0</option>
                            <option '.$unet[1].'>1</option>
                            <option '.$unet[2].'>2</option>
                            <option '.$unet[3].'>3</option>
                            <option '.$unet[4].'>4</option>
                            <option '.$unet[5].'>5</option>
                            <option '.$unet[6].'>6</option>
                            <option '.$unet[7].'>7</option>
                            <option '.$unet[8].'>8</option>
                            <option '.$unet[9].'>9</option>                                    
                        </select>
                    </div>
                    <div class="seperateur"></div>
                     <div class="clear"></div>
                    <input type="checkbox" '.$checkbox_module.' id="type-12-13-14-16"> <label>Module</label>
                    
                    <input type="button" class="template-button" id="template-button-stats" name="button-stats" onclick="addmodule()" value="+">
                    <div class="div-module option">'.$listemoduletpl.'</div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="colonne">
            <div class="template-div">
                <span class="orange" style="width:100%;display:block">Aperçu</span>







                    <div id="fondnoir">
                        <div id="barmenu"> 
                            <h1 class="templatetitle"><img src="img/icone_partie.png" height="8px">Arietes</h1>
                            <h1 id="title2" class="title2" style="display:none">Compteur de points <input id="pointequipe" type="text" value="3"><span style="color:#ffa500">/ 3 </span></h1>
                            <div id="menu">                
                                <a href="" id="creer_fiche" title="Retour au liste fiche">Fiche</a>
                                <a href="" id="regle" title="Regle du jeu" style="display:none">Regle du jeu</a>
                                <a href="" id="doc" title="Documents" style="display:none">Gestion des documents</a>
                                <a href="" id="pnj" title="PNJ" style="display:none">Gestion des PNJ</a>
                                <a href="" id="musique" title="Musique" style="display:none">Gestion des Musiques</a>
                                <a href="" id="plateau" title="Plateau">Plateau</a>
                                <a href="" id="help" title="Aide"  onclick="infodroite(0);">Aide</a>
                                <span class="nomlog">'.$_SESSION['login_user'].'</span> 
                                <a href=""><span><img src="img/logout.png" id="logout"></span></a>
                            </div>
                        </div>
                        <div id="collone">
                            <div id="roll" style="display:none">
                                <span id="lancer">0</span>
                                <span id="bonuscompetence">+<span id="bonuscomp">0</span></span>
                                <input type="text" name="bonusmj" class="sans" id="bonusmj" value="0">
                                <span id="totallancer">0</span>
                                <input id="dice" type="button" style="border: none">
                                <bouton id="dede"></bouton>
                                <div class="clear"></div>
                                <div style="display:block; position:absolute; top:50px;">
                                    <button id="clear">Reset</button>
                                    <button id="cache">Cacher</button>
                                </div>
                                    <input type="radio" id="Choice0" name="contact" value="Aucun"><label for="Choice0"  class="posturecolonne" id="labelchoice0">Aucun</label>
                                    <input type="radio" id="Choice1" name="contact" value="Focus"><label for="Choice1"  class="posturecolonne">Focus</label>
                                    <input type="radio" id="Choice2" name="contact" value="Offensif"><label for="Choice2"  class="posturecolonne" id="labelchoice2">Offensif</label>
                                    <input type="radio" id="Choice3" name="contact" value="Défensif"><label for="Choice3"  class="posturecolonne" id="labelchoice3">Défensif</label>
                                    <div class="clear"></div>
                            </div>
                            <div id="joueur2" class="avatar" style="display:none">
                                <div class="image" style="background:url(\'img/image.jpg\') no-repeat top center; background-size:cover;">
                                            <h1><input name="nom" class="nom" type="text" value="'.$_SESSION['login_user'].'"></h1>
                                            <div class="clear"></div>
                                            <span id="aficheav" class="infoavatar" style="display:none">
                                                <input id="PVmax2" class="info_pv" type="text" value="10" style="color:#4CAF50">
                                                <input id="PV2" class="info_pv" type="text" value="5" style="color:#4CAF50">
                                                <div class="clear"></div>

                                                <input id="PSYmax2" class="info_pv" type="text" value="2" style="color:#2196F3">
                                                <input id="PSY2" class="info_pv" type="text" value="2" style="color:#2196F3">
                                                <div class="clear"></div>
                                            </span>
                                            <div class="clear"></div>
                                            <div class="infoarmure">
                                                <input id="bouclier-2" name="armure" class="armure" type="text" value="0" style="display:none">
                                                <input id="boucliersec-2" name="armure_magique" class="armuremagique" type="text" value="0" style="display:none">
                                            </div>
                                </div>  
                                <div id="barPV2" class="skills" style="height: 3px; width: 10px; background-color:#4CAF50;margin-bottom:3px;"></div>
                                <div id="barPSY2" class="skills" style="height: 3px; width: 100px; background-color:#2196F3;margin-bottom:3px;"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                        <div id="fiches">
                            <ul class="menu-fiche">
                                <li style="display:none" id="ouverturefiche" ><span class="agrandissement"></span></li>
                                <li style="background: #ffffff;display:none" id="fiche_joueur0"><input type="button"  class="fiche" style="color:#000000" value="Note MJ"></li>
                                <li style="background-color: #e2544a;display:none" id="fiche_joueur1"><input type="button"  class="fiche" style="color:#ffffff" value="key"></li>
                                <li style="background-color: #4A90E2;display:none" id="fiche_joueur2"><input type="button"  class="fiche" style="color:#ffffff" value="ultimate"></li>
                                <li style="background-color: #ff8000;display:none" id="fiche_joueur3"><input type="button"  class="fiche" style="color:#ffffff" value="test"></li>
                                <li style="background-color: #5d0476;display:none" id="fiche_joueur4"><input type="button"  class="fiche" style="color:#ffffff" value="dryas"></li>
                            </ul>
                        </div>


                    
                <div class="clear"></div>
             </div>
             <!--div class="template-div">
                <input type="checkbox" '.$checkbox_iframe.' id="type-12-13-14-17"> <label>Roll20 en dev</label>
                </div-->
        </div>
        <div class="clear"></div>
         <input type="submit" value="Validez" class="btn_msg" onclick="savetemplate('.$idtemplate.','.$_SESSION['iduser'].')">';
         if($idtemplate>0){ echo '<input type="submit" value="Supprimez" class="btn_msg" onclick="confirmsuppr('.$idtemplate.')">';} 
    echo '</div>';