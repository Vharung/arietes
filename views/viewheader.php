<!-- viewheader -->
<?php
$truc = isset($_GET['url']) ? $_GET['url'] : NULL;
if($truc=='partie'){
        $angular='<!--script src="'.$currentUrl .'js/plateau/prism.js"></script-->
        <script src="'.$currentUrl .'js/plateau/fabric.js"></script>
        <!--script src="'.$currentUrl .'js/plateau/angular.min.js"></script-->
        <script src="'.$currentUrl .'js/jquery.min.js" type="text/javascript"></script>
        <!--script src="'.$currentUrl .'js/plateau/bootstrap.js"></script-->';
}else{
    $angular='<script src="'.$currentUrl .'js/jquery.min.js" type="text/javascript"></script>';
}
$page = strtolower($page);
echo '<!DOCTYPE html>
<html lang="fr" ng-app="kitchensink">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>Arietes</title>
	<link href="img/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
	<link href="https://fonts.googleapis.com/css?family=MedievalSharp" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="'.$currentUrl .'css/normalize.css">
    <link rel="stylesheet" href="'.$currentUrl .'css/general.css">
    <link rel="stylesheet" href="'.$currentUrl .'css/'.$page.'.css">'
    .$angular.'   
    <script src="'.$currentUrl .'js/jquery-ui.js" type="text/javascript"></script>
    <script src="'.$currentUrl .'js/draggabilly.js" type="text/javascript"></script>
    <script src="'.$currentUrl .'js/general.js" type="text/javascript"></script>';
if(isset($erreur)){
    echo '<script>
    $( document ).ready(function() {
         message("Avertissement","'.$erreur.'","Valider");
    });
    var calldede="";
    </script>';
    unset($erreur);
}
echo '
</head> 

<body> ';

