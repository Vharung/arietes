<!-- viewmodulemenuplateau -->
<div id="info-5" class="regle" style="display:block"><h1>plateau</h1>
<ul class="nav nav-tabs">
  <?php  if($_SESSION['iduser']==$_SESSION['idmj']){
    echo '<li><input type="button" onclick="affichemenuplateau(1)" id="menu1" class="active"></li>';
    $actif='';
    $displaymenupla='style="display:none"';
  }else {
    $actif='class="active"';
    $displaymenupla='style="display:block"';
  }?>
        
        <li><input <?=$actif?> type="button" onclick="affichemenuplateau(2)" id="menu2"></li>
        <li><input type="button" onclick="affichemenuplateau(3)" id="menu3"></li>
  <?php  if($_SESSION['iduser']==$_SESSION['idmj']){
    echo '<li><input type="button" onclick="affichemenuplateau(4)" id="menu4"></li>';
  }?>
      </ul>
<div id="bd-wrapper" ng-controller="CanvasControls">
    <?php  if($_SESSION['iduser']==$_SESSION['idmj']){
    echo '<h5>Choisir couche
             <span style="float:right">Couche : 
             <select id="canvasselect" style="color:#000" onchange="changerplateau(idplateau);">
              <option>carte</option>
              <option selected>pion</option>
              <option>cacher</option>
              <option>gm</option>
             </select></span></h5>
          <h5>Option général</h5>
  <button id="group" class="btn modiftextbutton">Grouper</button>
  <button id="ungroup" class="btn modiftextbutton">Dégrouper</button>
  <button id="multiselect" class="btn modiftextbutton">Multiselection</button>
  <button id="addmore" class="btn modiftextbutton">Ajouter</button><br><br>
  <button id="discard" class="btn modiftextbutton">Déselecetionner</button>
  <button id="copy" class="btn modiftextbutton" onclick="Copy()">Copier</button>
  <button id="paste" class="btn modiftextbutton" onclick="Paste()">Coller</button>
  <div class="clear"></div>';
    }?>
     
    <div id="commands" ng-click="maybeLoadShape($event)">
      <div class="tab-content">
        <div  id="panel3" style="display:none">
          <div class="object-controls" object-buttons-enabled="getSelected()">
            <h5>General : </h5>
            <p>
            <div class="clear"></div>
            <p><label for="opacity">Opacité :</label>
            <input value="100" type="range" bind-value-to="opacity" class="btn-object-action modif-bar-control"></p>
            
            <p> <label for="opacity">Epaisseur :</label>
            <input value="1" min="0" max="30" type="range" bind-value-to="strokeWidth" class="btn-object-action modif-bar-control"></p>
            <div class="clear"></div>
              <!--h5>Attribuer droit : </h5>
              <p>Liste joueur</p>
              <div class="clear"></div>
              <h5>Origine : </h5>
              <p>
                <label class="troiscollone"><input type="radio" name="origin-x" class="origin-x btn-object-action" value="left" bind-value-to="originX"> Gauche</label>
                <label class="troiscollone"><input type="radio" name="origin-x" class="origin-x btn-object-action" value="center" bind-value-to="originX"> Centre</label>
                <label class="troiscollone"><input type="radio" name="origin-x" class="origin-x btn-object-action" value="right" bind-value-to="originX"> Droite</label>
                <label class="troiscollone"><input type="radio" name="origin-y" class="origin-y btn-object-action" value="top" bind-value-to="originY"> Haut</label>
                <label class="troiscollone"><input type="radio" name="origin-y" class="origin-y btn-object-action" value="center" bind-value-to="originY"> Centre</label>
                <label class="troiscollone"><input type="radio" name="origin-y" class="origin-y btn-object-action" value="bottom" bind-value-to="originY"> Bas</label>
              </p>
              <div class="clear"></div>
              <h5>Deplacement : </h5>
              <p>
                <button id="send-backwards" class="btn btn-object-action modiffondbutton" ng-click="sendBackwards()">Vers l'arriere</button>
                <button id="send-to-back" class="btn btn-object-action modiffondbutton" ng-click="sendToBack()">Arrière plan</button>
                <button id="bring-forward" class="btn btn-object-action modiffondbutton" ng-click="bringForward()">Vers l'avant</button>
                <button id="bring-to-front" class="btn btn-object-action modiffondbutton" ng-click="bringToFront()">Premier plan</button>
              </p>
              <div class="clear"></div-->
              <h5>Effet : </h5>
              <p>
                <button id="gradientify" class="btn btn-object-action modiftextbutton" ng-click="gradientify()">Dégradé</button>
                <button id="shadowify" class="btn btn-object-action modiftextbutton" ng-click="shadowify()">Ombre</button>
                <button id="clip" class="btn btn-object-action modiftextbutton" ng-click="clip()">Token</button>
              </p>
            </div>
            <button class="btn btn-object-action modiftextbutton" id="remove-selected" ng-click="removeSelected()" style="float:right">Supprimer le token</button>
          </div>
          <div class="tab-pane" id="panel2"  <?=$displaymenupla?>>
              <h5>Dessinez</h5>
              
                <button id="drawing-mode" class="btn modiftextbutton" onclick="activedraw();">Activez le mode dessin</button>
                <div class="clear"></div>
                <div id="drawing-mode-options" >
                  <!--label for="drawing-mode-selector">Mode :</label>
                  <select id="drawing-mode-selector">
                    <option>Pencil</option>
                    <option>Circle</option>
                    <option>Spray</option>
                    <option>Pattern</option>

                    <option>hline</option>
                    <option>vline</option>
                    <option>square</option>
                    <option>diamond</option>
                    <option>texture</option>
                  </select>
                   <div class="clear" style="margin-top: 10px;"></div-->
                  <label for="drawing-line-width">Line width :</label>
                  <span class="info" id="infowidth">30</span><input type="range" value="30" min="0" max="150" class="modif-bar-control" id="drawing-line-width" onchange="modifwidth();"><br>
                  <div class="clear" style="margin-top: 10px;"></div>
                  <label for="drawing-color">Line color :</label>
                  <input type="color" value="#005E7A" class="sans" style="float: right;" id="drawing-color" onchange='changecolordraw();'><br>

                  <!--label for="drawing-shadow-color">Shadow color:</label>
                  <input type="color" value="#005E7A" id="drawing-shadow-color"><br>

                  <label for="drawing-shadow-width">Shadow width:</label>
                  <span class="info">0</span><input type="range" value="0" min="0" max="50" id="drawing-shadow-width"><br>

                  <label for="drawing-shadow-offset">Shadow offset:</label>
                  <span class="info">0</span><input type="range" value="0" min="0" max="50" id="drawing-shadow-offset"><br-->
                </div>
            <div class="clear"></div>
            <h5>Forme géométrique</h5>
            <p>
              <input type="color" value="#005E7A" id="colorgeo" class="sans" style="float: right;">
              <button type="button" class="btn rect modiftextbutton" onclick="addRect()">▮</button>
              <button type="button" class="btn circle modiftextbutton"  onclick="addCerc()">⬤</button>
              <button type="button" class="btn triangle modiftextbutton" onclick="addTriangle()">▲</button>
              <button type="button" class="btn line modiftextbutton" onclick="addLigne()">/</button>
            </p>
            <div class="clear"></div>
            <h5>Texte</h5>
            <textarea id="text-cont"></textarea>
            <select id="fontfamilly" class="select2 font-change" data-type="fontFamily">
            <option value="Arial">Arial</option>
            <option value="Arial Black">Arial Black</option>
            <option value="Impact">Impact</option>
            <option value="Tahoma">Tahoma</option>
            <option value="Times New Roman">Times New Roman</option>
            </select>
            <select id="size" class="select2 font-change" data-type="fontSize">
            <option value="10">10</option>
            <option value="12">12</option>
            <option value="14">14</option>
            <option value="16">16</option>
            <option value="18">18</option>
            <option value="20">20</option>
            <option value="24">24</option>
            <option value="32">32</option>
            </select>
            <input type="color" class="sans" value="#005E7A" data-type="color" style="margin-top: 5px;">
            <select class="select2 font-change" data-type="textAlign">
            <option value="left">left</option>
            <option value="center">center</option>
            <option value="right">right</option>
            </select><button  id="add" class="btn modiftextbutton">Ajouter |</button>


            </p>
            <div class="clear"></div>
          </div>
          <?php  if($_SESSION['iduser']==$_SESSION['idmj']){
            echo'
          <div  id="panel4"  style="display:none">
            <div id="global-controls">
              <h5>Option</h5>
              <p>
                <label>Arrière-plan :</label></p>
                <input type="color" value="#36393f" id="bg_color" size="10" onchange="bgcolor();" class="sans" style="float:right">
                <button onclick="bgcolordefaut()" class="btn modiftextbutton" style="float:right">Défaut</button>
              </p>
              <div class="clear"></div>
              <h5>Gestion</h5>
              <p>
                <input type="text" value="" id="changernomplat">
                <button class="modiftextbutton" style="width:100%;margin-bottom: 3px;" onclick="changernomplat()"> Changer nom</button>
                <button class="modiftextbutton" style="width:100%;margin-bottom: 3px;" onclick="nouveauplateau()">Nouveau</button><br>
                <button class="modiftextbutton" style="width:100%;margin-bottom: 3px;" onclick="listeaffichage()">Afficher à :</button>
                <div id="listejoueuraffiche"></div>

                <button class="btn btn-danger clear modiftextbutton" onclick="confirmClear()" style="width: 100%;margin-bottom: 3px;">Vider le tableau</button>
                <button class="modiftextbutton" style="width:100%;margin-bottom: 3px;" onclick="confirmSuppr()">Supprimer le plateau</button>

                <button class="btn btn-danger clear modiftextbutton" onclick="savecanvas()" style="width: 100%;margin-bottom: 3px;">Save</button>
                <!--button class="btn btn-danger clear modiftextbutton"  onclick="loadcanvas()" style="width: 100%;margin-bottom: 3px;">Charger</button-->
                </p>
                <div class="clear"></div>
                <h5>Mes plateaux</h5>
                <div id="listimgplateau">';

            if(isset($id)){
              for($i = 0; $i < count($id); $i++){
                echo '<div id="imageplat'.$id[$i].'" class="board" style="background:url(plateau/'.$img[$i].'.jpg) center center no-repeat; " onclick="changerplateau('.$id[$i].')">'.$nom[$i].'</div>';
              }
            }
            
            echo '<script> 
              if(idmj==idplayer && idplateau>0){
                name=$("#nomplateauactif").html();
                $("#changernomplat").val(name);
                activeplateau(idplateau);
              }

              </script>
            </div>

                


              


              
            </div>

          </div>
          <div  id="panel1" style="display:block">
            <h5>Grille</h5>
              <input type="button" onclick="sansgrille()" title="effet" id="menu100" class="active" value="Sans">
              <input type="button" onclick="grille()" title="effet" id="menu101" value="Carré">
              <input type="button" onclick="grillehexa()" title="effet" id="menu102" value="Hexa">
             <!--ul class="nav nav-tabs">
              <li><input type="button" onclick="affichemenuotoken(20)" id="menu20" class="active"></li>
              <li><input type="button" onclick="affichemenuotoken(21)" id="menu21"></li>
              <li><input type="button" onclick="affichemenuotoken(22)" id="menu22"></li>
            </ul>
            <div  id="panel20"  style="display:block">
              <p>
                <button type="button" class="btn biblio modiftextbutton">mur</button>
                <button type="button" class="btn biblio modiftextbutton">arbre</button>
              </p>
              <div class="clear"></div>
            </div>
            <div  id="panel21"  style="display:none">
              <div class="clear"></div>
            </div>
            <div  id="panel22"  style="display:none">
              <div class="clear"></div>
            </div-->
             <h5>bibliotheque</h5>
            <ul class="nav nav-tabs">
              <li><input type="button" onclick="affichemenuobjet(10)" title="effet" id="menu10" class="active"></li><!--effet-->
              <li><input type="button" onclick="affichemenuobjet(11)" title="token" id="menu11"></li><!--token-->
              <li><input type="button" onclick="affichemenuobjet(12)" title="lieu" id="menu12"></li><!--lieu-->
              <li><input type="button" onclick="affichemenuobjet(13)" title="objet" id="menu13"></li><!--objet-->
            </ul>
            <div  id="panel10"  style="display:block">
              <p>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/fumee.png\')"><img src="bibliotheque/effet/fumee.png" title="fumée"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/explosion.png\')"><img src="bibliotheque/effet/explosion.png" title="explosion"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/trace-explosion.png\')"><img src="bibliotheque/effet/trace-explosion.png" title="trace d\'explosion"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Nebula-a.png\')"><img src="bibliotheque/effet/Nebula-a.png" title="Nebula-a"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Nebula-b.png\')"><img src="bibliotheque/effet/Nebula-b.png" title="Nebula-b"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Nebula-c.png\')"><img src="bibliotheque/effet/Nebula-c.png" title="Nebula-c"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Scorch-a.png\')"><img src="bibliotheque/effet/Scorch-a.png" title="trace d\'explosion"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Scorch-c.png\')"><img src="bibliotheque/effet/Scorch-c.png" title="trace d\'explosion"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Smoke02_SR.png\')"><img src="bibliotheque/effet/Smoke02_SR.png" title="fumée magique"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Smoke03_SR.png\')"><img src="bibliotheque/effet/Smoke03_SR.png" title="fumée magique"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Fire-b.png\')"><img src="bibliotheque/effet/Fire-b.png" title="Feu"></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg (\'bibliotheque/effet/Fire-c.png\')"><img src="bibliotheque/effet/Fire-c.png" title="Feu"></button>

              </p>
              <div class="clear"></div>
            </div>
            <div  id="panel11"  style="display:none">
              <p>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/1.png\')"><img src="bibliotheque/token/1.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/2.png\')"><img src="bibliotheque/token/2.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/3.png\')"><img src="bibliotheque/token/3.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/4.png\')"><img src="bibliotheque/token/4.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/5.png\')"><img src="bibliotheque/token/5.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/6.png\')"><img src="bibliotheque/token/6.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/7.png\')"><img src="bibliotheque/token/7.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/8.png\')"><img src="bibliotheque/token/8.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/9.png\')"><img src="bibliotheque/token/9.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/10.png\')"><img src="bibliotheque/token/10.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/11.png\')"><img src="bibliotheque/token/11.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/12.png\')"><img src="bibliotheque/token/12.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/13.png\')"><img src="bibliotheque/token/13.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/14.png\')"><img src="bibliotheque/token/14.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/15.png\')"><img src="bibliotheque/token/15.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/16.png\')"><img src="bibliotheque/token/16.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/17.png\')"><img src="bibliotheque/token/17.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/18.png\')"><img src="bibliotheque/token/18.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/19.png\')"><img src="bibliotheque/token/19.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/20.png\')"><img src="bibliotheque/token/20.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/21.png\')"><img src="bibliotheque/token/21.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/22.png\')"><img src="bibliotheque/token/22.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/23.png\')"><img src="bibliotheque/token/23.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/24.png\')"><img src="bibliotheque/token/24.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/25.png\')"><img src="bibliotheque/token/25.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/26.png\')"><img src="bibliotheque/token/26.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/27.png\')"><img src="bibliotheque/token/27.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/28.png\')"><img src="bibliotheque/token/28.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/29.png\')"><img src="bibliotheque/token/29.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/30.png\')"><img src="bibliotheque/token/30.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/31.png\')"><img src="bibliotheque/token/31.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/32.png\')"><img src="bibliotheque/token/32.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/33.png\')"><img src="bibliotheque/token/33.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/34.png\')"><img src="bibliotheque/token/34.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/35.png\')"><img src="bibliotheque/token/35.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/36.png\')"><img src="bibliotheque/token/36.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/37.png\')"><img src="bibliotheque/token/37.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/38.png\')"><img src="bibliotheque/token/38.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/39.png\')"><img src="bibliotheque/token/39.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/40.png\')"><img src="bibliotheque/token/40.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/41.png\')"><img src="bibliotheque/token/41.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/42.png\')"><img src="bibliotheque/token/42.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/43.png\')"><img src="bibliotheque/token/43.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/44.png\')"><img src="bibliotheque/token/44.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/45.png\')"><img src="bibliotheque/token/45.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/46.png\')"><img src="bibliotheque/token/46.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/47.png\')"><img src="bibliotheque/token/47.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/48.png\')"><img src="bibliotheque/token/48.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/49.png\')"><img src="bibliotheque/token/49.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/50.png\')"><img src="bibliotheque/token/50.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/51.png\')"><img src="bibliotheque/token/51.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/52.png\')"><img src="bibliotheque/token/52.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/53.png\')"><img src="bibliotheque/token/53.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/54.png\')"><img src="bibliotheque/token/54.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/55.png\')"><img src="bibliotheque/token/55.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/56.png\')"><img src="bibliotheque/token/56.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/57.png\')"><img src="bibliotheque/token/57.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/58.png\')"><img src="bibliotheque/token/58.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/59.png\')"><img src="bibliotheque/token/59.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/60.png\')"><img src="bibliotheque/token/60.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/61.png\')"><img src="bibliotheque/token/61.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/62.png\')"><img src="bibliotheque/token/62.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/63.png\')"><img src="bibliotheque/token/63.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/64.png\')"><img src="bibliotheque/token/64.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/65.png\')"><img src="bibliotheque/token/65.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/66.png\')"><img src="bibliotheque/token/66.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/67.png\')"><img src="bibliotheque/token/67.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/68.png\')"><img src="bibliotheque/token/68.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/69.png\')"><img src="bibliotheque/token/69.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/70.png\')"><img src="bibliotheque/token/70.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/81.png\')"><img src="bibliotheque/token/81.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/82.png\')"><img src="bibliotheque/token/82.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/83.png\')"><img src="bibliotheque/token/83.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/84.png\')"><img src="bibliotheque/token/84.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/85.png\')"><img src="bibliotheque/token/85.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/86.png\')"><img src="bibliotheque/token/86.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/87.png\')"><img src="bibliotheque/token/87.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/88.png\')"><img src="bibliotheque/token/88.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/89.png\')"><img src="bibliotheque/token/89.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/90.png\')"><img src="bibliotheque/token/90.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/91.png\')"><img src="bibliotheque/token/91.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/92.png\')"><img src="bibliotheque/token/92.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/93.png\')"><img src="bibliotheque/token/93.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/94.png\')"><img src="bibliotheque/token/94.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/95.png\')"><img src="bibliotheque/token/95.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/96.png\')"><img src="bibliotheque/token/96.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/97.png\')"><img src="bibliotheque/token/97.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/98.png\')"><img src="bibliotheque/token/98.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/99.png\')"><img src="bibliotheque/token/99.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/100.png\')"><img src="bibliotheque/token/100.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/101.png\')"><img src="bibliotheque/token/101.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/102.png\')"><img src="bibliotheque/token/102.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/103.png\')"><img src="bibliotheque/token/103.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/104.png\')"><img src="bibliotheque/token/104.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/105.png\')"><img src="bibliotheque/token/105.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/106.png\')"><img src="bibliotheque/token/106.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/107.png\')"><img src="bibliotheque/token/107.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/108.png\')"><img src="bibliotheque/token/108.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/109.png\')"><img src="bibliotheque/token/109.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/110.png\')"><img src="bibliotheque/token/110.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/111.png\')"><img src="bibliotheque/token/111.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/112.png\')"><img src="bibliotheque/token/112.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/113.png\')"><img src="bibliotheque/token/113.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/114.png\')"><img src="bibliotheque/token/114.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/115.png\')"><img src="bibliotheque/token/115.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/116.png\')"><img src="bibliotheque/token/116.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/117.png\')"><img src="bibliotheque/token/117.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/118.png\')"><img src="bibliotheque/token/118.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/119.png\')"><img src="bibliotheque/token/119.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/120.png\')"><img src="bibliotheque/token/120.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/121.png\')"><img src="bibliotheque/token/121.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/122.png\')"><img src="bibliotheque/token/122.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/123.png\')"><img src="bibliotheque/token/123.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/124.png\')"><img src="bibliotheque/token/124.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/125.png\')"><img src="bibliotheque/token/125.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/126.png\')"><img src="bibliotheque/token/126.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/127.png\')"><img src="bibliotheque/token/127.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/128.png\')"><img src="bibliotheque/token/128.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/129.png\')"><img src="bibliotheque/token/129.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/130.png\')"><img src="bibliotheque/token/130.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/131.png\')"><img src="bibliotheque/token/131.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/132.png\')"><img src="bibliotheque/token/132.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/133.png\')"><img src="bibliotheque/token/133.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/134.png\')"><img src="bibliotheque/token/134.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/135.png\')"><img src="bibliotheque/token/135.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/136.png\')"><img src="bibliotheque/token/136.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/137.png\')"><img src="bibliotheque/token/137.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/138.png\')"><img src="bibliotheque/token/138.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/139.png\')"><img src="bibliotheque/token/139.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/140.png\')"><img src="bibliotheque/token/140.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/141.png\')"><img src="bibliotheque/token/141.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/142.png\')"><img src="bibliotheque/token/142.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/143.png\')"><img src="bibliotheque/token/143.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/144.png\')"><img src="bibliotheque/token/144.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/145.png\')"><img src="bibliotheque/token/145.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/146.png\')"><img src="bibliotheque/token/146.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/147.png\')"><img src="bibliotheque/token/147.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/149.png\')"><img src="bibliotheque/token/149.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/150.png\')"><img src="bibliotheque/token/150.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/151.png\')"><img src="bibliotheque/token/151.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/152.png\')"><img src="bibliotheque/token/152.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/153.png\')"><img src="bibliotheque/token/153.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/154.png\')"><img src="bibliotheque/token/154.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/155.png\')"><img src="bibliotheque/token/155.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/156.png\')"><img src="bibliotheque/token/156.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/157.png\')"><img src="bibliotheque/token/157.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/158.png\')"><img src="bibliotheque/token/158.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/159.png\')"><img src="bibliotheque/token/159.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/160.png\')"><img src="bibliotheque/token/160.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/161.png\')"><img src="bibliotheque/token/161.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/162.png\')"><img src="bibliotheque/token/162.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/163.png\')"><img src="bibliotheque/token/163.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/164.png\')"><img src="bibliotheque/token/164.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/165.png\')"><img src="bibliotheque/token/165.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/166.png\')"><img src="bibliotheque/token/166.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/167.png\')"><img src="bibliotheque/token/167.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/168.png\')"><img src="bibliotheque/token/168.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/169.png\')"><img src="bibliotheque/token/169.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/170.png\')"><img src="bibliotheque/token/170.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/181.png\')"><img src="bibliotheque/token/181.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/182.png\')"><img src="bibliotheque/token/182.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/183.png\')"><img src="bibliotheque/token/183.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/184.png\')"><img src="bibliotheque/token/184.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/185.png\')"><img src="bibliotheque/token/185.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/186.png\')"><img src="bibliotheque/token/186.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/187.png\')"><img src="bibliotheque/token/187.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/188.png\')"><img src="bibliotheque/token/188.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/190.png\')"><img src="bibliotheque/token/190.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/191.png\')"><img src="bibliotheque/token/191.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/192.png\')"><img src="bibliotheque/token/192.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/token/193.png\')"><img src="bibliotheque/token/193.png" title=""></button>
              </p>
              <div class="clear"></div>
            </div>
            <div  id="panel12"  style="display:none">
              <p>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/1.png\')"><img src="bibliotheque/lieu/1.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/2.png\')"><img src="bibliotheque/lieu/2.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/3.png\')"><img src="bibliotheque/lieu/3.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/4.png\')"><img src="bibliotheque/lieu/4.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/5.png\')"><img src="bibliotheque/lieu/5.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/6.png\')"><img src="bibliotheque/lieu/6.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/7.png\')"><img src="bibliotheque/lieu/7.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/8.png\')"><img src="bibliotheque/lieu/8.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/9.png\')"><img src="bibliotheque/lieu/9.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/10.png\')"><img src="bibliotheque/lieu/10.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/11.png\')"><img src="bibliotheque/lieu/11.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/12.png\')"><img src="bibliotheque/lieu/12.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/13.png\')"><img src="bibliotheque/lieu/13.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/14.png\')"><img src="bibliotheque/lieu/14.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/15.png\')"><img src="bibliotheque/lieu/15.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/16.png\')"><img src="bibliotheque/lieu/16.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/17.png\')"><img src="bibliotheque/lieu/17.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/18.png\')"><img src="bibliotheque/lieu/18.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/19.png\')"><img src="bibliotheque/lieu/19.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/20.png\')"><img src="bibliotheque/lieu/20.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/21.png\')"><img src="bibliotheque/lieu/21.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/22.png\')"><img src="bibliotheque/lieu/22.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/lieu/23.png\')"><img src="bibliotheque/lieu/23.png" title=""></button>
              </p>
              <div class="clear"></div>
            </div>
            <div  id="panel13"  style="display:none">
              <p>
              <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/1.png\')"><img src="bibliotheque/objet/1.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/2.png\')"><img src="bibliotheque/objet/2.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/3.png\')"><img src="bibliotheque/objet/3.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/4.png\')"><img src="bibliotheque/objet/4.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/5.png\')"><img src="bibliotheque/objet/5.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/6.png\')"><img src="bibliotheque/objet/6.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/7.png\')"><img src="bibliotheque/objet/7.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/8.png\')"><img src="bibliotheque/objet/8.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/9.png\')"><img src="bibliotheque/objet/9.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/10.png\')"><img src="bibliotheque/objet/10.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/11.png\')"><img src="bibliotheque/objet/11.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/12.png\')"><img src="bibliotheque/objet/12.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/13.png\')"><img src="bibliotheque/objet/13.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/14.png\')"><img src="bibliotheque/objet/14.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/15.png\')"><img src="bibliotheque/objet/15.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/16.png\')"><img src="bibliotheque/objet/16.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/17.png\')"><img src="bibliotheque/objet/17.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/18.png\')"><img src="bibliotheque/objet/18.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/19.png\')"><img src="bibliotheque/objet/19.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/20.png\')"><img src="bibliotheque/objet/20.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/21.png\')"><img src="bibliotheque/objet/21.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/22.png\')"><img src="bibliotheque/objet/22.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/23.png\')"><img src="bibliotheque/objet/23.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/24.png\')"><img src="bibliotheque/objet/24.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/25.png\')"><img src="bibliotheque/objet/25.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/26.png\')"><img src="bibliotheque/objet/26.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/27.png\')"><img src="bibliotheque/objet/27.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/28.png\')"><img src="bibliotheque/objet/28.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/29.png\')"><img src="bibliotheque/objet/29.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/30.png\')"><img src="bibliotheque/objet/30.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/31.png\')"><img src="bibliotheque/objet/31.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/32.png\')"><img src="bibliotheque/objet/32.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/33.png\')"><img src="bibliotheque/objet/33.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/34.png\')"><img src="bibliotheque/objet/34.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/35.png\')"><img src="bibliotheque/objet/35.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/36.png\')"><img src="bibliotheque/objet/36.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/37.png\')"><img src="bibliotheque/objet/37.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/38.png\')"><img src="bibliotheque/objet/38.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/39.png\')"><img src="bibliotheque/objet/39.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/40.png\')"><img src="bibliotheque/objet/40.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/41.png\')"><img src="bibliotheque/objet/41.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/42.png\')"><img src="bibliotheque/objet/42.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/43.png\')"><img src="bibliotheque/objet/43.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/44.png\')"><img src="bibliotheque/objet/44.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/45.png\')"><img src="bibliotheque/objet/45.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/46.png\')"><img src="bibliotheque/objet/46.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/47.png\')"><img src="bibliotheque/objet/47.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/48.png\')"><img src="bibliotheque/objet/48.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/49.png\')"><img src="bibliotheque/objet/49.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/50.png\')"><img src="bibliotheque/objet/50.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/51.png\')"><img src="bibliotheque/objet/51.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/52.png\')"><img src="bibliotheque/objet/52.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/53.png\')"><img src="bibliotheque/objet/53.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/54.png\')"><img src="bibliotheque/objet/54.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/55.png\')"><img src="bibliotheque/objet/55.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/56.png\')"><img src="bibliotheque/objet/56.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/57.png\')"><img src="bibliotheque/objet/57.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/58.png\')"><img src="bibliotheque/objet/58.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/59.png\')"><img src="bibliotheque/objet/59.png" title=""></button> 
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/60.png\')"><img src="bibliotheque/objet/60.png" title=""></button>

                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/61.png\')"><img src="bibliotheque/objet/61.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/62.png\')"><img src="bibliotheque/objet/62.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/63.png\')"><img src="bibliotheque/objet/63.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/64.png\')"><img src="bibliotheque/objet/64.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/65.png\')"><img src="bibliotheque/objet/65.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/66.png\')"><img src="bibliotheque/objet/66.png" title=""></button>
                <button type="button" class="btn biblio modiftextbutton" onclick="addImg(\'bibliotheque/objet/67.png\')"><img src="bibliotheque/objet/67.png" title=""></button>
              </p>
              <div class="clear"></div>
            </div>
            <h5>Image</h5>
            <p>
              <input type="text" id="imgperso" value=""  class="sans" placeholder="url" style="background: #36393F;width:calc(100% - 60px); float:left;padding: 3px;color:#fff">
              <button type="button" class="bouton_save" onclick="addImgUrl()" style="margin-top:0px;">Ajouter</button>
            </p>
            <div class="clear"></div>
            <h5>Token personnage generateur</h5>
            <button class="bouton_save" style="margin-top:0px;" onclick="listetoken();">Générer liste</button>
            <span id="listetoken"></span>
            <div class="clear"></div>
            
          </div>';
        }?>
        </div>
      </div>
    </div>
</div>