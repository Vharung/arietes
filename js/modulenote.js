function savenote(id){
     $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                notemj:'oui',
                valeurnb:id,
                note:$('#notemj'+id).val(),
            },
            success: function(content){
            },
            fail: function(content){
            },
    });
}
function modifetatmj(id){
     $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            tourmj:'oui',
            id_player:id,
            etat_player:$('#etatjoueurmj-'+id).val(),
            tour_player:$('#tourjoueurmj-'+id).val(),
        },
        success: function(content){
        },
        fail: function(content){
        },
    });
}
function saveturnorder(id_player){
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            turnorder:'oui',
            id_player:id_player,
            nb:$('#turnorder-'+id_player).val(),
            name:$('#joueur'+id_player+' input.nom').val(),
            color1:$('#couleurprincipale'+id_player).val(),
            color2:$('#couleursecondaire'+id_player).val(),
        },
        success: function(content){
        },
        fail: function(content){
        },
    });
}
function updateOrder(order) {
    var divList = $(".card");
    if (order == '1') { //tri numerique croissant
        divList.sort(function (a, b) {
            return  $(a).data('turn') - $(b).data('turn');
        });
    } else if (order == '2') {//tri numerique decroissant
        divList.sort(function (a, b) {
            return $(b).data('turn') - $(a).data('turn');
        });
    }
    $(".card-container").html(divList);
}
function nexturn(){
    listorder=[];
    neworder='';
    $('.card').each(function( index ) {
        data=$(this).data('turn');
        contenu=$(this).html();
        listorder.push('<div class="noteplayer card" data-turn="'+data+'">'+contenu+'</div>');
    });
    for(i=1;i<listorder.length;i++){
        neworder+=listorder[i];
    }
    neworder+=listorder[0];
    $('.card-container').html(neworder)
}
function orderpnj(){
    nb=$('#turnorder-0').val();
    $('#turnorder-0').attr('value',nb);
    $('#turnorder-0').parent().parent('div').attr('data-turn',nb);
}