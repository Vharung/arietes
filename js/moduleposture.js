compteactive=0;
function ajoutposture(compte,iduser){
    nomuser=$('#joueur'+iduser+' input.nom').val();
    valeurcolor=$("#couleurprincipale"+iduser).val();
    totallancer=$('#totallancer').html();
    for(i=0;i<4;i++){
        $('#labelchoice'+i).css({'color':'#ffffff'});
    }
    if(compteactive==1 ){
        totallancer=parseInt(totallancer)-5;
        $('#totallancer').html(totallancer);
    }
    if(compte==1 && compteactive!=1){
        compteactive=1; 
        $('#labelchoice1').css({'color':'#ffa500'});
        messagedes('<span style="color:' + valeurcolor + '">' + nomuser + ' </span> passe en</p><h1 style="color:#3185FC">Focus</h1>');
        totallancer=parseInt(totallancer)+5;
        $('#totallancer').html(totallancer);
        $('#joueur'+iduser+' .posture').css({"background":"url('img/posture-1.png')","background-size":"cover"});
        var audio = new Audio('../sound/focus.wav');
        audio.play();
        changeposture(compte,iduser);
    }else if(compte==2 && compteactive!=2){
        compteactive=2; 
        $('#labelchoice2').css({'color':'#ffa500'});
        messagedes('<span style="color:' + valeurcolor + '">' + nomuser + ' </span> passe en</p><h1 style="color:#E84855">Offensif</h1>');
        $('#joueur'+iduser+' .posture').css({"background":"url('img/posture-2.png')","background-size":"cover"});
        var audio = new Audio('../sound/offensif.wav');
        audio.play();
        changeposture(compte,iduser);
    }else if(compte==3 && compteactive!=3){
        compteactive=3; 
        $('#labelchoice3').css({'color':'#ffa500'});
        messagedes('<span style="color:' + valeurcolor + '">' + nomuser + ' </span> passe en</p><h1 style="color:#F9DC5C">Défensif</h1>');
        $('#joueur'+iduser+' .posture').css({"background":"url('img/posture-3.png')","background-size":"cover"});
        var audio = new Audio('../sound/Defensif.wav');
        audio.play();
        changeposture(compte,iduser);
    }else {
        compteactive=0; 
        $('#labelchoice0').css({'color':'#ffa500'});
        messagedes('<span style="color:' + valeurcolor + '">' + nomuser + ' </span> n\'a aucun statut</p>');
        $('#joueur'+iduser+' .posture').css({"background":"none"});
        changeposture(compte,iduser);
    }
    setTimeout(function () {
        if(compteactive==1 ){
            totallancer=parseInt(totallancer)-5;
            $('#totallancer').html(totallancer);
        }
        for(i=0;i<4;i++){
            $('#labelchoice'+i).css({'color':'#ffffff'});
            compteactive=0;
        }
    }, 20000)
}
function changeposture(compte,iduser){
    nomuser=$('#joueur'+iduser+' input.nom').val();
    for(i=0;i<4;i++){
         $('#fiche-'+iduser+' .posture-'+i).removeClass('activepost');
    }
    $('#fiche-'+iduser+' .posture-'+compte).addClass('activepost');
    
}