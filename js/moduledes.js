$('#bonusmj').on('wheel', function(event){
    var rolled = $('#bonusmj').val();
    if(event.originalEvent.deltaY < 0){
       rolled = parseInt(rolled)+5;
    }
    else {
      rolled = parseInt(rolled)-5;
    }
    if(rolled>=100 || rolled<=-100){
        $('#bonusmj').css({"font-size":"1.5em"}); 
    }else {
        $('#bonusmj').css({"font-size":"2.5em"});
    }
    $('#bonusmj').val(rolled);
    synchodes();
});
//calcul du resultat
function calculroll() {
    setTimeout(function () {
        reset_roll();
    }, 20000)
    var x = $('#lancer').html();
    var y = $('#bonuscomp').html();
    var z = $('#bonusmj').val();
    var total = parseInt(x) + parseInt(y) + parseInt(z)
    if ($('#Choice1').is(':checked')) {
        total = total + 5;
    }
    if(total>=100 || total<=-100){
        $('#totallancer').css({"font-size":"2em"}); 
    }else {
        $('#totallancer').css({"font-size":"3em"});
    }
    $('#totallancer').html(total);

}
//reset roll
function reset_roll(){ 
    $('#lancer').html(0);
    $('#bonuscomp').html(0);
    $('#bonusmj').val(0);
    $('#totallancer').html(0);
    $('#set').val(default_des);
    $( "#Choice2" ).prop( "checked", false );
    $( "#Choice1" ).prop( "checked", false );
    $( "#Choice3" ).prop( "checked", false );
}

//lanceur des prédefini personnaliser
function lancerdede(idlancer){
    valeurcolor=$("#couleurprincipale"+idlancer).val();
    valeur2color=$("#couleursecondaire"+idlancer).val();
    dr=$('#set').val();
    varcalldede=[];
    dr=dr.replace(/ /g,"");
    lesdes=dr.split("+");
    resultatdes=0;
    for(i=0;i<lesdes.length;i++){
        des=lesdes[i].split("d");
        nbdes=des[0];
        typedes=des[1];
        if(typedes==100){
            for(j=0;j<nbdes;j++){
                d100=Math.floor((Math.random() * 9))
                varcalldede.push(d100);
                resultatdes=resultatdes+d100*10;
            }            
        }else if(typedes==20){
            for(j=0;j<nbdes;j++){
                d20=Math.floor((Math.random() * 20)+1)
                varcalldede.push(d20);
                resultatdes=resultatdes+d20;
            }
        }else if(typedes==10){
            for(j=0;j<nbdes;j++){             
                d10=Math.floor((Math.random() * 10)+1)
                varcalldede.push(d10);
                resultatdes=resultatdes+d10;
            }
        }else if(typedes==8){
            for(j=0;j<nbdes;j++){
                d8=Math.floor((Math.random() * 8)+1)
                varcalldede.push(d8);
                resultatdes=resultatdes+d8;
            }
        }else if(typedes==6){
            for(j=0;j<nbdes;j++){
                d6=Math.floor((Math.random() * 6)+1)
                varcalldede.push(d6);
                resultatdes=resultatdes+d6;
            }
        }else if(typedes==4){
            for(j=0;j<nbdes;j++){
                d4=Math.floor((Math.random() * 4)+1);
                varcalldede.push(d4)
                resultatdes=resultatdes+d4; 
            }               
        }else if(typedes==undefined){
             resultatdes=resultatdes+parseInt(nbdes);
        }        
    }
   
    nomlanceur=$('#joueur'+idlancer+' .nom').val();
    if(nomlanceur=="" || nomlanceur == undefined){nomlanceur='MJ'}
    valeurcolor=$("#couleurprincipale"+idlancer).val();
    if(valeurcolor=="" ||valeurcolor== undefined){
        valeurcolor='#ffffff';
    }
    valeur2color=$("#couleursecondaire"+idlancer).val();
    if(valeur2color=="" ||valeur2color== undefined){
        valeur2color='#000';
    }
    if($('#Choice2').is(':checked')){
            destinataire=idmj;
        }else {destinataire=0}
    synchronistionroll(dr,varcalldede,nomlanceur,valeurcolor,valeur2color,resultatdes,idlancer,destinataire);//*resultatdes
    
}
function lancerlesdede(id){
    if(cacheactive==1){
        if(id==idplayer || idplayer==idmj){
            $('#dede').click();
        }
    }else {
        $('#dede').click();
    }
    var audio = new Audio('../sound/lancer.wav');
    audio.play();
    setTimeout(function () {
        $('#joueur'+id+' .diceindic').css({'display':'none'});
    }, 2000)
    $('#joueur'+id+' .diceindic').css({'display':'block'}); 
}
function resultRoll(result,nomlanceur,colorlanceur){
    result=String(result)
    var myTab = result.split(',');
    var total=parseInt($('#bonusdes').val());
    for(var i = 0 ; i < myTab.length ; i++){
       total = total+parseInt(myTab[i]); 
    }
    offen=$('#labelchoice2').css('color');
    if(offen=='rgb(255, 165, 0)'){
        critique=valeurcritique+valeurcritique;
        reussite=valeurreussite+valeurreussite;
    }else{
        critique=valeurcritique;
        reussite=valeurreussite;
    } 
    var max = $('#totallancer').html();
    if(lancerprincipal==1){//lancer de des principale
        dest=0;
        if(cacheactive==1){
            dest=idmj;
        }
        if(total=='00'){total=100;messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#de2221;">'+total+'</span></p><h1 style="color:#de2221;">Echec critique </h1>') }
        if(reussite<critique){
          if (total <= reussite) {
                if(cacheactive==1){
                    messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fcd667">'+total+'</span></p><h1 style="color:#fcd667;">Réussite critique</h1>')
                    synchromj('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="ccolor:#fcd667">'+total+'</span></p><h1 style="color:#fcd667;">Réussite critique</h1>')
                }else {
                    synchromessage('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fcd667">'+total+'</span></p><h1 style="color:#fcd667;">Réussite critique</h1>');
                }                
                code = 6;
            } else if (total >= critique) {
                if(cacheactive==1){
                    messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#de2221">'+total+'</span></p><h1 style="color:#de2221;">Echec critique</h1>')
                    synchromj('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#de2221">'+total+'</span></p><h1 style="color:#de2221;">Echec critique</h1>');
                }else {
                    synchromessage('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#de2221">'+total+'</span></p><h1 style="color:#de2221;">Echec critique</h1>');
                }
                code = 5;
            } else if (total <= max) {
                if(cacheactive==1){
                    messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#5dab48">'+total+'</span></p><h1 style="color:#5dab48;">Réussite</h1>')
                    synchromj('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#5dab48">'+total+'</span></p><h1 style="color:#5dab48;">Réussite</h1>');
                }else {
                    synchromessage('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#5dab48">'+total+'</span></p><h1 style="color:#5dab48;">Réussite</h1>');
                }
                code = 3;
            } else if (total >= max) {
                if(cacheactive==1){
                    messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fca84e">'+total+'</span></p><h1 style="color:#fca84e;">Echec</h1>')
                    synchromj('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fca84e">'+total+'</span></p><h1 style="color:#fca84e;">Echec</h1>');
                }else {
                    synchromessage('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fca84e">'+total+'</span></p><h1 style="color:#fca84e;">Echec</h1>');
                }
                code = 4;
            }
        }else {
           if (total >= reussite) {
                if(cacheactive==1){
                    messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fcd667">'+total+'</span></p><h1 style="color:#fcd667;">Réussite critique</h1>')
                    synchromj('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fcd667">'+total+'</span></p><h1 style="color:#fcd667;">Réussite critique</h1>');
                }else {
                    synchromessage('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fcd667">'+total+'</span></p><h1 style="color:#fcd667;">Réussite critique</h1>');
                }
                code = 6;
            } else if (total <= critique) {
               if(cacheactive==1){
                    messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#de2221">'+total+'</span></p><h1 style="color:#de2221;">Echec critique</h1>')
                    synchromj('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#de2221">'+total+'</span></p><h1 style="color:#de2221;">Echec critique</h1>');
                }else {
                    synchromessage('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#de2221">'+total+'</span></p><h1 style="color:#de2221;">Echec critique</h1>');
                }
                code = 5;
            } else if (total >= max) {
                if(cacheactive==1){
                    messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#5dab48">'+total+'</span></p><h1 style="color:#5dab48;">Réussite</h1>')
                    synchromj('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#5dab48">'+total+'</span></p><h1 style="color:#5dab48;">Réussite</h1>');
                }else {
                    synchromessage('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#5dab48">'+total+'</span></p><h1 style="color:#5dab48;">Réussite</h1>');
                }
                code = 3;
            } else if (total <= max) {
                if(cacheactive==1){
                    messagedes('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fca84e">'+total+'</span></p><h1 style="color:#fca84e;">Echec</h1>')
                    synchromj('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fca84e">'+total+'</span></p><h1 style="color:#fca84e;">Echec</h1>');
                }else {
                    synchromessage('<span style="color:' + colorlanceur + '">' + nomlanceur + ' </span> à fait : <span style="color:#fca84e">'+total+'</span></p><h1 style="color:#fca84e;">Echec</h1>');
                }
                code = 4;
            }
        }
        
        $('#form_message').val(total);
         saveTchat(1,dest,code);
        $('#form_message').val('');
        $('#form_message').css({'color': '#ffffff'});
        $('#bonusdes').val(0);
        $('#set').val(default_des);/*valeur par defaut de la template*/
    } else{//lancer de des secondaire lancer depuis le tchat /r etc...
        $('#set').val(default_des);
        $('#bonusdes').val(0);
        var lancer_des=$('#form_message').val();
        $('#form_message').val(lancer_des+' : '+total);
        
        lancerprincipal=1;
    } 
}