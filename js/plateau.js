$(function () {
	canvas=document.getElementById("tableboard-0");
	token=document.getElementById("tableboard-1");
	arbre=document.getElementById("tableboard-2");
	mj=document.getElementById("tableboard-3");
	var lescanvas = [canvas,token,arbre,mj]
	/*canvas=document.getElementById("tableboard-1");
	var ctx = canvas.getContext("2d");
	var canvasOffset = $("#tableboard-1").offset();
	var offsetX = canvasOffset.left;
	var offsetY = canvasOffset.top;
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;*/
	var ctx = canvas.getContext("2d");
	var tokenctx = token.getContext("2d");
	var arbrectx = arbre.getContext("2d");
	var mjctx = mj.getContext("2d");
	var canvasOffset = $("#tableboard-0").offset();
	var offsetX = canvasOffset.left;
	var offsetY = canvasOffset.top;
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	var tokenOffset = $("#tableboard-1").offset();
	var offsetX = tokenOffset.left;
	var offsetY = tokenOffset.top;
	token.width = window.innerWidth;
	token.height = window.innerHeight;

	var arbreOffset = $("#tableboard-2").offset();
	var offsetX = arbreOffset.left;
	var offsetY = arbreOffset.top;
	arbre.width = window.innerWidth;
	arbre.height = window.innerHeight;

	var mjOffset = $("#tableboard-3").offset();
	var offsetX = mjOffset.left;
	var offsetY = mjOffset.top;
	mj.width = window.innerWidth;
	mj.height = window.innerHeight;


	/*text*/
	arbrectx.font = '1em arial';
	arbrectx.fillStyle = 'white';
	arbrectx.textAlign = "right"; 
	arbrectx.fillText(nom_plateau[1], arbre.width-40, 20);

	/*image*/	
	
	for(i=0;i<listescr.length;i++){
		if(calque[i]==0){
			imagerie(listescr[i],0,x[i],y[i],ctx);
		}else if(calque[i]==1){
			imagerie(listescr[i],1,x[i],y[i],tokenctx);
		}else if(calque[i]==2){
			imagerie(listescr[i],1,x[i],y[i],arbrectx);
		}else if(calque[i]==3){
			imagerie(listescr[i],1,x[i],y[i],mjctx);
		}
		
	}
	/*var img = new Image();
	var img2 = new Image();
	    img.onload = function () {
	        ctx.drawImage(img, 100, 100);
	        ctx.drawImage(img2, 100, 100);
	    };
	    img.src =listescr[0];
	    img2.src =listescr[1];

	/*for(i=0;i<listescr.length;i++){
	    var img = new Image();
	    img.onload = function () {
	        ctx.drawImage(img, 100, 100);
	    };
	    img.src =listescr[i];


	    //var isDragging = false;
    }
    // functions to handle mouseup, mousedown, mousemove, mouseout events

    /*$("#tableboard").mousedown(function (e) {
        handleMouseDown(e);
    });
    $("#tableboard").mousemove(function (e) {
        handleMouseMove(e);
    });
    $("#tableboard").mouseup(function (e) {
        handleMouseUp(e);
    });
    $("#tableboard").mouseout(function (e) {
        handleMouseOut(e);
    });*/

}); // end $(function(){});

function imagerie(image,calque,x,y,ctx){
	var img = new Image();
	img.onload = function () {
	    ctx.drawImage(img, x, y);
	};
	img.src =image;
}
/*var plateau = document.getElementById('tableboard');
//plateau.style.width = window.innerWidth - 280 + 'px';
//plateau.style.height = window.innerHeight - 20 + 'px';

if (plateau.getContext) {
  var ctx = plateau.getContext('2d');
  	var img = new Image();
	img.src = 'https://cdnfr1.img.sputniknews.com/images/103083/04/1030830428.jpg';
  	function drawImageScaled(img, ctx) {
	   	var plateau = ctx.plateau ;
	   	var hRatio = plateau.width  / img.width    ;
	   	var vRatio =  plateau.height / img.height  ;
	   	var ratio  = Math.min ( hRatio, vRatio );
	   	var centerShift_x = ( canvas.width - img.width*ratio ) / 2;
	   	var centerShift_y = ( canvas.height - img.height*ratio ) / 2;  
	   	ctx.clearRect(0,0,canvas.width, canvas.height);
	   	ctx.drawImage(img, 0,0, img.width, img.height,centerShift_x,centerShift_y,img.width*ratio, img.height*ratio);  
	}
  // code de dessin dans le plateau
  	/*rectangle :
    ctx.fillRect(25, 25, 100, 100);
    ctx.clearRect(45, 45, 60, 60);
    ctx.strokeRect(50, 50, 50, 50);*/

    /*triangle
    ctx.beginPath();
    ctx.moveTo(75, 50);
    ctx.lineTo(100, 75);
    ctx.lineTo(100, 25);
    ctx.fill();*/

    /*smiley
    ctx.beginPath();
    ctx.arc(75, 75, 50, 0, Math.PI * 2, true);  // Cercle extérieur
    ctx.moveTo(110,75);
    ctx.arc(75, 75, 35, 0, Math.PI, false);  // Bouche (sens horaire)
    ctx.moveTo(65, 65);
    ctx.arc(60, 65, 5, 0, Math.PI * 2, true);  // Oeil gauche
    ctx.moveTo(95, 65);
    ctx.arc(90, 65, 5, 0, Math.PI * 2, true);  // Oeil droite
    ctx.stroke();
    ctx.beginPath();
    ctx.fillStyle = 'rgb(200, 0, 0)';
    ctx.arc(100, 100,15, 0, 2 * Math.PI, false);  // Cercle extérieur ctx.arc(x, y, radius, startAngle, endAngle, anticlockwise);
    ctx.fill();

	/*image
    // créer un nouvel objet image à utiliser comme modèle
	  var img = new Image();
	  img.src = 'https://cdnfr1.img.sputniknews.com/images/103083/04/1030830428.jpg';
	  img.onload = function() {

	    // créer le modèle
	    var ptrn = ctx.createPattern(img, 'no-repeat');
	    ctx.fillStyle = ptrn;
	    ctx.fillRect(0, 0, 80, 80);

	  }*/

	  /*texte
	  var ctx = document.getElementById('plateau').getContext('2d');
	  ctx.font = '48px arial';
	  ctx.fillText('Hello world', 10, 50);*/

	 /*var img = new Image();
	  img.onload = function() {
	    for (var i = 0; i < 4; i++) {
	      for (var j = 0; j < 3; j++) {
	        ctx.drawImage(img, j * 50, i * 38, 50, 38);
	      }
	    }
	  };
	  img.src = 'https://mdn.mozillademos.org/files/5397/rhino.jpg';
	  // Draw slice drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
	  //ctx.drawImage(document.getElementById('source'),33, 71, 104, 124, 21, 20, 30, 40);
	  var img = new Image();
	  img.src = 'https://cdnfr1.img.sputniknews.com/images/103083/04/1030830428.jpg';
	  ctx.drawImage(img, 0, 0, plateau.width, plateau.height)
	  /*save restaure
	  var ctx = document.getElementById('plateau').getContext('2d');

  		ctx.fillRect(0, 0, 150, 150);   // Dessine un rectangle avec les réglages par défaut
		  ctx.save();                  // Sauvegarde l'état par défaut
		 
		  ctx.fillStyle = '#09F';      // Fait des changements de réglages
		  ctx.fillRect(15, 15, 120, 120); // Dessine un rectangle avec les nouveaux réglages

		  ctx.save();                  // Sauvegarde l'état actuel
		  ctx.fillStyle = '#FFF';      // Fait des changements de réglages
		  ctx.globalAlpha = 0.5; 
		  ctx.fillRect(30, 30, 90, 90);   // Dessine un rectangle avec de nouveaux réglages

		  ctx.restore();               // Restaure l'état précédent
		  ctx.fillRect(45, 45, 60, 60);   // Dessine un rectangle avec les réglages restaurés

		  ctx.restore();               // Restaure l'état d'origine
		  ctx.fillRect(60, 60, 30, 30);   // Dessine un rectangle avec les réglages restaurés*/

		  /*deplacement
		  for (var i = 0; i < 3; i++) {
    for (var j = 0; j < 3; j++) {
      ctx.save();
      ctx.fillStyle = 'rgb(' + (51 * i) + ', ' + (255 - 51 * i) + ', 255)';
      ctx.translate(10 + j * 50, 10 + i * 50);
      ctx.fillRect(0, 0, 25, 25);
      ctx.restore();
    }
  }*/

  /*rotation 
  ctx.save();
  // rectangle bleu
  ctx.fillStyle = '#0095DD';
  ctx.fillRect(30, 30, 100, 100); 
  ctx.rotate((Math.PI / 180) * 25);
  // rectangle gris
  ctx.fillStyle = '#4D4E53';
  ctx.fillRect(30, 30, 100, 100);
  ctx.restore();

  // rectangles de droite, rotation depuis le centre du rectangle
  // dessine le rectangle bleu
  ctx.fillStyle = '#0095DD';
  ctx.fillRect(150, 30, 100, 100);  
  
  ctx.translate(200, 80); // déplace au centre du rectangle 
                          // x = x + 0.5 * width
                          // y = y + 0.5 * height
  ctx.rotate((Math.PI / 180) * 25); // rotation
  ctx.translate(-200, -80); // déplace en arrière
  
  // dessine le rectangle gris
  ctx.fillStyle = '#4D4E53';
  ctx.fillRect(150, 30, 100, 100);	*/

  /*scale
  var ctx = document.getElementById('plateau').getContext('2d');

  // dessine un rectangle simple, mais le met à l'échelle.
  ctx.save();
  ctx.scale(10, 3);
  ctx.fillRect(1, 10, 10, 10);
  ctx.restore();

  // mirror horizontally
  ctx.scale(-1, 1);
  ctx.font = '48px serif';
  ctx.fillText('MDN', -135, 120)						

} else {
  // code pour le cas où plateau ne serait pas supporté
}*/