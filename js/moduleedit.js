function calcenco(iduser){
    enco=0;
    $( '#edit-'+iduser+' .block-inventaire .inventaire' ).each(function( index ) {
      poids=$(this).find('.poids input').val();
      qte=$(this).find('.qte input').val();
      enco=enco+parseInt(poids)*parseInt(qte);
    });
    max=$('#edit-'+iduser+' .block-encombrement input').val();
    if( enco>max){
        affichemessagepartie('<h1>Attention</h1><p>Vous dépassez votre encombrement maximun<br><input type="button" value="Valider" class="bouton_save" onclick="fermer_messagepartie()"></p>');
    }
    $( '#edit-'+iduser+'  .block-encombrement span span').html(enco);
}
compte=-100;
function  ajoutcomp(type,idblock,iduser) {
    if(type=="module_comp"){
        $('#edit-'+iduser+' #block'+idblock ).append('<div class="inventaire">'+
        '<span class="nom">'+
            '<span class="nom_comp"><input type="text" class="sans" value="" placeholder="nom"></span>'+
            '<span class="valeur1_comp"><input type="text" class="sans" value="" size="2" placeholder="-"></span>'+
        '</span>'+
                '<span class="suprimer" onclick="javascript: parent=$(this).parent(\'div\');parent.remove();">X</span>'+
                '<span class="modif" onclick="modif_comp('+compte+')">-</span>'+
            '</div>'+
            '<div class="clear"></div>'+
            '<div id="modif'+compte+'" class="modif_comp" style="display:none">'+
                '<div class="margin-left petite"><span class="commande">descriptif :</span><textarea class="sans" placeholder="descriptif"></textarea></div></span>'+
            '</div>');
        compte=compte-1;
    }else if(type=="module_inv"){
        $('#edit-'+iduser+' #block'+idblock).append('<div class="inventaire">'+
                        '<span class="nom">'+
                    '<span class="nom_comp"><input type="text" value="" class="sans" placeholder="nom"></span>'+                    
                    '<span class="valeur1_comp qte"><input type="text" value="" class="sans" size="2" style="text-align:center" placeholder="-" onchange="calcenco('+iduser+')"></span>'+
                    '<span class="valeur1_comp poids"><input type="text" value="" class="sans" size="2" style="text-align:center"  placeholder="-" onchange="calcenco('+iduser+')"></span>'+
                '</span>'+
                '<span class="suprimer" onclick="javascript: parent=$(this).parent(\'div\');parent.remove();">X</span>'+
                '<span class="modif" onclick="modif_comp('+compte+')">-</span>'+
            '</div>'+
           '<div class="clear"></div>'+
           '<div id="modif'+compte+'" class="modif_comp" style="display:none">'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">commande :</span> <input class="sans" type="text" placeholder="/r 1d6+2" value="" style="background:#36393F; padding:3px; width:215px;margin-bottom:5px;" onchange="savemoduleinve()"></div></span>'+
                '<div class="clear"></div>'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">descriptif :</span><textarea class="sans" placeholder="descriptif" onchange="savemoduleinve()"></textarea></div></span>'+
            '</div>');   
        compte=compte-1;
    }else if(type=="module_conn"){
        $('#edit-'+iduser+'  #block'+idblock ).append('<div class="inventaire">'+
                '<span class="nom">'+
                    '<span class="nom_comp"><input type="text" value="" class="sans" placeholder="nom"></span>'+
                    '<span class="valeur1_comp"><input type="text" value="" class="sans" size="2" placeholder="-"></span>'+
                '</span>'+
                '<span class="suprimer" onclick="javascript: parent=$(this).parent(\'div\');parent.remove();">X</span>'+
                '<span class="modif" onclick="modif_comp('+compte+')">-</span>'+
            '</div>'+
            '<div class="clear"></div>'+
            '<div id="modif'+compte+'" class="modif_comp" style="display:none">'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">commande :</span> <input class="sans" type="text" placeholder="/r 1d6+2" value="" style="background:#36393F; padding:3px; width:215px;margin-bottom:5px;"></div></span>'+
                '<div class="clear"></div>'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">descriptif :</span><textarea class="sans" placeholder="descriptif"></textarea></div></span>'+
            '</div>');
        compte=compte-1;
    }
}


$(".suprimer").click(function(){
    parent=$(this).parent('div')
    parent.remove(); 
});
function modif_comp(id){
    $('.modif_comp').hide()
    $('#modif'+id).show();
}


function validmodif(iduser){
    savecolor(iduser)
    a=0;
    $('#edit-'+iduser+' .block-encombrement').each(function() {
        idblock=$( this ).data( "idblock" );
        if(a==0){
            supprcomp(idblock,iduser)
        }
        a++
        nom_comp='module_enco';
        valeur=new Array();
        for(i=1;i<11;i++){
           valeur[i]=null;
        }
        valeur[1]=$('#edit-'+iduser+' .encomin').html();
        valeur[2]=$('#edit-'+iduser+' .block-encombrement input').val();
        
        savemodule(idblock,iduser,nom_comp,valeur[1],valeur[2],valeur[3],valeur[4],valeur[5],valeur[6],valeur[7],valeur[8],valeur[9],valeur[10]);
    });

    a=0;
    $('#edit-'+iduser+' .block-histoire').each(function() {
        idblock=$( this ).data( "idblock" );
        if(a==0){
            supprcomp(idblock,iduser)
        }
        a++
        nom_comp='module_text';       
        valeur=new Array();
        for(i=1;i<11;i++){
            valeur[i]=null;
        }
        valeur[1]=$(this).children('.histoire').val()
        savemodule(idblock,iduser,nom_comp,valeur[1],valeur[2],valeur[3],valeur[4],valeur[5],valeur[6],valeur[7],valeur[8],valeur[9],valeur[10]);
    });
    a=0;
    $('#edit-'+iduser+' .block-stat').each(function() { 
        idblock=$( this ).data( "idblock" );
        if(a==0){
            supprcomp(idblock,iduser)
        }
        if(a>0)
        nom_comp='module_stat';
        valeur=[];
        b=1;
        $(this).find("input").each(function(){
            valeur[b]=($(this).val());
            b++;
        });
        for(i=b;i<11;i++){
            valeur[i]=null;   
        }
        savemodule(idblock,iduser,nom_comp,valeur[1],valeur[2],valeur[3],valeur[4],valeur[5],valeur[6],valeur[7],valeur[8],valeur[9],valeur[10]);
    });
    a=0;
    $('#edit-'+iduser+' .block-compétence').each(function() { 
        idblock=$( this ).data( "idblock" );
        if(a==0){
            supprcomp(idblock,iduser)
        }
        if(a>0)
        nom_comp='module_comp';
        listetitre=[];listetval=[];listedesc=[];
        $(this).find(".nom_comp input").each(function(){
            listetitre.push ($(this).val())
        });
        $(this).find(".valeur1_comp input").each(function(){
            listetval.push ($(this).val())
        });
        $(this).find(".modif_comp textarea").each(function(){
            listedesc.push ($(this).val())
        });        
        for(i=1;i<11;i++){
            valeur[i]=null;   
        }
        fLen = listetitre.length;
        for (i = 0; i < fLen; i++) {
            savemodule(idblock,iduser,nom_comp,listetitre[i],listedesc[i],listetval[i],valeur[4],valeur[5],valeur[6],valeur[7],valeur[8],valeur[9],valeur[10]);
        }
    });
    a=0;
    $('#edit-'+iduser+' .block-liste').each(function() { 
        idblock=$( this ).data( "idblock" );
        if(a==0){
            supprcomp(idblock,iduser)
        }
        if(a>0)
        nom_comp='module_comp';
        listetitre=[];listetval=[];listedesc=[];listecomm=[];
        $(this).find(".nom_comp input").each(function(){
            listetitre.push ($(this).val())
        });
        $(this).find(".valeur1_comp input").each(function(){
            listetval.push ($(this).val())
        });
        $(this).find(".modif_comp textarea").each(function(){
            listedesc.push ($(this).val())
        });
        $(this).find(".modif_comp input").each(function(){
            listecomm.push ($(this).val())
        });         
        for(i=1;i<11;i++){
            valeur[i]=null;   
        }
        fLen = listetitre.length;
        for (i = 0; i < fLen; i++) {
            savemodule(idblock,iduser,nom_comp,listetitre[i],listedesc[i],listetval[i],listecomm[i],valeur[5],valeur[6],valeur[7],valeur[8],valeur[9],valeur[10]);
        }
    });
    a=0;
    $('#edit-'+iduser+' .block-inventaire').each(function() { 
        idblock=$( this ).data( "idblock" );
        if(a==0){
            supprcomp(idblock,iduser)
        }
        if(a>0)
        nom_comp='module_comp';
        listetitre=[];listetval=[];listetpds=[];listedesc=[];listecomm=[];
        $(this).find(".nom_comp input").each(function(){
            listetitre.push ($(this).val())
        });
        $(this).find(".qte input").each(function(){
            listetval.push ($(this).val())
        });
        $(this).find(".poids input").each(function(){
            listetpds.push ($(this).val())
        });
        $(this).find(".modif_comp textarea").each(function(){
            listedesc.push ($(this).val())
        });
        $(this).find(".modif_comp input").each(function(){
            listecomm.push ($(this).val())
        });
        fLen = listetitre.length;
        for (i = 0; i < fLen; i++) {
            savemodule(idblock,iduser,nom_comp,listetitre[i],listedesc[i],listetval[i],listetpds[i],listecomm[i],valeur[6],valeur[7],valeur[8],valeur[9],valeur[10]);
        }
    });
    refershedit(iduser);
    enregistrementfiche(iduser);
}
function  supprcomp(idblock,iduser) {
   $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            supprcomp:'oui',
            idblock:idblock,
            iduser:iduser,
        },
        success: function(content){
            
        },
        fail: function(){
        },
    });
}
function savemodule(idblock,iduser,nom_comp,valeur1,valeur2,valeur3,valeur4,valeur5,valeur6,valeur7,valeur8,valeur9,valeur10){
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                savemodule:'oui',
                idblock:idblock,
                iduser:iduser,
                nom_comp:nom_comp,       
                valeur1:valeur1,
                valeur2:valeur2,
                valeur3:valeur3,
                valeur4:valeur4,
                valeur5:valeur5,
                valeur6:valeur6,
                valeur7:valeur7,
                valeur8:valeur8,
                valeur9:valeur9,
                valeur10:valeur10,
            },
            success: function(content){
                //refershedit(iduser);
            },
            fail: function(){
            },
        });        
}
function refershedit(iduser){
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            refreshedit:'oui',
            iduser:iduser,
        },
        success: function(content){
            synchroedit(iduser,content);
        },
        fail: function(){
        },
    }); 
}
function refersheditseul(iduser){
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            refreshedit:'oui',
            iduser:iduser,
        },
        success: function(content){
             fiche='<input value="X" class="sans" style="position:absolute;top:5px;right:5px;color:red;" type="button" onclick="fermermodif('+iduser+');;refersheditseul('+iduser+');">'+content+
            '<input value="Valider" class="bouton_save" type="button" onclick="validmodif('+iduser+'),fermermodif();"> <input value="Importer" class="bouton_save" type="button" onclick="importation('+iduser+')"> <input value="Exporter" class="bouton_save" type="button" onclick="exportation('+iduser+')"></div>';
            $('#edit-'+iduser).html(fiche)
        },
        fail: function(){
        },
    }); 
}
function enregistrementfiche(iduser){
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            refreshfiche:'oui',
            iduser:iduser,
        },
        success: function(content){
            synchrofiche(iduser,content);
        },
        fail: function(){
        },
    }); 
}

function exportation(iduser){
    tb=[iduser]
    nomedit=$('#nomedit'+iduser).val();
    imgedit=$('#imgedit'+iduser).val();
    encomin=$('#edit-'+iduser+' .encomin').html();
    encomax=$('#edit-'+iduser+' .block-encombrement input').val();
    json='{"name":"'+nomedit+'","img":"'+imgedit+'","encomin":"'+encomin+'","encomax":"'+encomax+'"';
    json+=',"blockavatar":[';
    a=0;
    $('#joueur'+iduser+' .info_pv').each(function() {            
        var titre = $(this).attr('id')
        lg=titre.length;
        lg=parseInt(lg)-   tb.length
        valeur=$(this).val()
        titre=titre.substr(0,lg)
        if(a>0){json+=',';}
        json+= '{"key":"'+titre+'","value":"'+valeur+'"}';
        a++;
    });
    if($('#joueur'+iduser+' .armure').val()!=undefined){
        valeur=$('#joueur'+iduser+' .armure').val()
        json+= ',{"key":"armure","value":"'+valeur+'"}'; 
    }
    if($('#joueur'+iduser+' .armuremagique').val()!=undefined){
        valeur=$('#joueur'+iduser+' .armuremagique').val()
        json+= ',{"key":"armuremagique","value":"'+valeur+'"}'; 
    }
    json+='],"blockhistoire":[';
    a=0;
    $('#edit-'+iduser+' .block-histoire').each(function() {
        if(a>0){json+=',';}
        var titre = $(this).children('h2').html()
        var histoire = $(this).children('.histoire').val()
        json+= '{"key":"'+titre+'","value":"'+histoire+'"}';
        a++;
    });
    json+='],"blockstat":[';a=0;
    $('#edit-'+iduser+' .block-stat').each(function() { 
        if(a>0){json+=',';}   
        var titre = $(this).children('h2.margin-left').html();
        json+='{"block":"'+titre+'","contenu":[';
        stattitre=[];stattval=[];
        $(this).find(".nomstat").each(function(){
            stattitre.push ($(this).html())
        });
        $(this).find("input").each(function(){
            stattval.push ($(this).val())
        });
        fLen = stattitre.length;
        for (i = 0; i < fLen; i++) {
            if(i>0){
                json+=',';
            }
            json += '{"key":"' + stattitre[i] + '","valeur":"'+stattval[i]+'"}';
        }
        json+= "]}";
        a++;
    });
    json+='],"blockcompetence":[';a=0;
    $('#edit-'+iduser+' .block-compétence').each(function() { 
        if(a>0){json+=',';}   
        var titre = $(this).children('h2.margin-left').text();
        json+='{"block":"'+titre+'","contenu":[';
        listetitre=[];listetval=[];listedesc=[];
        $(this).find(".nom_comp input").each(function(){
            listetitre.push ($(this).val())
        });
        $(this).find(".valeur1_comp input").each(function(){
            listetval.push ($(this).val())
        });
        $(this).find(".modif_comp textarea").each(function(){
            listedesc.push ($(this).val())
        });        
        fLen = listetitre.length;
        for (i = 0; i < fLen; i++) {
            if(i>0){
                json+=',';
            }
            json += '{"key":"' + listetitre[i] + '","valeur":"'+listetval[i]+'","descriptif":"'+listedesc[i]+'"}';
        }
        json+= "]}";
        a++;
    });
    json+='],"blockliste":[';a=0;
    $('#edit-'+iduser+' .block-liste').each(function() { 
        if(a>0){json+=',';}   
        var titre = $(this).children('h2.margin-left').text();
        json+='{"block":"'+titre+'","contenu":[';
        listetitre=[];listetval=[];listedesc=[];listecomm=[];
        $(this).find(".nom_comp input").each(function(){
            listetitre.push ($(this).val())
        });
        $(this).find(".valeur1_comp input").each(function(){
            listetval.push ($(this).val())
        });
        $(this).find(".modif_comp textarea").each(function(){
            listedesc.push ($(this).val())
        });
        $(this).find(".modif_comp input").each(function(){
            listecomm.push ($(this).val())
        }); 
        fLen = listetitre.length;
        for (i = 0; i < fLen; i++) {
            if(i>0){
                json+=',';
            }
            json += '{"key":"' + listetitre[i] + '","valeur":"'+listetval[i]+'","descriptif":"'+listedesc[i]+'","commande":"'+listecomm[i]+'"}';
        }
        json+= "]}";
        a++;
    });
    json+='],"blockinventaire":[';a=0;
    $('#edit-'+iduser+' .block-inventaire').each(function() { 
        if(a>0){json+=',';}   
        var titre = $(this).children('h2.margin-left').children('.nomgen').text();
        json+='{"block":"'+titre+'","contenu":[';
        listetitre=[];listetval=[];listetpds=[];listedesc=[];listecomm=[];
        $(this).find(".nom_comp input").each(function(){
            listetitre.push ($(this).val())
        });
        $(this).find(".qte input").each(function(){
            listetval.push ($(this).val())
        });
        $(this).find(".poids input").each(function(){
            listetpds.push ($(this).val())
        });
        $(this).find(".modif_comp textarea").each(function(){
            listedesc.push ($(this).val())
        });
        $(this).find(".modif_comp input").each(function(){
            listecomm.push ($(this).val())
        });
        fLen = listetitre.length;
        for (i = 0; i < fLen; i++) {
            if(i>0){
                json+=',';
            }
            json += '{"key":"' + listetitre[i] + '","qte":"'+listetval[i]+'","poids":"'+listetpds[i]+'","descriptif":"'+listedesc[i]+'","commande":"'+listecomm[i]+'"}';
        }
        json+= "]}";
        a++;
    });
    json+=']}';
    //"conn":[{"key":"Level","value":"1"},{"key":"Race","value":"Dragon"}
   
    text='<h1>Exportation</h1><input type="button" class="btnexport" value="X" onclick="closeexport()"><textarea>'+json+'</textarea>';
    $('#exportation').html(text)
    $('#exportation').css({'display':'block'})
}
function importation(iduser){
    text='<h1>Importation</h1><input type="button" class="btnexport" value="X" onclick="closeexport()"><textarea id="importjson"></textarea><input type="button" style="color:#fff" class="bouton_save" value="Importer" onclick="jsonimport('+iduser+')">';
    $('#exportation').html(text)
    $('#exportation').css({'display':'block'})

}
function jsonimport(iduser){
    $( '#edit-'+iduser ).removeClass("inventaire")
    //remise à zero avant import
    json=$('#importjson').val()
    obj = JSON.parse(json);
    if(obj.name !== 'undefined'){
        $('#nomedit'+iduser).val(obj.name);
        modifeditnom(iduser) 
    }
    if(obj.img !== 'undefined'){
        $('#imgedit'+iduser).val(obj.img);
        modifeditimage(iduser)
    }
    if(obj.encomin !== 'undefined'){
        $('#edit-'+iduser+' .block-encombrement span span').html(obj.encomin);
    }
    if(obj.encomax !== 'undefined'){
         $('#edit-'+iduser+' .block-encombrement input').val(obj.encomax);
    }
    if(obj.blockavatar !== 'undefined'){
        var long = obj.blockavatar.length;
        for (var i=0; i<long; i++){ 
            if(obj.blockavatar[i].key=="armure"){
                $('#joueur'+iduser+' .armure').val(obj.blockavatar[i].value)
            }else if(obj.blockavatar[i].key=="armuremagique"){
                $('#joueur'+iduser+' .armuremagique').val(obj.blockavatar[i].value)
            }else {
                $('#'+obj.blockavatar[i].key+iduser).val(obj.blockavatar[i].value)
                if ((i % 2) !== 0) {
                    modifbar(obj.blockavatar[i].key,iduser)
                }
            }            
        }
    }
    if(obj.blockhistoire !== 'undefined'){
        var long = obj.blockhistoire.length;
        for (var i=0; i<long; i++){ 
            $('#edit-'+iduser+' #edition'+obj.blockhistoire[i].key+' textarea').val(obj.blockhistoire[i].value)
        }
    }
    if(obj.blockstat !== 'undefined'){
        var long = obj.blockstat.length;
        for (i=0; i<long; i++){ 
            iddiv=obj.blockstat[i].block;
            var lg = obj.blockstat[i].contenu.length;
            for (j = 0; j <lg; j++) {
                $('#edit-'+iduser+' #nomstatedit'+iddiv+' #statedit'+obj.blockstat[i].contenu[j].key+" input").val(obj.blockstat[i].contenu[j].valeur)  
            }       
        }
    }

    if(obj.blockcompetence !== 'undefined'){
        var long = obj.blockcompetence.length;
        for (i=0; i<long; i++){ 
            iddiv=obj.blockcompetence[i].block;
            var lg = obj.blockcompetence[i].contenu.length;
            html=''
            for (j = 0; j <lg; j++) {
                html+='<div class="inventaire"><span id="idsql-" class="nom">'+
                    '<span class="nom_comp"><input type="text" class="sans" value="'+obj.blockcompetence[i].contenu[j].key+'" placeholder="nom"></span>'+
                    '<span class="valeur1_comp"><input type="text" class="sans" value="'+obj.blockcompetence[i].contenu[j].valeur+'" size="2" placeholder="-"></span>'+
                '</span>'+
                ' <span class="suprimer" onclick="javascript: parent=$(this).parent(\'div\');parent.remove();">X</span>'+
                '<span class="modif" onclick="modif_comp('+compte+')">-</span>'+
                '</div>'+
                '<div class="clear"></div>'+
                '<div id="modif'+compte+'" class="modif_comp" style="display:none">'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">descriptif :</span><textarea class="sans" placeholder="descriptif">'+obj.blockcompetence[i].contenu[j].descriptif+'</textarea></div></span>'+
                '</div></div>';
                compte=compte-1; 
            }  
            html+='</div>';
            $('#edit-'+iduser+' #nomcompedit'+iddiv+' .competenceliste').html(html)
        }
    }
    if(obj.blockliste !== 'undefined'){
        var long = obj.blockliste.length;
        for (i=0; i<long; i++){ 
            iddiv=obj.blockliste[i].block;
            var lg = obj.blockliste[i].contenu.length;
            html=''
            for (j = 0; j <lg; j++) {
                html+='<div class="inventaire"><span id="idsql-" class="nom">'+
                    '<span class="nom_comp"><input type="text" class="sans" value="'+obj.blockliste[i].contenu[j].key+'" placeholder="nom"></span>'+
                    '<span class="valeur1_comp"><input type="text" class="sans" value="'+obj.blockliste[i].contenu[j].valeur+'" size="2" placeholder="-"></span>'+
                '</span>'+
                ' <span class="suprimer" onclick="javascript: parent=$(this).parent(\'div\');parent.remove();">X</span>'+
                '<span class="modif" onclick="modif_comp('+compte+')">-</span>'+
                '</div>'+
                '<div class="clear"></div>'+
                '<div id="modif'+compte+'" class="modif_comp" style="display:none">'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">commande :</span> <input class="sans" type="text" placeholder="/r 1d6+2" value="'+obj.blockliste[i].contenu[j].commande+'" style="background:#36393F; padding:3px; width:215px;margin-bottom:5px;"></div></span>'+
                '<div class="clear"></div>'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">descriptif :</span><textarea class="sans" placeholder="descriptif">'+obj.blockliste[i].contenu[j].descriptif+'</textarea></div></span>'+
                '</div></div>'; 
                compte=compte-1;
            }  
            html+='</div>';
            $('#edit-'+iduser+' #nomconnedit'+iddiv+' .connaissanceliste').html(html)
        }
    }
    if(obj.blockinventaire !== 'undefined'){
        var long = obj.blockinventaire.length;
        for (i=0; i<long; i++){ 
            iddiv=obj.blockinventaire[i].block;
            var lg = obj.blockinventaire[i].contenu.length;
            html=''
            for (j = 0; j <lg; j++) {
                html+='<div class="inventaire"><span id="idsql-" class="nom">'+
                    '<span class="nom_comp"><input type="text" class="sans" value="'+obj.blockinventaire[i].contenu[j].key+'" placeholder="nom"></span>'+
                    '<span class="valeur1_comp qte"><input type="text" class="sans" value="'+obj.blockinventaire[i].contenu[j].qte+'" size="2" placeholder="-" onchange="calcenco('+iduser+')"></span>'+
                    '<span class="valeur1_comp poids"><input type="text" class="sans" value="'+obj.blockinventaire[i].contenu[j].poids+'" size="2" placeholder="-" onchange="calcenco('+iduser+')"></span>'+
                '</span>'+
                ' <span class="suprimer" onclick="javascript: parent=$(this).parent(\'div\');parent.remove();">X</span>'+
                '<span class="modif" onclick="modif_comp('+compte+')">-</span>'+
                '</div>'+
                '<div class="clear"></div>'+
                '<div id="modif'+compte+'" class="modif_comp" style="display:none">'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">commande :</span> <input class="sans" type="text" placeholder="/r 1d6+2" value="'+obj.blockinventaire[i].contenu[j].commande+'" style="background:#36393F; padding:3px; width:215px;margin-bottom:5px;"></div></span>'+
                '<div class="clear"></div>'+
                '<div class="margin-left petite"><span style="height:20px;display:block;float:left;">descriptif :</span><textarea class="sans" placeholder="descriptif">'+obj.blockinventaire[i].contenu[j].descriptif+'</textarea></div></span>'+
                '</div></div>'; 
                compte=compte-1;
            }  
            html+='</div>';
            $('#edit-'+iduser+' #nominvedit'+iddiv+' .competenceliste').html(html)
        }
    }
    closeexport()
    calcenco(iduser)
}
function closeexport(){
    $('#exportation').css({'display':'none'})
}
/*blockliste":[{"block":"Carateristique ","contenu":[{"key":"Level","valeur":"1"},{"key":"php","valeur":"99"}]},{"block":"Connaissance ","contenu":[{"key":"PHP infuse","valeur":"10"}]},{"block":"Dons ","contenu":[{"key":"Savoir infini","valeur":"5"}]}],"blockinventaire":[{"block":"Arme","contenu":[{"key":"Haches de bataille","qte":"1","poids":"20"},{"key":"couteau","qte":"2","poids":"1"}]},{"block":"Inventaire","contenu":[{"key":"Gourde","qte":"1","poids":"1"}]}]}*/