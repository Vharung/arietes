function savePoint($idpartie) {
    $.ajax({
        type: 'POST',
        url: "./controllers/controllersave.php",
        data: {
            id_player: $idpartie,
            point:$('#pointequipe').val(),
            savepoint: 'oui',
        },
        dataType: 'html',
        async: false
    })
    .fail(function () {
        alert("erreur sauvegarde pointequipe.js ligne 14");
    })
    .done(function (data) {
        //Synchronisation à faire
        synchopoint($('#pointequipe').val());          
    })
}