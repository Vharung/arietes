//refresh du tchat
function refreshtchat() {
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
	            refreshtchat:'oui',
		    },
            statusCode: {
                404: function() {
                	$('#tchat').html('Tchat non trouvé');
                },
                500: function() {
                	$('#tchat').html('Tchat hors connection');
                }
            },
            success: function(content){
                $('#tchat').html(content);
            },
            fail: function(content){
                alert("Echec AJAX : "+content);
            },
    });
}
//refresh du posture
function refreshposture(compte,name,id) {
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                saveposture:'oui',
                posture:compte,
                name:name,
                id_player:id,
                color1:$('#couleurprincipale'+id).val(),
                color2:$('#couleursecondaire'+id).val(),
            },
            success: function(){
            },
            fail: function(content){
                alert("Echec AJAX : "+content);
            },
    });
}
//refresh du cache
cacheactive=0;
function refreshcacher(){
    cacheactive=1;
    $('#cache').css({'color':'#ffa500'});
    setTimeout(function () {
       cacheactive=0;
       $('#cache').css({'color':'#ffffff'});
    }, 5000)
}
//refresh du tour
function refreshtour(id){
    console.log('refreshtour'+id)
    $('.tourinfo').css({'left':'225px'});
    $('#tour'+id).css({'left':'280px'});
    couleur=$('#tour'+id).css('background-color');
    name=$('#joueur'+id+' input.nom').val();
    lettre1=name.substring(0, 1);
    if(lettre1=='a' || lettre1=='e' || lettre1=='i' || lettre1=='o' || lettre1=='u' || lettre1=='y' || lettre1=='A' || lettre1=='E' || lettre1=='I' || lettre1=='O' || lettre1=='U' || lettre1=='Y' ){
        d='d\'';
    }else {
        d='de ';
    }
    messagedes('<span style="color:'+couleur+'">Au tour '+d+name+'</span>');
    var audio = new Audio('../sound/avosordre.mp3');
    audio.play();
}

//module avantage
function changeavant(avant,id){
   $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                saveavant:'oui',
                avant:avant,
                id_player:id,
                name:$('#joueur'+id+' input.nom').val(),
                color1:$('#couleurprincipale'+id).val(),
                color2:$('#couleursecondaire'+id).val(),
            },
            success: function(){
                synchroavant(avant,id)
            },
            fail: function(content){
                alert("Echec AJAX : "+content);
            },
    }); 
}
function refreshavant(avant,iduser){
    $('#joueur'+iduser+' .avantage').css({"background":"url('img/avantage-"+avant+".png')","background-size":"cover"});
    for(i=0;i<4;i++){
         $('#fiche-'+iduser+' .avant-'+i).removeClass('activepost');
    }
    name=$('#joueur'+iduser+' input.nom').val();
    color1=$('#couleurprincipale'+iduser).val();
    $('#fiche-'+iduser+' .avant-'+avant).addClass('activepost');
    if (avant == 1) {
        avantnom="un avantage";
        color='#32CD32';
        setTimeout(function() {
            var audio = new Audio('../sound/bonus.wav');
            audio.play();
        }, 500);
    } else if (avant == 2) {
        avantnom="un desavantage";
        color='#800000';
        setTimeout(function() {
            var audio = new Audio('../sound/malus.wav');
            audio.play();
        }, 500);
    }else {
        avantnom="aucun avantage";
        color='#ffffff';
    } 
    msg="<span style='color:"+color1+"'>"+name+"<span><span style='color:"+color+"'> a "+avantnom+"</span>";
    messagedes(msg)   
}
//module statut
function changeetat(avant,id){
   $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                saveetat:'oui',
                avant:avant,
                id_player:id,
                name:$('#joueur'+id+' input.nom').val(),
                color1:$('#couleurprincipale'+id).val(),
                color2:$('#couleursecondaire'+id).val(),
            },
            success: function(){
                synchroetat(avant,id)
            },
            fail: function(content){
                alert("Echec AJAX : "+content);
            },
    }); 
}
function refreshetat(etat,iduser){
    $('#joueur'+iduser+' .effet').css({"background":"url('img/etat-"+etat+".png')","background-size":"cover"});
    for(i=0;i<4;i++){
         $('#fiche-'+iduser+' .avant-'+i).removeClass('activepost');
    }
    name=$('#joueur'+iduser+' input.nom').val();
    color1=$('#couleurprincipale'+iduser).val();
    $('#fiche-'+iduser+' .etat-'+etat).addClass('activepost');
    etatnom=["est en forme","est brulé","a cassé son arme","s'asphyxie","est furieux","est aveugle","est blessé","est mourant","a cassé son bouclier"]
    msg="<span style='color:"+color1+"'>"+name+"<span> "+etatnom[etat];
    messagedes(msg)   
}