function message(title,msg,bt1){
	messages='<h1>'+title+'</h1>';
	messages+='<p>'+msg+'</p>';
	messages+='<input type="button" class="btn_msg" value="'+bt1+'" onclick="fermer_message();">';
	affichemessage(messages);
}
function affichemessage(msg){
	messages='<input type="button" value="X" title="Fermer" class="close" onclick="fermer_message();">'+msg;
	$('#message').html(messages)
	var audio = new Audio('../sound/notif.mp3');
    audio.play();
	$('#message').css({'display': 'block'});
}
function fermer_message() {
	$('#message').html('')
    $('#message').css({'display': 'none'});
}
function affichemessagepartie(msg){
    messages='<input type="button" value="X" title="Fermer" class="close" onclick="fermer_messagepartie();">'+msg;
    $('#messagepartie').html(messages)
    var audio = new Audio('../sound/notif.mp3');
    audio.play();
    $('#messagepartie').css({'display': 'block'});
}
function fermer_messagepartie() {
    $('#messagepartie').html('')
    $('#messagepartie').css({'display': 'none'});
}
function oublie(url){
	messages="<h1>Mot de passe oublié </h1><form method='post' action='"+url+"'><p><input type='email' name='recup_mail' style='color:#000; float:none' placeholder='Email'></p><input style='float:none' type='submit' value='Valider' name='recup_submit'/></span></form>'";
	affichemessage(messages);
}
function supprimerpartie(url){
	messages="<h1>Supprimer la partie ? </h1><form method='post' action='"+url+"'><p><input style='float:none' class='btn_msg' type='submit' value='Valider' name='recup_submit'/><input  class='btn_msg' style='float:none' type='button' value='Annuler' name='recup_submit'/></span></form>'";
	affichemessage(messages);
}
function videtchat(url){
    messages='<h1>Vider le tchat</h1><input type="button" value="Oui" class="btn_msg" onclick="location.href=\'index.php?url=liste&idpartie='+url+'&videtchat=oui\'"><input type="button" value="Non" class="btn_msg" onclick="fermer_message();">';
    affichemessage(messages);
}
var fiche_ouverte = 0;
var ouverte = 0;
function agrandissement() {
    $("#fiches").css({'-webkit-transform': 'translate(0px,-400px)', 'transition-duration': '1s'});
    ouverte = 1;
}
function diminusion() {
    $("#fiches").css({'-webkit-transform': 'translate(0px,0px)', 'transition-duration': '1s'});
    ouverte = 0;
}
newmsg=0;
function chargerfiche(fiche) {
    newmsg=0;
    
    if (ouverte == 0) {
        agrandissement();
        ouverte = 1;
    } else if (ouverte == 1 && fiche_ouverte == fiche) {
        diminusion();
    }
    $('article').hide();
    $('#fiche-' + fiche).show();
    $("li").removeClass("active_fiche");
    $("li#fiche_joueur" + fiche).addClass("active_fiche");
    fiche_ouverte = fiche;
    if(fiche==0 && ouverte==1){
        newmsg=0;
    }
    if(fiche=='c'){
        $('#ouverturefiche').addClass("active");
        derniermessage();
    }else {
        $('#ouverturefiche').removeClass("active");
    }

}
info=0;
function agrandirinfo(){
    if(info==0){
        $("#infocollone").css({'-webkit-transform': 'translate(-320px,0px)', 'transition-duration': '1s'});
        $('#reduire').val('Reduire');
        info = 1;
    }else {
        $("#infocollone").css({'-webkit-transform': 'translate(0px,0px)', 'transition-duration': '1s'});
        $('#reduire').val('Agrandir');
        info = 0;
    }
   
}
function infodroite(id){
    for(i=0;i<7;i++){
        $('#info-'+i).css({'display':'none'});
    }
    $('#info-'+id).css({'display':'block'});
    info=0;
    agrandirinfo();
}
function ouvremodif(){
    $('#editfiche').css({'display':'block'});
}
function messagedes(msg) {
    $('#messagedes').html(msg);
    setTimeout(function () {
        $('#messagedes').css({'transform': ' scale(0)', 'transition-duration': '2s', 'opacity': '0'});
    }, 3000);
    $('#messagedes').css({'transform': ' scale(5)', 'transition-duration': '2s', 'opacity': '1'});
}
function ouvremodif(id){
    $('#editfiche').css({'display':'block'});
    $('#joueuredit'+id).css({'display':'block'});
    $('#edit-'+id).css({'display':'block'});
}
function fermermodif(iduser){
    $('#editfiche').css({'display':'none'});
    $('.id_fiche_edit').css({'display':'none'});
    $('.avatar_edit').css({'display':'none'});
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                refreshfiche:'oui',
                iduser:iduser,
            },
            success: function(content){
                synchrofiche(iduser,content)
            },
            fail: function(){
            },
    });
}
function modif(id){
    $('.block-edition .modif').css({'display':'none'});
    $('.block-edition #modif-'+id).css({'display':'block'});
}
$( function() {
    $('#messagepartie').draggable();
    $('#message').draggable();
    $('#editfiche').draggable();
});

function affichemenuplateau(id){
    for(i=1;i<5;i++){
        $('#menu'+i).removeClass('active');
        $('#panel'+i).css({'display':'none'});
    }
    $('#menu'+id).addClass('active');
    $('#panel'+id).css({'display':'block'});
}
function affichemenuobjet(id){
      for(i=10;i<14;i++){
        $('#menu'+i).removeClass('active');
        $('#panel'+i).css({'display':'none'});
    }
    $('#menu'+id).addClass('active');
    $('#panel'+id).css({'display':'block'});
}
function affichemenuotoken(id){
      for(i=20;i<23;i++){
        $('#menu'+i).removeClass('active');
        $('#panel'+i).css({'display':'none'});
    }
    $('#menu'+id).addClass('active');
    $('#panel'+id).css({'display':'block'});
}  
