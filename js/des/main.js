"use strict";

function dice_initialize(container) {
    $t.remove($t.id('loading_text'));
    var canvas = $t.id("canvas_des");
    //canvas = $t.id('canvas');//bug conflits
    canvas.style.width = window.innerWidth - 285 + 'px';
    canvas.style.height = window.innerHeight - 70 + 'px';
    var label = $t.id('label');
    var set = $t.id('set');
    var selector_div = 1;
    on_set_change();

    $t.dice.use_true_random = false;

     function on_set_change(ev) { set.style.width = set.value.length + 3 + 'ex'; }
    

    $t.bind($t.id('clear'), ['mouseup', 'touchend'], function(ev) {
        ev.stopPropagation();
        set.value = '0';
        on_set_change();
    });

    var box = new $t.dice.dice_box(canvas, { w: 500, h: 300 });
    box.animate_selector = false;

    $t.bind(window, 'resize', function() {
        canvas.style.width = window.innerWidth - 285 + 'px';
        canvas.style.height = window.innerHeight - 70 + 'px';
        box.reinit(canvas, { w: 500, h: 300 });
    });

    function show_selector() {
        //selector_div.style.display = 'inline-block';
        //box.draw_selector();
        canvas.style.display = 'none';
    }

    function before_roll(vectors, notation, callback) {
       
        canvas.style.display = 'block';

        selector_div=0;
        // do here rpc call or whatever to get your own result of throw.
        // then callback with array of your result, example:
        // callback([2, 2, 2, 2]); // for 4d6 where all dice values are 2.
        callback(calldede);
    }

    function notation_getter() {
        return $t.dice.parse_notation(set.value);
    }

    function after_roll(notation, result) {
        //resultRoll(result);
        //message('<span style="color:black">' + $('#form_nom_joueur').val() + ' </span><br>' + total, 'gold');
        var res = result.join(' ');
        if (notation.constant) res += ' +' + notation.constant;
        if (result.length > 1) res += ' = ' + 
                (result.reduce(function(s, a) { return s + a; }) + notation.constant);
        //label.innerHTML = res;

    }

    //box.bind_mouse(container, notation_getter, before_roll, after_roll);
    box.bind_throw($t.id('dede'), notation_getter, before_roll, after_roll);
    $t.bind(container, ['click'], function(ev) {
        ev.stopPropagation();
        if (selector_div== 0) {
            if (!box.rolling) show_selector();
            box.rolling = false;
            return;
        }
        var name = box.search_dice_by_mouse(ev);
        if (name != undefined) {
            var notation = $t.dice.parse_notation(set.value);
            notation.set.push(name);
            set.value = $t.dice.stringify_notation(notation);
            on_set_change();
        }
    });

    var params = $t.get_url_params();
    if (params.notation) {
        set.value = params.notation;
    }
    if (params.roll) {
        $t.raise_event($t.id('dice'), 'mouseup');
    }
    else {
        show_selector();
    }
}
