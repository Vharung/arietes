function visibledoc(joueurvue,listejoueur,iddoc){
    listejoueur = listejoueur.split("|");
    joueurvue = joueurvue.split("|");
    message='<h1>Rendre visible pour : </h1>'+
    '<p>';
    for (i = 0; i < listejoueur.length; i++) {
        visible=false;
        opacity='0.5';
        for(j=0;j<joueurvue.length; j++){
            if(listejoueur[i]==joueurvue[j]){
                visible=true;
                opacity='1';
            }
        }
        message+='<span class="listejoueur">'+$('#joueur'+listejoueur[i]+' input.nom').val()+
        '<span class="visibledoc" id="visibledoc'+listejoueur[i]+'" value="'+listejoueur[i]+'" style="opacity:'+opacity+'" onclick="listevisible('+listejoueur[i]+','+iddoc+')"></span>'+
        '</span>';
    }
    message+=''+
    '<input class="bouton_save" type="button" value="Validez" onclick="fermer_messagepartie();"></p>';
    affichemessagepartie(message);
    
}
function listevisible(id,iddoc){
    test=$('#visibledoc'+id).css('opacity')
    if(test==1){
        $('#visibledoc'+id).css({'opacity':'0.5'});
    }else {
        $('#visibledoc'+id).css({'opacity':'1'});
    }  
    listevisibles='';  
    i=0;
    $(".visibledoc").each(function() {
        if($(this).css('opacity')==1){
            if(i>0){
                listevisibles+='|';
            }
            listevisibles+=$(this).attr('value');
            i++;
        }
    });
    if(listevisibles==''){
        listevisibles=null;
    }
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            savevisdoc:'oui',
            iddoc:iddoc,
            vis:listevisibles,
        },
        success: function(){
            synchodocvis();
        },
        fail: function(){
        },
    });
}
function changecontenu(id){
    titre=$('#titredoc-'+id).val();
    contenu=$('#doccontenu-'+id).val();
    contenumj=$('#doccontenumj-'+id).val();
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            savedoc:'oui',
            iddoc:id,
            titre:titre,
            contenu:contenu,
            contenumj:contenumj,
        },
        success: function(){
            synchodocvis();
        },
        fail: function(){
        },
    });
}
function nouveaudoc(){
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            newdoc:'oui',
        },
        success: function(){
           synchodocvis();
        },
        fail: function(){
        },
    });
}
function validsupprdoc(id){
      message='<h1>Validation suppression</h1>'+
      '<p><input class="bouton_save" type="button" value="Valider" onclick="supprdoc('+id+');"> <input class="bouton_save" type="button" value="Annuler" onclick="fermer_messagepartie();"></p>';
    affichemessagepartie(message);
}
function supprdoc(id){
    fermer_messagepartie();
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            supprdoc:'oui',
            iddoc:id,
        },
        success: function(){
            synchodocvis();
        },
        fail: function(){
        },
    });
}
function refreshdoc(){
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        dataType: 'html',
        data: {
            refreshdoc:'oui',
        },

        success: function(content){
            result=content.substr(52)
            $('#info-2').html(result);
        },
        fail: function(){
        },
    });
}