/*function ajoutmusic(iduser){
    msg="<h1>Ajout de musique</h1><p>"+
    "<input type='text' value='' id='dossier' placeholder='Dossier' class='sans newmusic'> / <input type='text' id='sousdossier' value='' placeholder='Sous-Dossier' class='sans newmusic'>"+
    "/ <input type='text' id='nommusic' value='' placeholder='nom' class='sans newmusic'><br>"+
    "<input type='text' id='urlmusic' value='' placeholder='url - youtube' class='sans newmusic2'>"+
    "<input type='button' class='bouton_save' value='Ajouter' onclick='ajoutmusique("+iduser+")'> <input type='button' class='bouton_save' value='Annuler' onclick='fermer_messagepartie();'> </p>";
    affichemessagepartie(msg);
}*/
function ajoutmusique(){
    urlvideo=$('#urlmusic').val();
    srcmusic=$('#srcmusic').val();
    if(srcmusic=="Youtube"){
        urlvideo=urlvideo.substring(32);
        dossier=$('#dossier').val();  
        sousdossier=$('#sousdossier').val();
        nommusic =$('#nommusic').val();
        if(sousdossier!="" && dossier==""){
            message('Alert','Dossier vide','Valider');
        }
        if(nommusic==""){
            message('Alert','Nom vide','Valider');
        }else {
            $.ajax({
                type: "POST",
                url: "./controllers/controllersave.php",
                async:true,
                ifModified:true,
                data: {
                    savemusic:'oui',
                    dossier:dossier,
                    sousdossier:sousdossier,
                    nommusic:nommusic,
                    url:urlvideo,
                    scr:'youtube',
                },
                success: function(content){
                    $('#embed').html(content);
                    $('#embed4').html(content);
                    $('#dossier').val('');
                    $('#sousdossier').val('');
                    $('#nommusic').val('');
                    $('#urlmusic').val('');
                },
                fail: function(content){
                    alert("Echec AJAX : "+content);
                },
            });
        }
    }else{
        message('Alert','Source de musique invalide','Valider')
    }       
}  
function validsuprmus(id){
    msg="<h1>Supprimer la musique</h1><p>"+
    "<input type='button' class='bouton_save' value='Valider' onclick='suprmus("+id+")'> <input type='button' class='bouton_save' value='Annuler' onclick='fermer_messagepartie();'> </p>";
    affichemessagepartie(msg);
}
function suprmus(){
    id=$('#embed4 option:selected').attr('idmusic');
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            suprmusic:'oui',
            id:id,
        },
        success: function(content){
            for(i=0;i<5;i++){
                $('#embed'+i).html(content);
            }
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    });     
} 
function synchromus(idvideo,action,value){
    $('#video'+idvideo).embedplayer(action);
    if(action=='seek'){
         $('#video'+idvideo).embedplayer('seek', +this.value);
    }
   
}

/*nouveau moteur musique*/
idvideogen=1;
function initEmbed() {/*devrait etre adapté au idmusique*/
    $(this).on('embedplayer:statechange', function (event) {
        $('#state').text(event.state);
    }).on('embedplayer:error', function (event) {
        var message = event.error||'';
        if (event.title)        { message += " "+event.title; }
        else if (event.message) { message += " "+event.message; }
        $('#error').text(message);
    }).on('embedplayer:durationchange', function (event) {/*les musiques ce melange*/
        console.log('initEmbed'+idvideogen)

        if (isFinite(event.duration)) {
            $('#currenttime'+idvideogen).show().prop('max', event.duration);
        }
        else {
            $('#currenttime'+idvideogen).hide();
        }
        /*Convertion des seconds en autre unité*/
         secondes=event.duration.toFixed(2);
        var temps=new Date();
        temps.setTime(secondes*1000);
        heure=temps.getHours()-1;
        if(heure==0){
            heure='';
        }else{
            heure=heure+':';
        }
        minutes=temps.getMinutes();
        if(minutes==0){
            minutes='00:';
        }else if(minutes<10){
            minutes='0'+minutes+':';
        }else {
            minutes=minutes+':';
        }
        seconds=temps.getSeconds();
        if(seconds==0){
            seconds='';
        }else if(seconds<10){
            seconds='0'+seconds;
        }else {
            seconds=seconds;
        }
        $('#duration'+idvideogen).text(heure+minutes+seconds);
        //$('#duration'+idvideogen).text(event.duration.toFixed(2)+' seconds');
    }).on('embedplayer:timeupdate', function (event) {
        $('#currenttime'+idvideogen).val(event.currentTime);
        /*Convertion des seconds en autre unité*/
        secondes=event.currentTime.toFixed(2);
         var temps=new Date();
        temps.setTime(secondes*1000);
        heure=temps.getHours()-1;
        minutes=temps.getMinutes();
        if(heure==0){
            heure='';
        }else{
            heure=heure+':';
        }
        minutes=temps.getMinutes();
        if(minutes==0){
            minutes='00:';
        }else if(minutes<10){
            minutes='0'+minutes+':';
        }else {
            minutes=minutes+':';
        }
        seconds=temps.getSeconds();
        if(seconds==0){
            seconds='';
        }else if(seconds<10){
            seconds='0'+seconds;
        }else {
            seconds=seconds;
        }
        $('#currenttime-txt'+idvideogen).text(heure+minutes+seconds);
        //$('#currenttime-txt'+idvideogen).text(event.currentTime.toFixed(2)+' seconds');
    }).on('embedplayer:volumechange', function (event) {
        $('#volume'+idvideogen).val(event.volume);
        $('#volume-label'+idvideogen).text(
            event.volume <=   0 ? '🔇' :
            event.volume <= 1/3 ? '🔈' :
            event.volume <= 2/3 ? '🔉' :
                                  '🔊'
        );
        $('#volume-txt'+idvideogen).text(event.volume.toFixed(2));
    }).on('embedplayer:ready', function (event) {
        var link = $(this).embedplayer('link');
        if (link) {
            $('#link'+idvideogen).attr('href', link);
            $('#link-wrapper'+idvideogen).show();
        }
        var volume = +$('#volume'+idvideogen).val();
        $(this).embedplayer('volume', volume);
        $('#volume-txt'+idvideogen).text(volume.toFixed(2));
    }).
    embedplayer("listen");
}



function loadVideo(tag, url,idvideo) {
    idvideogen=idvideo;
    try {
        var attrs = {
            id: 'video'+idvideo,
            src: url
        };
        switch (tag) {
        case 'iframe'+idvideo:
            attrs.allowfullscreen = 'allowfullscreen';
            attrs.frameborder = '0';
            attrs.width = '300';
            attrs.height = '200';
            // Twitch now needs the embedding site's origin passed as an parameter
            if (/^https?:\/\/player\.twitch\.tv/.test(attrs.src)) {
                attrs.src += '&origin=' + encodeURIComponent(location.origin || location.protocol + '//' + location.host);
            }
            break;

        case 'video'+idvideo:
            attrs.width = '640';
            attrs.height = '360';
            attrs.idvideo=idvideo;/*ajout*/
        case 'audio'+idvideo:
            attrs.controls = 'controls';
            attrs.preload = 'auto';
            break;
        }
        $('#link-wrapper'+idvideo).hide();
        $('<'+tag+'>').attr(attrs).replaceAll('#video'+idvideo).each(initEmbed); 
        
    }
    catch (e) {
        $('#error'+idvideo).text(String(e));
    }
}

function updateVideo (idvideo,valeur,source,urlmusic) {
    console.log('updateVideo '+idvideo+' '+urlmusic )
    var value = valeur.split('|');
    loadVideo(value[0], value[1],idvideo);
    if(source=="youtube"){
        $('#apercu'+idvideo).css({'background':'url(\'https://img.youtube.com/vi/'+urlmusic+'/0.jpg\') top left','background-size':'cover'});
    }
    /*save dans bdd playlist*/
    if(idplayer==idmj){
        /*$.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                changeidvideo:'oui',
                id:idvideo,
                lecture:'1',
            },
            success: function(content){
            },
            fail: function(content){
                alert("Echec AJAX : "+content);
            },
        });*/
    }
}

function rechmus(id) {/*upload via bdd selon idpartie et idmusique dans playlist*/
    v=$('#embed'+id).val();
    var srcmusic=$('#embed'+id+' option:selected').attr('type');
    if(srcmusic=="youtube"){
        var urlmusic=$('#embed'+id+' option:selected').attr('url');
    }
    $('#duration'+id+', #currenttime'+id+', #volume'+id+'').text('?');
    $('#state'+id).text('loading...');
    $('#error'+id).text('');
    updateVideo(id,v,srcmusic,urlmusic);
    idvideogen=i;
}

$( document ).ready(function() {
   for(i=1;i<5;i++){
        rechmus(i);
    } 
});






