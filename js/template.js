function supprligne(id){
	$('#ligne'+id).remove();
}
ligne=0;
function addstats() {
	$(".div-stats").append('<div id="ligne'+ligne+'" class="option_3colonne">Nom : <input class="template-stats-text" type="text" name="stats-name"> <span class="supprligne" onclick="supprligne('+ligne+')">X</span></div>');
	ligne=ligne-1;
}
function addstats_sec() {
	$(".div-stats_sec").append('<div id="ligne'+ligne+'" class="option_3colonne">Nom : <input class="template-stats-text" type="text" name="stats-name_sec"> <span class="supprligne" onclick="supprligne('+ligne+')">X</span></div>');
	ligne=ligne-1;
}
function addmodule() {
	$(".div-module").append('<div id="ligne'+ligne+'"><div class="option_3colonne"><span>Nom : </span><input class="name-12-13-14-16" type="text" value="" size="10"></div>'+
        '<div class="option_3colonne">'+
           '<span> Type : </span><select class="type-12-13-14-16">'+
                '<option value="12">Text</option>'+
                '<option value="13">Comp.</option>'+
                '<option value="16">Liste</option>'+
                '<option value="14">Inven.</option>'+
            '</select></div>'+
        '<div class="option_3colonne"><span> Col. : </span>'+
        '<select class="colonne-12-13-14-16">'+
            '<option>1</option>'+
            '<option>2</option>'+
            '<option>3</option>'+
            '<option>4</option>'+
        '</select>'+
        '<select class="position-12-13-14-16">'+
            '<option>0</option>'+
            '<option>1</option>'+
            '<option>2</option>'+
            '<option>3</option>'+
            '<option>4</option>'+
            '<option>5</option>'+
            '<option>6</option>'+
            '<option>7</option>'+
            '<option>8</option>'+
            '<option>9</option>'+
        '</select> <span class="supprligne" onclick="supprligne('+ligne+')">X</span>'+
        '</div></div>');
	ligne=ligne-1;
}

function addbar() {
	$(".div-bar").append('<div id="ligne'+ligne+'"><div class="option_2colonne"><span>Nom : </span><input type="text" name="bar-name" placeholder="nom de la bar" value=""></div><div class="option_2colonne"><span>Couleur : </span> <span class="supprligne" onclick="supprligne('+ligne+')">X</span><input type="color" name="bar-color" value=""></div><div class="clear"></div></div>');
	ligne=ligne-1;
}
function chargetemplate(){
	idtpl=$('#mestpl').val();
	document.location.href='index.php?url=creertemplate&template='+idtpl;;
	
}
$( document ).ready(function() {
    $('input[type=checkbox]').click(function() {
		if($(this).attr('checked')=='checked'){
	  		$(this).removeAttr("checked");
		}else{
	  		$(this).attr('checked', 'checked');
	  	}
	});
});
function savetemplate(id,iduser){
	/*table template*/
	name_template=$('#name_template').val();
	if(name_template==''){
		alert('Renseignez le nom de la template');
		return;
	}
	idtemplate=id;
	id_user=iduser;
	$.ajax({
        type: "POST",
        url: "./controllers/controllerSavetemplate.php",
        async:true,
        ifModified:true,
        data: {
            savecretemplate:'oui',
            id_template:idtemplate,
            name_template:name_template,
        },
        success: function(content){
            id_template=content;
            /*table template_block*/
			//module point d'equipe
			if($('#type-1').attr('checked')=='checked'){
				name=$('#name-1').val();
				block=1;
				type=1;
				valeur=$('#valeur-1').val();
				savemodule(id_template,name,block,type,valeur);
			}
			//module dés
			if($('#type-2').attr('checked')=='checked'){
				name='module_des';
				block=2;
				type=2;
				des=$('input[name=desname]:checked').val()
				if(des=='perso'){
					des=$('#desperso').val();
				}
				maxdes=0;
		        n_plus = des.split("+");
		        for(i=0;i<n_plus.length;i++){
		        	n_d = n_plus[i].split("d");
		        	if(parseInt(n_d[1])==100){
		        		maxdes=maxdes-parseInt(n_d[0])*10;
		        	}
		        	maxdes=parseInt(n_d[0])*parseInt(n_d[1])+maxdes
		        }
				
				desreussite=$('#desreussite').val();
				descritique=$('#descritique').val();
				offreussite=$('#offreussite').val();
				offcritique=$('#offcritique').val();
				valeur=des+'|'+desreussite+'|'+descritique+'|'+maxdes+'|'+offreussite+'|'+offcritique;
				savemodule(id_template,name,block,type,valeur);
			}
			//module avatar
			if($('#type-4').attr('checked')=='checked'){
				name='module_avatar';
				block=4;
				type=4;
				valeur=null;
				savemodule(id_template,name,block,type,valeur);
				if($('#type-7').attr('checked')=='checked'){
					namebar=[];colorbar=[];
					$('input[name=bar-name]').each(function() {
						namebar.push($(this).val());
					});
					$('input[name=bar-color]').each(function() {
						colorbar.push($(this).val());
					});
					for(i=0;i<namebar.length;i++){
						name=namebar[i];
						block=5;
						type=7;
						valeur=colorbar[i];
						savemodule(id_template,name,block,type,valeur);
					}
				}
				if($('#type-5').attr('checked')=='checked'){
					name='module_bouclier';
					block=4;
					type=5;
					valeur=null;
					savemodule(id_template,name,block,type,valeur);
				}
				if($('#type-6').attr('checked')=='checked'){
					name='module_bouclier_sec';
					block=4;
					type=6;
					valeur=null;
					savemodule(id_template,name,block,type,valeur);
				}
			}
			if($('#type-8').attr('checked')=='checked'){
				name='module_tchat';
				block=6;
				type=8;
				valeur=null;
				savemodule(id_template,name,block,type,valeur);
			}
			if($('#type-9').attr('checked')=='checked'){
				name='module_note';
				block=7;
				type=9;
				valeur='';
				if($('#type-9-1').attr('checked')=='checked'){
					valeur+=$('#valeur-9-1').val();
				}
				if($('#type-9-2').attr('checked')=='checked'){
					if(valeur!=null){
						valeur+='|';
					}
					valeur+=$('#valeur-9-2').val();
				}
				if($('#type-9-3').attr('checked')=='checked'){
					if(valeur!=null){
						valeur+='|';
					}
					valeur+=$('#valeur-9-3').val();
				}
				savemodule(id_template,name,block,type,valeur);
			}
			if($('#type-10').attr('checked')=='checked'){
				name='module_fiche';
				block=8;
				type=10;
				valeur=null;
				savemodule(id_template,name,block,type,valeur);
				if($('#type-11').attr('checked')=='checked'){
					name=$('#name-10').val();
					block=$('#colonne-11').val()+$('#position-11').val();			
					type=11;
					valeur='';
					debut=0;
					$('input[name=stats-name]').each(function() {
						if(debut==1){
							valeur+='|';
						}
						valeur+=$(this).val();
						debut=1;
					});
					savemodule(id_template,name,block,type,valeur);
				}
				if($('#type-21').attr('checked')=='checked'){
					name=$('#name-21').val();
					block=$('#colonne-21').val()+$('#position-21').val();			
					type=21;
					valeur='';
					debut=0;
					$('input[name=stats-name_sec]').each(function() {
						if(debut==1){
							valeur+='|';
						}
						valeur+=$(this).val();
						debut=1;
					});
					savemodule(id_template,name,block,type,valeur);
				}
				if($('#type-12-13-14-16').attr('checked')=='checked'){
					nametmp=[];
					typetmp=[];
					colonnetmp=[];
					positiontmp=[];
					$('.name-12-13-14-16').each(function() {
						nametmp.push($(this).val());
					});
					$('.colonne-12-13-14-16').each(function() {
						colonnetmp.push($(this).val());
					});
					$('.position-12-13-14-16').each(function() {
						positiontmp.push($(this).val());
					});
					$('.type-12-13-14-16').each(function() {
						typetmp.push($(this).val());
					});
					for(i=0;i<nametmp.length;i++){
						name=nametmp[i];
						block=colonnetmp[i]+positiontmp[i];
						type=typetmp[i];
						valeur=null;
						savemodule(id_template,name,block,type,valeur);
					}
				}
				if($('#type-15').attr('checked')=='checked'){
					name=$('#name-15').val();
					block=$('#colonne-15').val()+$('#position-15').val();			
					type=15;
					valeur=null;
					debut=0;
					savemodule(id_template,name,block,type,valeur);
				}
				if($('#type-17').attr('checked')=='checked'){
					name=$('#name-17').val();
					block=$('#colonne-17').val()+$('#position-17').val();			
					type=18;
					valeur=null;
					debut=0;
					savemodule(id_template,name,block,type,valeur);
				}
				if($('#type-18').attr('checked')=='checked'){
					name=$('#name-18').val();
					block=$('#colonne-18').val()+$('#position-18').val();			
					type=18;
					valeur=null;
					debut=0;
					savemodule(id_template,name,block,type,valeur);
				}		
				if($('#type-19').attr('checked')=='checked'){
					name=$('#name-19').val();
					block=$('#colonne-19').val()+$('#position-19').val();			
					type=19;
					valeur=null;
					debut=0;
					savemodule(id_template,name,block,type,valeur);
				}
			}
				if($('#type-50').attr('checked')=='checked'){
					name='Règle du jeu';
					block=50;
					type=50;
					valeur=$('#valeur-50').val();
					savemodule(id_template,name,block,type,valeur);
				}
				if($('#type-51').attr('checked')=='checked'){
					name='module_doc';
					block=51;
					type=51;
					valeur=null;
					savemodule(id_template,name,block,type,valeur);
				}
				if($('#type-52').attr('checked')=='checked'){
					name='module_pnj';
					block=52;
					type=52;
					valeur=null;
					savemodule(id_template,name,block,type,valeur);
				}
				if($('#type-53').attr('checked')=='checked'){
					name='module_music';
					block=53;
					type=53;
					valeur=null;
					savemodule(id_template,name,block,type,valeur);
				}
				affichemessagepartie('<h1>Sauvegarde effectuée</h1><p><a href="index.php?url=creertemplate&template='+id_template+'" class="bouton_save"> Validez</a>');
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    });
	// 	
}
function savemodule(idtemplate,name,block,type,valeur){
	$.ajax({
        type: "POST",
        url: "./controllers/controllerSavetemplate.php",
        async:true,
        ifModified:true,
        data: {
            savemodule:'oui',
            id_template:idtemplate,
            name:name,
            block:block,
            type:type,
            valeur:valeur,
        },
        success: function(content){
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    });
}
function confirmsuppr(idtemplate){
	message='<h1>Validation suppression</h1>'+
      '<p><input class="btn_msg" type="button" value="Valider" onclick="supprtemplate('+idtemplate+');"> <input class="btn_msg" type="button" value="Annuler" onclick="fermer_messagepartie();"></p>';
    affichemessagepartie(message);
}
function supprtemplate(id){
	$.ajax({
        type: "POST",
        url: "./controllers/controllerSavetemplate.php",
        async:true,
        ifModified:true,
        data: {
            supprtemplate:'oui',
            id_template:id,
        },
        success: function(content){
        	window.location.href = 'index.php?url=creertemplate&template=0';
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    });
}
md=0
function moduledes(){
	if(md==0){
		$("#roll").css({'display':'block'});md=1;
	}else {
		$("#roll").css({'display':'none'});md=0;
	}
	
}
ma=0
function avatr(){
	if(ma==0){
		$("#joueur2").css({'display':'block'});ma=1;
	}else {
		$("#joueur2").css({'display':'none'});ma=0;
	}
	
}
mba=0;
function barav(){
	if(mba==0){
		$("#aficheav").css({'display':'block'});mba=1;
	}else {
		$("#aficheav").css({'display':'none'});mba=0;
	}
}
mb=0;
function afbc(){
	if(mb==0){
		$("#bouclier-2").css({'display':'block'});mb=1;
	}else {
		$("#bouclier-2").css({'display':'none'});mb=0;
	}
}
mbs=0;
function afbcs(){
	if(mbs==0){
		$("#boucliersec-2").css({'display':'block'});mbs=1;
	}else {
		$("#boucliersec-2").css({'display':'none'});mbs=0;
	}
}
mc=0;
function compt(){
	if(mc==0){
		$("#title2").css({'display':'block'});mc=1;
	}else {
		$("#title2").css({'display':'none'});mc=0;
	}
}
mn=0;
function noteaf(){
	if(mn==0){
		$("#fiche_joueur0").css({'display':'inline-block'});mn=1;
	}else {
		$("#fiche_joueur0").css({'display':'none'});mn=0;
	}
}
mf=0;
function ficheaf(){
	if(mf==0){
		for(i=1;i<5;i++){
			$("#fiche_joueur"+i).css({'display':'inline-block'});
		}
		mf=1;
	}else {
		for(i=1;i<5;i++){
			$("#fiche_joueur"+i).css({'display':'none'});
		}
		mf=0;
	}
}
mh=0;
function chataf(){
	if(mh==0){
		$("#ouverturefiche").css({'display':'inline-block'});
		mh=1;
	}else {
		$("#ouverturefiche").css({'display':'none'});
		mh=0;
	}
}

mp=0;
function pnjaf() {
	if(mp==0){
		$("#pnj").css({'display':'inline-block'});
		mp=1;
	}else {
		$("#pnj").css({'display':'none'});
		mp=0;
	}
}
mo=0;
function docaf() {
	if(mo==0){
		$("#doc").css({'display':'inline-block'});
		mo=1;
	}else {
		$("#doc").css({'display':'none'});
		mo=0;
	}
}
mm=0;
function musaf() {
	if(mm==0){
		$("#musique").css({'display':'inline-block'});
		mm=1;
	}else {
		$("#musique").css({'display':'none'});
		mm=0;
	}
}
mr=0;
function regaf() {
	if(mr==0){
		$("#regle").css({'display':'inline-block'});
		mr=1;
	}else {
		$("#regle").css({'display':'none'});
		mr=0;
	}
}