//var canvas_carte = this.__canvas = new fabric.Canvas('carte');
var canvas = this.__canvas = new fabric.Canvas('pion');
//var canvas_cacher = this.__canvas = new fabric.Canvas('cacher');
//var canvas_gm = this.__canvas = new fabric.Canvas('gm');
$(document).keydown(function(touche){
var k = touche.which || touche.keyCode;                      
  if (k == 46){
    remove();
  }
});
/*changer couleur BG*/
function bgcolor(){
  bg_color=$('#bg_color').val();
  $('body').css({'background-color':bg_color})
}
function bgcolordefaut(){
  $('body').css({'background-color':'#36393f'})
}
/*ajout image */
 function addImg(img){
  canvasselect=$('#canvasselect').val();
  fabric.Image.fromURL(img, function(img){
    canvas.add(img.set({ 
      flipX: false, 
      left: 500, 
      top: 100,
      calque:canvasselect,
      droit:0
    }).scale(0.5));
    moveclaque(img,canvasselect);
  })
}
function addImgUrl(){
  img=$('#imgperso').val();
  canvasselect=$('#canvasselect').val();
  fabric.Image.fromURL(img, function(img){
    canvas.add(img.set({ 
      flipX: false, 
      left: 500, 
      top: 100,
      calque:canvasselect,
      droit:0
    }).scale(0.5));
    moveclaque(img,canvasselect);
  })
}
/*listetoken*/
function listetoken(){
  imagetoken=[];nomtoken=[]
  $( ".avatar").each(function( index ) {
    var bg = this.id; 
    imagetoken.push(bg);
  });
  $( "#avatar .nom" ).each(function( index ) {
    nom=this.value; 
    nomtoken.push(nom) ;
  });
  liste='';
  for(var i= 0; i < imagetoken.length; i++){
      liste+="<button onclick=\"addToken('"+imagetoken[i]+"','"+nomtoken[i]+"')\" style='color:#000;'>"+nomtoken[i]+"</button>";
  }
  $('#listetoken').html(liste);
}
function addToken(id){
  imageURL=$('#'+id+' .image').css('background-image')
  imageURL=imageURL.substr(5);

  var image = new Image();
  image.src = imageURL;
  imgH = image.height;
  imgH=parseInt(imgH)/3;
  fabric.Image.fromURL(imageURL, function(oImg) {
    oImg.scale(1).set({
        left: 500,
        top: 100,
        stroke : 'white',
        strokeWidth : 200,
        clipTo: function (ctx) {
            ctx.arc(0, 0, imgH, 0, Math.PI * 2, true);
          }
    });
    
    oImg.scaleToHeight(100);
    oImg.scaleToWidth(200);
    canvas.add(oImg).setActiveObject(oImg);
    canvas.renderAll();
  });
}
/*gestion calque*/
zcarte=0;
zpion=10000;
zarbre=20000;
zmj=30000;
function moveclaque(obj,canvasselect){
  obj.set({ opacity: 1 });
  if(canvasselect=='carte'){zcalque=zcarte;zcarte++;}
  if(canvasselect=='pion'){zcalque=zpion;zpion++;}
  if(canvasselect=='cacher'){zcalque=zarbre;zarbre++;}
  if(canvasselect=='gm'){zcalque=zmj;zmj++; 
    obj.set({
        opacity: 0.5
    });
  }
  canvas.moveTo(obj, zcalque);
}
/*ajout objet*/
function addRect(){
  color=$('#colorgeo').val();
  canvasselect=$('#canvasselect').val();
  var rect = new fabric.Rect({
    left: 500,
    top: 100,
    width: 100,
    height: 50,
    fill: color,
    calque:canvasselect,
    droit:0
  });
  canvas.add(rect);
  moveclaque(rect,canvasselect);
}

function addCerc(){
  color=$('#colorgeo').val();
  canvasselect=$('#canvasselect').val();
  var circle = new fabric.Circle({
    left: 500,
    top: 100,
    radius: 50,
    fill: color
  });
  canvas.add(circle);
  canvas.moveTo(circle, 2);
  moveclaque(cirle,canvasselect);
}
function addTriangle(){
  color=$('#colorgeo').val(); 
  canvasselect=$('#canvasselect').val();
  var triangle = new fabric.Triangle({
    left: 500,
    top: 100,
    width: 70,
    height: 70,
    fill: color,
    calque:canvasselect,
    droit:0
  });
  canvas.add(triangle);
  moveclaque(triangle,canvasselect);
}
function addLigne(){
  color=$('#colorgeo').val();
  canvasselect=$('#canvasselect').val();
  var line = new fabric.Line([10, 10, 120, 60], {
    left: 500,
    top: 100,
    stroke: color,
    strokeWidth: 5,
    calque:canvasselect,
    droit:0
  });
  canvas.add(line);
  canvasselect=$('#canvasselect').val();
  moveclaque(line,canvasselect);
}
function addText(){
  color=$('#colorgeo').val();
  canvasselect=$('#canvasselect').val();
  var text = new fabric.Text('Hello world', {
    fontFamily: 'Arial',
    fontSize: 30,
    fontWeight: 'bold',
    left: 500,
    top: 100,
    fill: color,
    calque:canvasselect,
    droit:0
  });
  canvas.add(text)
  canvasselect=$('#canvasselect').val();
  moveclaque(text,canvasselect);
}

var appObject = function() {

  return {
    __canvas: canvas,
    __tmpgroup: {},

    addText: function() {
      textuel=$('#text-cont').val();
      color=$('#colorgeo').val();
      fanily=$('#fontfamilly option:selected').text();
      size=$('#size option:selected').text();
      var newID = (new Date()).getTime().toString().substr(5);
      var text = new fabric.IText(textuel, {
        fontFamily: fanily,
        fontSize:size,
        left: 500,
        top: 100,
        myid: newID,
        fill: color,
        objecttype: 'text'
      });

      this.__canvas.add(text);
      this.addLayer(newID, 'text');
    },
    setTextValue: function(value) {
      var obj = this.__canvas.getActiveObject();
      if (obj) {
        obj.setText(value);
        this.__canvas.renderAll();
      }
    },
    addLayer: function() {

    }

  };
}

$(document).ready(function() {
  var app = appObject();
  $('#add').click(function() {
    app.addText();
  });
  $('#text-cont').keyup(function() {
    app.setTextValue($(this).val());
  })

})
/*utilisation de moveto a voir*/
function changementdeplan(objet){
   canvas.moveTo(object, index); object.moveTo(index); 
}
/*ajout supprimer*/
//ne marche pas en node pour le moment
function add() {}
function remove(){
  var activeObjects = canvas.getActiveObjects();
  canvas.discardActiveObject()
  if (activeObjects.length) {
    canvas.remove.apply(canvas, activeObjects);
  }
}

/*grouper element*/


$( document ).ready(function() {
  var $ = function(id){return document.getElementById(id)};
  fabric.Object.prototype.transparentCorners = false;
  var group = $('group'),
      ungroup = $('ungroup'),
      multiselect = $('multiselect'),
      addmore = $('addmore'),
      discard = $('discard');
      if(idplayer==idmj){
        addmore.onclick = add;
      

        multiselect.onclick = function() {
          canvas.discardActiveObject();
          var sel = new fabric.ActiveSelection(canvas.getObjects(), {
            canvas: canvas,
          });
          canvas.setActiveObject(sel);
          canvas.requestRenderAll();
        }

        group.onclick = function() {
          if (!canvas.getActiveObject()) {
            return;
          }
          if (canvas.getActiveObject().type !== 'activeSelection') {
            return;
          }
          canvas.getActiveObject().toGroup();
          canvas.requestRenderAll();
        }

        ungroup.onclick = function() {
          if (!canvas.getActiveObject()) {
            return;
          }
          if (canvas.getActiveObject().type !== 'group') {
            return;
          }
          canvas.getActiveObject().toActiveSelection();
          canvas.requestRenderAll();
        }

        discard.onclick = function() {
          canvas.discardActiveObject();
          canvas.requestRenderAll();
        }
      }

});






/*grille*/
var affichageGrille="vide";
var canvaGrille=document.getElementById("grille");
var contextGrille=canvaGrille.getContext("2d");
contextGrille.canvas.width  = window.innerWidth;
contextGrille.canvas.height = window.innerHeight;

function sansgrille(){
  contextGrille.clearRect(0, 0, canvaGrille.width, canvaGrille.height);
  affichageGrille="vide";
}


function grille(){
  if(affichageGrille=="vide")
  {  
  var ecart = 50; //largeur d'un côté des cases
  contextGrille.strokeStyle = "#525252";
  for(var h = ecart ; h < canvaGrille.height ; h += ecart) {
     contextGrille.moveTo(0, h); //déplacer le pinceau à (x,y) sans tracer
     contextGrille.lineTo(canvaGrille.width, h); //tracer jusqu'à (x,y)
  }
  //colonnes
  for(var w = ecart ; w < canvaGrille.width ; w += ecart) {
     contextGrille.moveTo(w, 0);
     contextGrille.lineTo(w, canvaGrille.height);
  }
  contextGrille.stroke();
  affichageGrille="rect";
  }
  else
  {
  contextGrille.clearRect(0, 0, canvaGrille.width, canvaGrille.height);
  if(affichageGrille=="rect")
  {
    affichageGrille="vide";
  }
  else
  {
    affichageGrille="vide";
    grille();
  }
  }
}


function grillehexa(){
  
      var hexHeight,
        hexRadius,
        hexRectangleHeight,
        hexRectangleWidth,
        hexagonAngle = 0.523598776, // 30 degrees in radians
        sideLength = 25,
        boardWidth = 100,
        boardHeight = 100;

    hexHeight = Math.sin(hexagonAngle) * sideLength;
    hexRadius = Math.cos(hexagonAngle) * sideLength;
    hexRectangleHeight = sideLength + 2 * hexHeight;
    hexRectangleWidth = 2 * hexRadius;
  
  
  if(affichageGrille=="vide")
  {  

  contextGrille.fillStyle = "#525252";
        contextGrille.strokeStyle = "#525252";
        contextGrille.lineWidth = 1;

        drawBoard(contextGrille, boardWidth, boardHeight);
  affichageGrille="hex";
  }
  else
  {
  contextGrille.clearRect(0, 0, canvaGrille.width, canvaGrille.height);
  if(affichageGrille=="hex")
  {
    affichageGrille="vide";
  }
  else
  {
    affichageGrille="vide";
    grillehexa();
  }
  }


    function drawBoard(canvasContext, width, height) {
        var i,
            j;

        for(i = 0; i < width; ++i) {
            for(j = 0; j < height; ++j) {
                drawHexagon(
                    canvasContext, 
                    i * hexRectangleWidth + ((j % 2) * hexRadius), 
                    j * (sideLength + hexHeight), 
                    false
                );
            }
        }
    }

    function drawHexagon(canvasContext, x, y, fill) {           
        var fill = fill || false;

        canvasContext.beginPath();
        canvasContext.moveTo(x + hexRadius, y);
        canvasContext.lineTo(x + hexRectangleWidth, y + hexHeight);
        canvasContext.lineTo(x + hexRectangleWidth, y + hexHeight + sideLength);
        canvasContext.lineTo(x + hexRadius, y + hexRectangleHeight);
        canvasContext.lineTo(x, y + sideLength + hexHeight);
        canvasContext.lineTo(x, y + hexHeight);
        canvasContext.closePath();

        if(fill) {
            canvasContext.fill();
        } else {
            canvasContext.stroke();
        }
    }

}


/*copier coller*/

function Copy() {
  // clone what are you copying since you
  // may want copy and paste on different moment.
  // and you do not want the changes happened
  // later to reflect on the copy.
  canvas.getActiveObject().clone(function(cloned) {
    _clipboard = cloned;
  });
}

function Paste() {
  // clone again, so you can do multiple copies.
  _clipboard.clone(function(clonedObj) {
    canvas.discardActiveObject();
    clonedObj.set({
      left: clonedObj.left + 10,
      top: clonedObj.top + 10,
      evented: true,
    });
    if (clonedObj.type === 'activeSelection') {
      // active selection needs a reference to the canvas.
      clonedObj.canvas = canvas;
      clonedObj.forEachObject(function(obj) {
        canvas.add(obj);
      });
      // this should solve the unselectability
      clonedObj.setCoords();
    } else {
      canvas.add(clonedObj);
    }
    _clipboard.top += 10;
    _clipboard.left += 10;
    canvas.setActiveObject(clonedObj);
    canvas.requestRenderAll();
  });
}

/*draw*/



/*sauvegarde et chargement*/
function savecanvas(){
  var json = canvas.toJSON(); 
  obj = JSON.stringify(canvas);

  img=canvas.toDataURL("image/png");
  /*$decoded = base64_decode(substr($img,22));
  file_put_contents('./plateau/image'+$idplateau+'.png', $decoded);*/
  //$('#imageplateau').html('<img src="'+img+'" style="width:100px;height:100px; border:1px solid red">');

  name=$('#nomplateauactif').html()
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          saveplateau:'oui',
          id:idplateau,               
          name:name,
          contenu:obj,
      },
      success: function(content){
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  }) 
  //plateaunode(obj,0);//synchronisation plateau
}


function changerplateau(id){
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          changerplateau:'oui',
          id:id,               
      },
      success: function(content){
        loadcanvas(content);
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  })
  idplateau=id;
  name=$('#imageplat'+id).html();
  $('#nomplateauactif').html(name);
  $('#changernomplat').val(name)
  activeplateau(id);
}

function loadcanvas(contenu){
  canvas.loadFromJSON(contenu)
  obj=contenu;
  if(idplayer==idmj && idplateau>0){
    name=$('#imageplat'+idplateau).html();
    $('#nomplateauactif').html(name);
    $('#changernomplat').val(name)
    activeplateau(idplateau);
  }
  
}

function confirmClear(){
  affichemessagepartie('<h1>Confirmation vider le plateau</h1><input class="btn_msg" type="button" value="Oui" onclick="viderplateau()"><input class="btn_msg" type="button" value="Non" onclick="fermer_messagepartie()">');
}
function viderplateau(){
  canvas.clear();
  fermer_messagepartie() 
}
function changernomplat(){
  nom=$('#changernomplat').val();
  $('#nomplateauactif').html(nom);
  $('#imageplat'+idplateau).html(nom);
  savecanvas();
}

function nouveauplateau(){
  viderplateau();
  $('#changernomplat').val('Nouveau')
  $('#nomplateauactif').html('Nouveau');
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          nouveauplateau:'oui',              
      },
      success: function(content){
        idplateau=content;
        $( "#listimgplateau" ).append( '<div id="imageplat'+content+'" class="board" style="" onclick="changerplateau('+content+')">Nouveau</div>' );
        activeplateau(content);
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  })
}
function activeplateau(id){
  $('.board').css({'border':'1px solid #fff'});
  $('#imageplat'+id).css({'border':'1px solid #ffa500'});
}
function confirmSuppr(){
  affichemessagepartie('<h1>CSupprimer le plateau</h1><input class="btn_msg" type="button" value="Oui" onclick="supprimeplateau()"><input class="btn_msg" type="button" value="Non" onclick="fermer_messagepartie()">');
}
function supprimeplateau(){
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          suprrplateau:'oui',   
          id:idplateau,           
      },
      success: function(content){
        $('#imageplat'+idplateau).remove();
        $('#changernomplat').val('');
        $('#nomplateauactif').html('');
        $('#imageplat'+idplateau).html('');
        viderplateau();
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  })
}

function listeaffichage(){
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          listeaffichage:'oui',             
      },
      success: function(content){
        $('#listejoueuraffiche').html(content)
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  })
}
function affichera(id){
  plateaunode(obj,id,idplateau);
  if(id==0){
    $('#listejoueuraffiche span').html('');
  }else {
    name=$('#nomplateauactif').html();
    $('#local'+id).html(' est sur '+name)
  }  
}
function actualiteTime(){
  if(idplayer == idmj){
    savecanvas();
  }else{
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            controleplateau:'oui', 
            id:idplateau,            
        },
        success: function(content){
          if(content==obj){
            obj2 = JSON.stringify(canvas);
            if(obj2!=obj){
              plateaunode(obj2,idmj,idplateau);    
            }
          }else{
            loadcanvas(content);
          }
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    })
  }
}
$( document ).ready(function() {
    if(idplateau>0){      
        setInterval(actualiteTime,1000);
    }
});

/*context menu*/ 
sourisx=0;
sourisy=0;

$('body').on('contextmenu', 'canvas', function(e){
    //if(idplayer==idmj){
       e.preventDefault();
      sourisx = e.offsetX;
      sourisy = e.offsetY;
      $('#menucontext').css({'top':sourisy,'left':sourisx,'display':'block'});
      return false;
    //}
});
time=0

$('canvas').click(function(event){
     
    if( event.which == 1 ){
      closemenu();        
        //alert('clic du bouton gauche');
    } else if ( event.which == 2 ){
       // alert('clic du bouton milieu');
    } else if ( event.which == 3 ){
        //alert('clic du bouton droit');
    }
});
  
function Copy() {
  // clone what are you copying since you
  // may want copy and paste on different moment.
  // and you do not want the changes happened
  // later to reflect on the copy.
  canvas.getActiveObject().clone(function(cloned) {
    _clipboard = cloned;
  });
}

function Paste(x,y) {
  // clone again, so you can do multiple copies.
  _clipboard.clone(function(clonedObj) {
    canvas.discardActiveObject();
    clonedObj.set({
      left: sourisx + 10,
      top: sourisy + 10,
      evented: true,
    });
    if (clonedObj.type === 'activeSelection') {
      // active selection needs a reference to the canvas.
      clonedObj.canvas = canvas;
      clonedObj.forEachObject(function(obj) {
        canvas.add(obj);
      });
      // this should solve the unselectability
      clonedObj.setCoords();
    } else {
      canvas.add(clonedObj);
    }
    _clipboard.top += 10;
    _clipboard.left += 10;
    canvas.setActiveObject(clonedObj);
    canvas.requestRenderAll();
  });
}

function planav() {
  if(canvasselect=='carte'){zcalque=zcarte+1;zcarte++;}
  if(canvasselect=='pion'){zcalque=zpion+1;zpion++;}
  if(canvasselect=='cacher'){zcalque=zarbre+1;zarbre++;}
  if(canvasselect=='gm'){zcalque=zmj+1;zmj++;}
  canvas.getActiveObject().moveTo(zcalque);
  canvas.requestRenderAll();
}
function planar() {
   if(canvasselect=='carte'){zcalque=0;}
  if(canvasselect=='pion'){zcalque=10000}
  if(canvasselect=='cacher'){zcalque=20000;}
  if(canvasselect=='gm'){zcalque=zmj+30000;}
  canvas.getActiveObject().moveTo(zcalque);
  canvas.requestRenderAll();
}
function horizontal() {
  canvas.getActiveObject().toggle('flipX');
  canvas.requestRenderAll();
}
function vertical() {
  canvas.getActiveObject().toggle('flipY');
  canvas.requestRenderAll();
}

function changeclaque(nbclaque){//movecalque(obj,canvasselect)
  if(nbclaque==0){nbclaque="carte"; zindex=zcarte; opa=1}
  else if(nbclaque==1){nbclaque="pion"; zindex=zpion; opa=1}
  else if(nbclaque==2){nbclaque="cacher"; zindex=zarbre; opa=1}
  else if(nbclaque==3){nbclaque="gm"; zindex=zmj; opa=0.5}
  canvas.getActiveObject().moveTo(zindex);
  canvas.getActiveObject().set({
    calque:nbclaque,
    opacity:opa,
  });
  canvas.requestRenderAll();
}

function closemenu(){
  $('#menucontext').css({'display':'none'});
}

function listedroit(){
  idtoken=[];nomtoken=[]
  $( ".avatar").each(function( index ) {
    var bg = this.id; 
    bg=bg.substring(6)
    idtoken.push(bg);
  });
  $( "#avatar .nom" ).each(function( index ) {
    nom=this.value; 
    nomtoken.push(nom) ;
  });
  liste="<li onclick=\"changedroit('0')\">Retirer les droits</li>";
  for(var i= 0; i < idtoken.length; i++){
      liste+="<li onclick=\"changedroit('"+idtoken[i]+"')\">"+nomtoken[i]+"</li>";
  }
  $('#listedroit').html(liste);
}

function changedroit(idjoueur){
   canvas.getActiveObject().set({
    droit:idjoueur,
  });
  canvas.requestRenderAll();
  closemenu()
}


function ping(){
  color=$('#couleurprincipale'+idplayer).val();
  var pingou = new fabric.Circle({
    left: sourisx,
    top: sourisy,
    radius: 0,
    fill: 'rgba(0,0,0,0)',
    stroke: color,
    strokeWidth: 4,
    originX: 'center',
    originY: 'center',
    strokeUniform: true
  });
  canvas.add(pingou);
    pingou.animate('radius', pingou.radius === 100 ? 400 : 100, {
      duration: 1000,
      onChange: canvas.renderAll.bind(canvas),
      onComplete: function() {
         canvas.remove(pingou);
      },
      
    });
}
/*zoom*/
canvas.on('mouse:wheel', function(opt) {
  var delta = opt.e.deltaY;
  var pointer = canvas.getPointer(opt.e);
  var zoom = canvas.getZoom();
  zoom = zoom + delta/200;
  if (zoom > 20) zoom = 20;
  if (zoom < 0.01) zoom = 0.01;
  canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
  opt.e.preventDefault();
  opt.e.stopPropagation();
});
/*Deplacement canvas*/
const STATE_IDLE = 'idle';
const STATE_PANNING = 'panning';
fabric.Canvas.prototype.toggleDragMode = function(dragMode) {
  // Remember the previous X and Y coordinates for delta calculations
  let lastClientX;
  let lastClientY;
  // Keep track of the state
  let state = STATE_IDLE;
  // We're entering dragmode
  if (dragMode) {
    // Discard any active object
    this.discardActiveObject();
    // Set the cursor to 'move'
    this.defaultCursor = 'pointer';
    // Loop over all objects and disable events / selectable. We remember its value in a temp variable stored on each object
    this.forEachObject(function(object) {
      object.prevEvented = object.evented;
      object.prevSelectable = object.selectable;
      object.evented = false;
      object.selectable = false;
    });
    // Remove selection ability on the canvas
    this.selection = false;
    // When MouseUp fires, we set the state to idle
    this.on('mouse:up', function(e) {
      state = STATE_IDLE;
    });
    // When MouseDown fires, we set the state to panning
    this.on('mouse:down', (e) => {
      state = STATE_PANNING;
      lastClientX = e.e.clientX;
      lastClientY = e.e.clientY;
    });
    // When the mouse moves, and we're panning (mouse down), we continue
    this.on('mouse:move', (e) => {
      if (state === STATE_PANNING && e && e.e) {
        // let delta = new fabric.Point(e.e.movementX, e.e.movementY); // No Safari support for movementX and movementY
        // For cross-browser compatibility, I had to manually keep track of the delta

        // Calculate deltas
        let deltaX = 0;
        let deltaY = 0;
        if (lastClientX) {
          deltaX = e.e.clientX - lastClientX;
        }
        if (lastClientY) {
          deltaY = e.e.clientY - lastClientY;
        }
        // Update the last X and Y values
        lastClientX = e.e.clientX;
        lastClientY = e.e.clientY;

        let delta = new fabric.Point(deltaX, deltaY);
        this.relativePan(delta);
        this.trigger('moved');
      }
    });
  } else {
    // When we exit dragmode, we restore the previous values on all objects
    this.forEachObject(function(object) {
      object.evented = (object.prevEvented !== undefined) ? object.prevEvented : object.evented;
      object.selectable = (object.prevSelectable !== undefined) ? object.prevSelectable : object.selectable;
    });
    // Reset the cursor
    this.defaultCursor = 'default';
    // Remove the event listeners
    this.off('mouse:up');
    this.off('mouse:down');
    this.off('mouse:move');
    // Restore selection ability on the canvas
    this.selection = true;
  }
};


// Handle dragmode change
panactiv=0;
let dragMode = false;
function pan() {
  dragMode = !dragMode;
  canvas.toggleDragMode(dragMode);
  if(panactiv==0){
    $('#panactif').html('Désactiver Pan')
    panactiv=1;
  }else {
    $('#panactif').html('Activer Pan')
    panactiv=0;
  }
}


/*Draw*/

function activedraw(){
  canvas.isDrawingMode = !canvas.isDrawingMode;
    if (canvas.isDrawingMode) {
      $('#drawing-mode').html('Désactiver le mode dessin') ;
      $('#drawing-mode-options').css({'display':'block'});
      canvas.freeDrawingBrush.color = $('#drawing-color').val();   
      canvas.freeDrawingBrush.width = parseInt($('#infowidth').html(), 10) || 1;
      canvasselect=$('#canvasselect').val();
      if(canvasselect=='carte'){zcalque=0;}
      if(canvasselect=='pion'){zcalque=10000}
      if(canvasselect=='cacher'){zcalque=20000;}
      if(canvasselect=='gm'){zcalque=zmj+30000;}
      canvas.freeDrawingBrush.getActiveObject().moveTo(zcalque);
      canvas.freeDrawingBrush.requestRenderAll();
      canvas.freeDrawingBrush.claque = canvasselect;
  }else {
    $('#drawing-mode').html('Activez le mode dessin') ;
    $('#drawing-mode-options').css({'display':'none'});
  }
}

function modifwidth(){
  modif=$('#drawing-line-width').val();
  $('#infowidth').html(modif);
  canvas.freeDrawingBrush.width = parseInt(modif, 10) || 1;
  this.previousSibling.innerHTML = this.value;
}

function changecolordraw(){
  modif=$('#drawing-color').val();
  canvas.freeDrawingBrush.color = modif;
}


/*
(function() {
  var $ = function(id){return document.getElementById(id)};

  var canvas = this.__canvas = new fabric.Canvas('pion', {
    isDrawingMode: true
  });

  fabric.Object.prototype.transparentCorners = false;

  var drawingModeEl = $('#drawing-mode'),
      drawingOptionsEl = $('#drawing-mode-options'),
      drawingColorEl = $('#drawing-color'),
      drawingShadowColorEl = $('#drawing-shadow-color'),
      drawingLineWidthEl = $('#drawing-line-width'),
      drawingShadowWidth = $('#drawing-shadow-width'),
      drawingShadowOffset = $('#drawing-shadow-offset')


  drawingModeEl.onclick = function() {
    canvas.isDrawingMode = !canvas.isDrawingMode;
    if (canvas.isDrawingMode) {
      drawingModeEl.innerHTML = 'Cancel drawing mode';
      drawingOptionsEl.style.display = '';
    }
    else {
      drawingModeEl.innerHTML = 'Enter drawing mode';
      drawingOptionsEl.style.display = 'none';
    }
  };

  if (fabric.PatternBrush) {
    var vLinePatternBrush = new fabric.PatternBrush(canvas);
    vLinePatternBrush.getPatternSrc = function() {

      var patternCanvas = fabric.document.createElement('pion');
      patternCanvas.width = patternCanvas.height = 10;
      var ctx = patternCanvas.getContext('2d');

      ctx.strokeStyle = this.color;
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(0, 5);
      ctx.lineTo(10, 5);
      ctx.closePath();
      ctx.stroke();

      return patternCanvas;
    };

    var hLinePatternBrush = new fabric.PatternBrush(canvas);
    hLinePatternBrush.getPatternSrc = function() {

      var patternCanvas = fabric.document.createElement('pion');
      patternCanvas.width = patternCanvas.height = 10;
      var ctx = patternCanvas.getContext('2d');

      ctx.strokeStyle = this.color;
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(5, 0);
      ctx.lineTo(5, 10);
      ctx.closePath();
      ctx.stroke();

      return patternCanvas;
    };

    var squarePatternBrush = new fabric.PatternBrush(canvas);
    squarePatternBrush.getPatternSrc = function() {

      var squareWidth = 10, squareDistance = 2;

      var patternCanvas = fabric.document.createElement('pion');
      patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
      var ctx = patternCanvas.getContext('2d');

      ctx.fillStyle = this.color;
      ctx.fillRect(0, 0, squareWidth, squareWidth);

      return patternCanvas;
    };

    var diamondPatternBrush = new fabric.PatternBrush(canvas);
    diamondPatternBrush.getPatternSrc = function() {

      var squareWidth = 10, squareDistance = 5;
      var patternCanvas = fabric.document.createElement('pion');
      var rect = new fabric.Rect({
        width: squareWidth,
        height: squareWidth,
        angle: 45,
        fill: this.color
      });

      var canvasWidth = rect.getBoundingRect().width;

      patternCanvas.width = patternCanvas.height = canvasWidth + squareDistance;
      rect.set({ left: canvasWidth / 2, top: canvasWidth / 2 });

      var ctx = patternCanvas.getContext('2d');
      rect.render(ctx);

      return patternCanvas;
    };

    var img = new Image();
    img.src = '../assets/honey_im_subtle.png';

    var texturePatternBrush = new fabric.PatternBrush(canvas);
    texturePatternBrush.source = img;
  }

  $('drawing-mode-selector').onchange = function() {

    if (this.value === 'hline') {
      canvas.freeDrawingBrush = vLinePatternBrush;
    }
    else if (this.value === 'vline') {
      canvas.freeDrawingBrush = hLinePatternBrush;
    }
    else if (this.value === 'square') {
      canvas.freeDrawingBrush = squarePatternBrush;
    }
    else if (this.value === 'diamond') {
      canvas.freeDrawingBrush = diamondPatternBrush;
    }
    else if (this.value === 'texture') {
      canvas.freeDrawingBrush = texturePatternBrush;
    }
    else {
      canvas.freeDrawingBrush = new fabric[this.value + 'Brush'](canvas);
    }

    if (canvas.freeDrawingBrush) {
      canvas.freeDrawingBrush.color = drawingColorEl.value;
      canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
      canvas.freeDrawingBrush.shadow = new fabric.Shadow({
        blur: parseInt(drawingShadowWidth.value, 10) || 0,
        offsetX: 0,
        offsetY: 0,
        affectStroke: true,
        color: drawingShadowColorEl.value,
      });
    }
  };

  drawingColorEl.onchange = function() {
    canvas.freeDrawingBrush.color = this.value;
  };
  drawingShadowColorEl.onchange = function() {
    canvas.freeDrawingBrush.shadow.color = this.value;
  };
  drawingLineWidthEl.onchange = function() {
    canvas.freeDrawingBrush.width = parseInt(this.value, 10) || 1;
    this.previousSibling.innerHTML = this.value;
  };
  drawingShadowWidth.onchange = function() {
    canvas.freeDrawingBrush.shadow.blur = parseInt(this.value, 10) || 0;
    this.previousSibling.innerHTML = this.value;
  };
  drawingShadowOffset.onchange = function() {
    canvas.freeDrawingBrush.shadow.offsetX = parseInt(this.value, 10) || 0;
    canvas.freeDrawingBrush.shadow.offsetY = parseInt(this.value, 10) || 0;
    this.previousSibling.innerHTML = this.value;
  };

  if (canvas.freeDrawingBrush) {
    canvas.freeDrawingBrush.color = drawingColorEl.value;
    canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
    canvas.freeDrawingBrush.shadow = new fabric.Shadow({
      blur: parseInt(drawingShadowWidth.value, 10) || 0,
      offsetX: 0,
      offsetY: 0,
      affectStroke: true,
      color: drawingShadowColorEl.value,
    });
  }
})();
*/