//var canvas_carte = this.__canvas = new fabric.Canvas('carte');
var canvas = this.__canvas = new fabric.Canvas('pion');
//var canvas_cacher = this.__canvas = new fabric.Canvas('cacher');
//var canvas_gm = this.__canvas = new fabric.Canvas('gm');
$(document).keydown(function(touche){
var k = touche.which || touche.keyCode;
                         
  if (k == 46){
    remove();
  }
});
/*changer couleur BG*/
function bgcolor(){
  bg_color=$('#bg_color').val();
  $('body').css({'background-color':bg_color})
}
function bgcolordefaut(){
  $('body').css({'background-color':'#36393f'})
}
/*ajout image */
 function addImg(img){
  canvasselect=$('#canvasselect').val();
  fabric.Image.fromURL(img, function(img){
    canvas.add(img.set({ 
      flipX: false, 
      left: 500, 
      top: 100,
      calque:canvasselect,
      droit:0
    }).scale(0.5));
    moveclaque(img,canvasselect);
  })
}
/*listetoken*/
function listetoken(){
  imagetoken=[];nomtoken=[]
  $( ".avatar").each(function( index ) {
    var bg = this.id; 
    imagetoken.push(bg);
  });
  $( "#avatar .nom" ).each(function( index ) {
    nom=this.value; 
    nomtoken.push(nom) ;
  });
  liste='';
  for(var i= 0; i < imagetoken.length; i++){
      liste+="<button onclick=\"addToken('"+imagetoken[i]+"','"+nomtoken[i]+"')\" style='color:#000;'>"+nomtoken[i]+"</button>";
  }
  $('#listetoken').html(liste);
}
function addToken(id){
  imageURL=$('#'+id+' .image').css('background-image')
  imageURL=imageURL.substr(5);

  var image = new Image();
  image.src = imageURL;
  imgH = image.height;
  imgH=parseInt(imgH)/3;
  fabric.Image.fromURL(imageURL, function(oImg) {
    oImg.scale(1).set({
        left: 500,
        top: 100,
        stroke : 'white',
        strokeWidth : 200,
        clipTo: function (ctx) {
            ctx.arc(0, 0, imgH, 0, Math.PI * 2, true);
          }
    });
    
    oImg.scaleToHeight(100);
    oImg.scaleToWidth(200);
    canvas.add(oImg).setActiveObject(oImg);
    canvas.renderAll();
  });
}
/*gestion calque*/
zcarte=0;
zpion=10000;
zarbre=20000;
zmj=30000;
function moveclaque(obj,canvasselect){
  obj.set({ opacity: 1 });
  if(canvasselect=='carte'){zcalque=zcarte;zcarte++;}
  if(canvasselect=='pion'){zcalque=zpion;zpion++;}
  if(canvasselect=='cacher'){zcalque=zarbre;zarbre++;}
  if(canvasselect=='gm'){zcalque=zmj;zmj++; 
    obj.set({
        opacity: 0.5
    });
  }
  canvas.moveTo(obj, zcalque);
}
/*ajout objet*/
function addRect(){
  color=$('#colorgeo').val();
  canvasselect=$('#canvasselect').val();
  var rect = new fabric.Rect({
    left: 500,
    top: 100,
    width: 100,
    height: 50,
    fill: color,
    calque:canvasselect,
    droit:0
  });
  canvas.add(rect);
  moveclaque(rect,canvasselect);
}

function addCerc(){
  color=$('#colorgeo').val();
  canvasselect=$('#canvasselect').val();
  var circle = new fabric.Circle({
    left: 500,
    top: 100,
    radius: 50,
    fill: color
  });
  canvas.add(circle);
  canvas.moveTo(circle, 2);
  moveclaque(cirle,canvasselect);
}
function addTriangle(){
  color=$('#colorgeo').val(); 
  canvasselect=$('#canvasselect').val();
  var triangle = new fabric.Triangle({
    left: 500,
    top: 100,
    width: 70,
    height: 70,
    fill: color,
    calque:canvasselect,
    droit:0
  });
  canvas.add(triangle);
  moveclaque(triangle,canvasselect);
}
function addLigne(){
  color=$('#colorgeo').val();
  canvasselect=$('#canvasselect').val();
  var line = new fabric.Line([10, 10, 120, 60], {
    left: 500,
    top: 100,
    stroke: color,
    strokeWidth: 5,
    calque:canvasselect,
    droit:0
  });
  canvas.add(line);
  canvasselect=$('#canvasselect').val();
  moveclaque(line,canvasselect);
}
function addText(){
  color=$('#colorgeo').val();
  canvasselect=$('#canvasselect').val();
  var text = new fabric.Text('Hello world', {
    fontFamily: 'Arial',
    fontSize: 30,
    fontWeight: 'bold',
    left: 500,
    top: 100,
    fill: color,
    calque:canvasselect,
    droit:0
  });
  canvas.add(text)
  canvasselect=$('#canvasselect').val();
  moveclaque(text,canvasselect);
}

var appObject = function() {

  return {
    __canvas: canvas,
    __tmpgroup: {},

    addText: function() {
      textuel=$('#text-cont').val();
      color=$('#colorgeo').val();
      fanily=$('#fontfamilly option:selected').text();
      size=$('#size option:selected').text();
      var newID = (new Date()).getTime().toString().substr(5);
      var text = new fabric.IText(textuel, {
        fontFamily: fanily,
        fontSize:size,
        left: 500,
        top: 100,
        myid: newID,
        fill: color,
        objecttype: 'text'
      });

      this.__canvas.add(text);
      this.addLayer(newID, 'text');
    },
    setTextValue: function(value) {
      var obj = this.__canvas.getActiveObject();
      if (obj) {
        obj.setText(value);
        this.__canvas.renderAll();
      }
    },
    addLayer: function() {

    }

  };
}

$(document).ready(function() {

  var app = appObject();
  $('#add').click(function() {
    app.addText();
  });
  $('#text-cont').keyup(function() {
    app.setTextValue($(this).val());
  })

})
/*utilisation de moveto a voir*/
function changementdeplan(objet){
   canvas.moveTo(object, index); object.moveTo(index); 
}
/*ajout supprimer*/
//ne marche pas en node pour le moment
function add() {}
function remove(){
  var activeObjects = canvas.getActiveObjects();
  canvas.discardActiveObject()
  if (activeObjects.length) {
    canvas.remove.apply(canvas, activeObjects);
  }
}

/*grouper element*/


$( document ).ready(function() {
  var $ = function(id){return document.getElementById(id)};
  fabric.Object.prototype.transparentCorners = false;
  var group = $('group'),
      ungroup = $('ungroup'),
      multiselect = $('multiselect'),
      addmore = $('addmore'),
      discard = $('discard');
      if(idplayer==idmj){
        addmore.onclick = add;
      

        multiselect.onclick = function() {
          canvas.discardActiveObject();
          var sel = new fabric.ActiveSelection(canvas.getObjects(), {
            canvas: canvas,
          });
          canvas.setActiveObject(sel);
          canvas.requestRenderAll();
        }

        group.onclick = function() {
          if (!canvas.getActiveObject()) {
            return;
          }
          if (canvas.getActiveObject().type !== 'activeSelection') {
            return;
          }
          canvas.getActiveObject().toGroup();
          canvas.requestRenderAll();
        }

        ungroup.onclick = function() {
          if (!canvas.getActiveObject()) {
            return;
          }
          if (canvas.getActiveObject().type !== 'group') {
            return;
          }
          canvas.getActiveObject().toActiveSelection();
          canvas.requestRenderAll();
        }

        discard.onclick = function() {
          canvas.discardActiveObject();
          canvas.requestRenderAll();
        }
      }

});






/*grille*/
var affichageGrille="vide";
var canvaGrille=document.getElementById("grille");
var contextGrille=canvaGrille.getContext("2d");
contextGrille.canvas.width  = window.innerWidth;
contextGrille.canvas.height = window.innerHeight;

function sansgrille(){
  contextGrille.clearRect(0, 0, canvaGrille.width, canvaGrille.height);
  affichageGrille="vide";
}


function grille(){
  if(affichageGrille=="vide")
  {  
  var ecart = 50; //largeur d'un côté des cases
  contextGrille.strokeStyle = "#525252";
  for(var h = ecart ; h < canvaGrille.height ; h += ecart) {
     contextGrille.moveTo(0, h); //déplacer le pinceau à (x,y) sans tracer
     contextGrille.lineTo(canvaGrille.width, h); //tracer jusqu'à (x,y)
  }
  //colonnes
  for(var w = ecart ; w < canvaGrille.width ; w += ecart) {
     contextGrille.moveTo(w, 0);
     contextGrille.lineTo(w, canvaGrille.height);
  }
  contextGrille.stroke();
  affichageGrille="rect";
  }
  else
  {
  contextGrille.clearRect(0, 0, canvaGrille.width, canvaGrille.height);
  if(affichageGrille=="rect")
  {
    affichageGrille="vide";
  }
  else
  {
    affichageGrille="vide";
    grille();
  }
  }
}


function grillehexa(){
  
      var hexHeight,
        hexRadius,
        hexRectangleHeight,
        hexRectangleWidth,
        hexagonAngle = 0.523598776, // 30 degrees in radians
        sideLength = 25,
        boardWidth = 100,
        boardHeight = 100;

    hexHeight = Math.sin(hexagonAngle) * sideLength;
    hexRadius = Math.cos(hexagonAngle) * sideLength;
    hexRectangleHeight = sideLength + 2 * hexHeight;
    hexRectangleWidth = 2 * hexRadius;
  
  
  if(affichageGrille=="vide")
  {  

  contextGrille.fillStyle = "#525252";
        contextGrille.strokeStyle = "#525252";
        contextGrille.lineWidth = 1;

        drawBoard(contextGrille, boardWidth, boardHeight);
  affichageGrille="hex";
  }
  else
  {
  contextGrille.clearRect(0, 0, canvaGrille.width, canvaGrille.height);
  if(affichageGrille=="hex")
  {
    affichageGrille="vide";
  }
  else
  {
    affichageGrille="vide";
    grillehexa();
  }
  }


    function drawBoard(canvasContext, width, height) {
        var i,
            j;

        for(i = 0; i < width; ++i) {
            for(j = 0; j < height; ++j) {
                drawHexagon(
                    canvasContext, 
                    i * hexRectangleWidth + ((j % 2) * hexRadius), 
                    j * (sideLength + hexHeight), 
                    false
                );
            }
        }
    }

    function drawHexagon(canvasContext, x, y, fill) {           
        var fill = fill || false;

        canvasContext.beginPath();
        canvasContext.moveTo(x + hexRadius, y);
        canvasContext.lineTo(x + hexRectangleWidth, y + hexHeight);
        canvasContext.lineTo(x + hexRectangleWidth, y + hexHeight + sideLength);
        canvasContext.lineTo(x + hexRadius, y + hexRectangleHeight);
        canvasContext.lineTo(x, y + sideLength + hexHeight);
        canvasContext.lineTo(x, y + hexHeight);
        canvasContext.closePath();

        if(fill) {
            canvasContext.fill();
        } else {
            canvasContext.stroke();
        }
    }

}


/*copier coller*/

function Copy() {
  // clone what are you copying since you
  // may want copy and paste on different moment.
  // and you do not want the changes happened
  // later to reflect on the copy.
  canvas.getActiveObject().clone(function(cloned) {
    _clipboard = cloned;
  });
}

function Paste() {
  // clone again, so you can do multiple copies.
  _clipboard.clone(function(clonedObj) {
    canvas.discardActiveObject();
    clonedObj.set({
      left: clonedObj.left + 10,
      top: clonedObj.top + 10,
      evented: true,
    });
    if (clonedObj.type === 'activeSelection') {
      // active selection needs a reference to the canvas.
      clonedObj.canvas = canvas;
      clonedObj.forEachObject(function(obj) {
        canvas.add(obj);
      });
      // this should solve the unselectability
      clonedObj.setCoords();
    } else {
      canvas.add(clonedObj);
    }
    _clipboard.top += 10;
    _clipboard.left += 10;
    canvas.setActiveObject(clonedObj);
    canvas.requestRenderAll();
  });
}

/*draw*/



/*sauvegarde et chargement*/
function savecanvas(){
  var json = canvas.toJSON(); 
  obj = JSON.stringify(canvas);

  img=canvas.toDataURL("image/png");
  /*$decoded = base64_decode(substr($img,22));
  file_put_contents('./plateau/image'+$idplateau+'.png', $decoded);*/
  //$('#imageplateau').html('<img src="'+img+'" style="width:100px;height:100px; border:1px solid red">');

  name=$('#nomplateauactif').html()
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          saveplateau:'oui',
          id:idplateau,               
          name:name,
          contenu:obj,
      },
      success: function(content){
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  }) 
  //plateaunode(obj,0);//synchronisation plateau
}


function changerplateau(id){
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          changerplateau:'oui',
          id:id,               
      },
      success: function(content){
        loadcanvas(content);
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  })
  idplateau=id;
  name=$('#imageplat'+id).html();
  $('#nomplateauactif').html(name);
  $('#changernomplat').val(name)
  activeplateau(id);
}

function loadcanvas(contenu){
  canvas.loadFromJSON(contenu)
  obj=contenu;
  if(idplayer==idmj && idplateau>0){
    name=$('#imageplat'+idplateau).html();
    $('#nomplateauactif').html(name);
    $('#changernomplat').val(name)
    activeplateau(idplateau);
  }
  
}

function confirmClear(){
  affichemessagepartie('<h1>Confirmation vider le plateau</h1><input class="btn_msg" type="button" value="Oui" onclick="viderplateau()"><input class="btn_msg" type="button" value="Non" onclick="fermer_messagepartie()">');
}
function viderplateau(){
  canvas.clear();
  fermer_messagepartie() 
}
function changernomplat(){
  nom=$('#changernomplat').val();
  $('#nomplateauactif').html(nom);
  $('#imageplat'+idplateau).html(nom);
  savecanvas();
}

function nouveauplateau(){
  viderplateau();
  $('#changernomplat').val('Nouveau')
  $('#nomplateauactif').html('Nouveau');
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          nouveauplateau:'oui',              
      },
      success: function(content){
        idplateau=content;
        $( "#listimgplateau" ).append( '<div id="imageplat'+content+'" class="board" style="" onclick="changerplateau('+content+')">Nouveau</div>' );
        activeplateau(content);
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  })
}
function activeplateau(id){
  $('.board').css({'border':'1px solid #fff'});
  $('#imageplat'+id).css({'border':'1px solid #ffa500'});
}
function confirmSuppr(){
  affichemessagepartie('<h1>CSupprimer le plateau</h1><input class="btn_msg" type="button" value="Oui" onclick="supprimeplateau()"><input class="btn_msg" type="button" value="Non" onclick="fermer_messagepartie()">');
}
function supprimeplateau(){
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          suprrplateau:'oui',   
          id:idplateau,           
      },
      success: function(content){
        $('#imageplat'+idplateau).remove();
        $('#changernomplat').val('');
        $('#nomplateauactif').html('');
        $('#imageplat'+idplateau).html('');
        viderplateau();
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  })
}

function listeaffichage(){
  $.ajax({
      type: "POST",
      url: "./controllers/controllersave.php",
      async:true,
      ifModified:true,
      data: {
          listeaffichage:'oui',             
      },
      success: function(content){
        $('#listejoueuraffiche').html(content)
      },
      fail: function(content){
          alert("Echec AJAX : "+content);
      },
  })
}
function affichera(id){
  plateaunode(obj,id,idplateau);
  if(id==0){
    $('#listejoueuraffiche span').html('');
  }else {
    name=$('#nomplateauactif').html();
    $('#local'+id).html(' est sur '+name)
  }  
}
function actualiteTime(){
  if(idplayer == idmj){
    savecanvas();
  }else{
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            controleplateau:'oui', 
            id:idplateau,            
        },
        success: function(content){
          if(content==obj){
            obj2 = JSON.stringify(canvas);
            if(obj2!=obj){
              plateaunode(obj2,idmj,idplateau);    
            }
          }else{
            loadcanvas(content);
          }
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    })
  }
}
$( document ).ready(function() {
    if(idplateau>0){      
        setInterval(actualiteTime,1000);
    }
});

/*contexte menu*/
function getCoords(el,event) {
  var ox = -el.offsetLeft,
  oy = -el.offsetTop;
  while(el=el.offsetParent){
    ox += el.scrollLeft - el.offsetLeft;
    oy += el.scrollTop - el.offsetTop;
  }
  return {x:event.clientX + ox , y:event.clientY + oy};
}
 
 
// Exemple d'utilisation :
 
canvas.onclick = function(e) {
  var coords = getCoords(this,e);
  alert("Clic -> X:"+coords.x+" - Y:"+coords.y);
};


          /*$("#PlateauCanvas").contextmenu({
            show: false,
            menu: [
                {
                    title: "<a class='context_menu_link'> <i class='fa fa-reply-all fa-rotate-180'></i>  Bring To Front</a>",
                    cmd: "ToFront"
                },
                {
                    title: "<a  class='context_menu_link'> <i class='fa fa-reply fa-rotate-180'></i>  Bring Forward</a>",
                    cmd: "Forward"
                },
                {
                    title: "<a class='context_menu_link'> <i class='fa fa-reply'></i> Send Backwards</a>",
                    cmd: "Backwards"
                },
                {
                    title: "<a class='context_menu_link'> <i class='fa fa-reply-all'></i> Send To Back</a>",
                    cmd: "ToBack"
                },
                {
                    title: "<a class='context_menu_link'> <i class='fa fa-object-ungroup'></i> Deselect All</a>",
                    cmd: "DeselectAll"
                },
                {
                    title: "<a class='context_menu_link'> <i class='fa fa-eye'></i> Toggle Only Mj</a>",
                    cmd: "ToggleOnlyMj"
                },
                {title: "<a class='context_menu_link'> <i class='fa fa-trash'></i> Delete</a>", cmd: "Remove"},
            ], select: function (event, ui) {

                for (var x in this.rightClickedItems) {
                    var item = this.rightClickedItems[x];
                    if (ui.cmd == 'ToFront') {
                        item.bringToFront();
                        item.set("z_index", this.canvas.getObjects().indexOf(item));
                        this.canvas.renderAll();
                        this.setDirty()
                    } else if (ui.cmd == 'Forward') {
                        item.bringForward();
                        item.set("z_index", this.canvas.getObjects().indexOf(item));
                        this.canvas.renderAll();
                        this.setDirty()
                    } else if (ui.cmd == 'Backwards') {
                        item.sendBackwards();
                        item.set("z_index", this.canvas.getObjects().indexOf(item));
                        this.canvas.renderAll();
                        this.setDirty()
                    } else if (ui.cmd == 'ToBack') {
                        item.sendToBack();
                        item.set("z_index", this.canvas.getObjects().indexOf(item));
                        this.canvas.renderAll();
                        this.setDirty()
                    } else if (ui.cmd == 'Remove') {
                        if (item instanceof fabric.Group) {
                            var all = item.getObjects();
                            this.canvas.discardActiveGroup();
                            for (var i in all) {
                                this.canvas.remove(all[i]);
                            }
                        }
                        else
                            item.remove();
                        this.setDirty()
                    }
                    else if (ui.cmd == 'DeselectAll') {
                        this.canvas.deactivateAll().renderAll();
                    }
                    else if (ui.cmd == 'ToggleOnlyMj') {
                        this.canvas.deactivateAll().renderAll();
                        item.set('OnlyMj', !item.get('OnlyMj'));
                        if (item.get('OnlyMj')) {
                            item.set('opacity', 0.5);
                        }
                        else {
                            item.set('opacity', 1);
                        }
                        this.canvas.renderAll();
                        this.setDirty();
                    }
                }
            }.bind(this)
        });

        $('#PlateauCanvas .canvas-container').bind('contextmenu', function (env) {
            var x = env.offsetX;
            var y = env.offsetY;
            var selectedItem = this.canvas.getObjects('custom-image');
            var itemS = this.canvas.getActiveObject();
            var items = this.canvas.getActiveGroup();
            if (itemS)
                selectedItem.push(itemS);
            if (items) {
                var d = items.width;
                var h = items.height;
                if (x >= (items.left) && x <= (items.left + d)) {
                    if (y >= (items.top) && y <= (items.top + h)) {
                        this.rightClickedItems = [items];
                        return true;
                    }
                }
            }
            for (var i = selectedItem.length; i--; i >= 0) {
                var item = selectedItem[i];
                var d = item.width;
                var h = item.height;
                if (x >= (item.left) && x <= (item.left + d)) {
                    if (y >= (item.top) && y <= (item.top + h)) {
                        this.rightClickedItems = [item];
                        return true;
                    }
                }
            }
            return false; //stops the event propigation
        }.bind(this));

        this.canvas.on('object:modified', function (options) {
            this.setDirty()
        }.bind(this));
    }
 
/*canvas.on('mouse:over', function(e) {
    e.target.set('fill', 'red');
    canvas.renderAll();
  });

  canvas.on('mouse:out', function(e) {
    e.target.set('fill', 'green');
    canvas.renderAll();
  });

// Adding objects to the canvas...


/*var canvas = new fabric.Canvas(document.getElementById('canvas'));
console.log(JSON.stringify(canvas)); // '{"objects":[],"background":""}'

canvas.add(new fabric.Rect({
  left: 10,
  top: 10,
  height: 50,
  width: 50,
  fill: 'green',
     stroke:'black'
}));
canvas.renderAll();

console.log(JSON.stringify(canvas));//logs the string representation
console.log(canvas.toObject());//logs canvas as an object
console.log(canvas.toSVG());//logs the SVG representation of canvas*/