function visiblepnj(joueurvue,listejoueur,iddoc){
    listejoueur = listejoueur.split("|");
    joueurvue = joueurvue.split("|");
    message='<h1>Rendre visible pour : </h1>'+
    '<p>';
    for (i = 0; i < listejoueur.length; i++) {
        visible=false;
        opacity='0.5';
        for(j=0;j<joueurvue.length; j++){
            if(listejoueur[i]==joueurvue[j]){
                visible=true;
                opacity='1';
            }
        }
        message+='<span class="listejoueur">'+$('#joueur'+listejoueur[i]+' input.nom').val()+
        '<span class="visiblepnj" id="visiblepnj'+listejoueur[i]+'" value="'+listejoueur[i]+'" style="opacity:'+opacity+'" onclick="listevisiblepnj('+listejoueur[i]+','+iddoc+')"></span>'+
        '</span>';
    }
    message+=''+
    '<input class="bouton_save" type="button" value="Validez" onclick="fermer_messagepartie();"></p>';
    affichemessagepartie(message);
    
}
function listevisiblepnj(id,iddoc){
    test=$('#visiblepnj'+id).css('opacity')
    if(test==1){
        $('#visiblepnj'+id).css({'opacity':'0.5'});
    }else {
        $('#visiblepnj'+id).css({'opacity':'1'});
    }  
    listevisibles='';  
    i=0;
    $(".visiblepnj").each(function() {
        if($(this).css('opacity')==1){
            if(i>0){
                listevisibles+='|';
            }
            listevisibles+=$(this).attr('value');
            i++;
        }
    });
    if(listevisibles==''){
        listevisibles=null;
    }
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                savevispnj:'oui',
                iddoc:iddoc,
                vis:listevisibles,
            },
            success: function(){
                //synchodpnjvis();
            },
            fail: function(){
            },
    });
}
function changecontenupnj(id){
    titre=$('#nompnj-'+id).val();
    contenu=$('#infopnj-'+id).val();
    contenumj=$('#mjpnj-'+id).val();
    imgpnj=$('#imgpnj-'+id).val();
    if (imgpnj==''){
        imgpnj='img/image.jpg';
    }
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                savepnj:'oui',
                iddoc:id,
                titre:titre,
                contenu:contenu,
                contenumj:contenumj,
                imgpnj:imgpnj,
            },
            success: function(){
                synchopnjvis();
            },
            fail: function(){
            },
    });
}
function nouveaupnj(){
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                newpnj:'oui',
            },
            success: function(){
               synchopnjvis();
            },
            fail: function(){
            },
    });
}
function refreshpnj(){
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            dataType: 'html',
            data: {
                refreshpnj:'oui',
            },

            success: function(content){
                result=content.substr(52)
                $('#info-3').html(result);
            },
            fail: function(){
            },
    });
}
function validsupprpnj(id){
      message='<h1>Validation suppression</h1>'+
      '<p><input class="bouton_save" type="button" value="Valider" onclick="supprpnj('+id+');"> <input class="bouton_save" type="button" value="Annuler" onclick="fermer_messagepartie();"></p>';
    affichemessagepartie(message);
}
function supprpnj(id){
    fermer_messagepartie();
    $.ajax({
            type: "POST",
            url: "./controllers/controllersave.php",
            async:true,
            ifModified:true,
            data: {
                supprpnj:'oui',
                iddoc:id,
            },
            success: function(){
                synchopnjvis();
            },
            fail: function(){
            },
    });
}