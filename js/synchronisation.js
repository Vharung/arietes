socket = io.connect('https://arietes.virtuajdr.net');
//socket = io.connect('http://localhost:3000');
//remplacer 10 par l'id du joueur pour pouvoir par la suite filtrer les messages a l'envoi
socket.emit('add-user',{'idplayer': idplayer, 'user': user,'idpartie':iddelapartie});

//menu contextuel desactyivé
document.addEventListener("contextmenu", function (event) {
 // event.preventDefault();
});

//tchat
function synchotchat(){
    socket.emit('tchat',{'idplayer':idplayer,'idgame':iddelapartie});
    return false; 
}
//point equipe
function synchopoint(points){
    socket.emit('point',{'msg':points,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//modif dés
function synchodes(){
    var x = $('#lancer').html();
    var y = $('#bonuscomp').html();
    var z = $('#bonusmj').val();
    socket.emit('des',{'lancer':x,'bonus':y,'mj':z,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//lancer des
function synchrolancer(id){
    socket.emit('lanceurd',{'id':id,'idplayer':idplayer,'idgame':iddelapartie});
    return false; 
}
//lancer retour
function synchronistionroll(dr,varcalldede,nomlanceur,valeurcolor,valeur2color,resultatdes,idlancer,destinataire){
    resultRoll(resultatdes,nomlanceur,valeurcolor);
    socket.emit('roll',{'dr': dr,'varcalldede': varcalldede,'nomlanceur': nomlanceur,'resultatdes': resultatdes,'idlancer':idlancer,'colorlanceur':valeurcolor,'color2lanceur':valeur2color,'destinataire':destinataire,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//message
function synchromessage(data,couleur){
    socket.emit('textuel',{'messages':data,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//mjvaleur
function synchromj(data){
    socket.emit('mjvaleur',{'donne':data,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//posture
function synchroposture(compte,iduser,idpartie){
    name=$('#joueur'+iduser+' input.nom').val();
    refreshposture(compte,name,iduser);
    socket.emit('posturecolonne',{'compte':compte,'iduser':iduser,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//cache dés 
function synchrocache(){
    socket.emit('cacher',{'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//tour info
function synchotour(id){
    socket.emit('tours',{'id':id,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//nom joueur
function synchronom(id,nom){
    socket.emit('noms',{'iduser':id,'names':nom,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//bar synchro
function synchrobar(id,nom,color,total,max,min){
    socket.emit('bar',{'iduser':id,'barnom':nom,'newcolor':color,'total':total,'idplayer':idplayer,'idgame':iddelapartie,'max':max,'min':min});
    return false;
}
//bar bouclier principal
function synchrobouclier(id,valeur){
    socket.emit('boucl',{'iduser':id,'valeur':valeur,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//bar bouclier secondaire
function synchroboucliersec(id,valeur){
    socket.emit('bouclsec',{'iduser':id,'valeur':valeur,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//modif visibilité doc
function synchodocvis(){
    socket.emit('docvis',{'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//modif visibilité doc
function synchopnjvis(){
    socket.emit('pnjvis',{'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
//avantage
function synchroavant(avant,id){
    socket.emit('avant',{'iduser':id,'avant':avant,'idplayer':idplayer,'idgame':iddelapartie});
    return false; 
}
//etat
function synchroetat(avant,id){
    socket.emit('etat',{'iduser':id,'avant':avant,'idplayer':idplayer,'idgame':iddelapartie});
    return false; 
}
//fiche
function synchrofiche(iduser,contenu){
    socket.emit('fiche',{'iduser':iduser,'contenu':contenu,'idplayer':idplayer,'idgame':iddelapartie});
    return false; 
}
function synchroedit(iduser,contenu){
    socket.emit('edit',{'iduser':iduser,'contenu':contenu,'idplayer':idplayer,'idgame':iddelapartie});
    return false; 
}
//color
function synchrocolor(iduser,color1,color2){
    socket.emit('color',{'iduser':iduser,'color1':color1,'color2':color2,'idplayer':idplayer,'idgame':iddelapartie});
    return false; 
}
function synchromusique(idvideo,action,valeur){
    console.log('synchromusique'+idvideo+' '+action+' '+valeur)
    socket.emit('musicale',{'idvideo':idvideo,'idplayer':idplayer,'idgame':iddelapartie,'action':action,'valeur':valeur});
    return false;
}
function syncupdateVideo(idvideo){
    var value = $('#embed'+idvideo).val();
    var srcmusic=$('#embed'+idvideo+' option:selected').attr('type');
    if(srcmusic=="youtube"){
        var urlmusic=$('#embed'+idvideo+' option:selected').attr('url');
    }
    socket.emit('updatevid',{'idvideo':idvideo,'idplayer':idplayer,'idgame':iddelapartie,'value':value,"source":srcmusic,"urlmusic":urlmusic});
    return false;
}
/*function synchrotimer(idvideo,timer){
    socket.emit('temp',{'idvideo':idvideo,'timer':timer,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}
function synchrovisiblemusique(idvideo,urlmusic,nommusique,opacity){
   socket.emit('visimus',{'idvideo':idvideo,'urlmusic':urlmusic,'nommusique':nommusique,'opacity':opacity,'idplayer':idplayer,'idgame':iddelapartie});
   return false; 
}*/
/*plateau*/
function plateaunode(obj,player,idplateau){
    socket.emit('plateaunode',{'obj':obj,'player':player,'idplateau':idplateau,'idplayer':idplayer,'idgame':iddelapartie});
    return false;
}


/*********************************************/


$(function () {
    //reception de tchat
    socket.on('chat', function(){
        refreshtchat();
        clinotage();
    });
    //reception d'un message Destin
    socket.on('pt', function(DestinData){
        $('#pointequipe').val(DestinData);
    });
     //reception changement de dés
    socket.on('modifdes', function(DesDate){
        lancer=DesDate.lancer;
        bonus=DesDate.bonus;
        mj=DesDate.mj;
        $('#lancer').html(lancer);
        $('#bonuscomp').html(bonus);
        $('#bonusmj').val(mj);
        calculroll();
    });
    //reception lanceur de dés
    socket.on('lanceurds', function(LanceurData){
        var audio = new Audio('../sound/lancer.wav');
        audio.play();
        id=LanceurData.id;
        setTimeout(function () {
            $('#joueur'+id+' .diceindic').css({'display':'none'});
        }, 2000)
        $('#joueur'+id+' .diceindic').css({'display':'block'});                
    });
    socket.on('resultroll', function(RollDate){
        $('#set').val(RollDate.dr)
        calldede=RollDate.varcalldede
        nomlanceur=RollDate.nomlanceur
        valeurcolor=RollDate.colorlanceur
        valeur2color=RollDate.color2lanceur
        resultatdes=RollDate.resultatdes
        idlancer=RollDate.idlancer
        destinataire=RollDate.destinataire
        if(destinataire>0 && idplayer==idlancer){
            lancerlesdede(idlancer);
        } else if(destinataire>0 && idplayer==destinataire){
            lancerlesdede();
        } else if (destinataire==0){
           lancerlesdede(idlancer)
        }       
        reset_roll()
    });
    socket.on('textuels', function(TexteData){
        messages=TexteData.messages;
        couleur=TexteData.couleur;
        messagedes(messages); 
    });
    socket.on('mjvaleurs', function(MjData){
        messages=MjData.donne;
        if(idmj==idplayer){
            messagedes(messages);  
        } 
    });
    socket.on('posturecolonnes', function(PostureData){
        compte=PostureData.compte;
        iduser=PostureData.iduser;
        ajoutposture(compte,iduser);
    });
    socket.on('cache', function(){
        refreshcacher();
    });
    socket.on('tour', function(idTour){
        refreshtour(idTour);
    });
    socket.on('nom', function(NomData){
        name=NomData.names;
        iduser=NomData.iduser;
        $('#joueur'+iduser+' input.nom').val(name);
        $('#nomedit'+iduser).val(name);
        $('#fiche_joueur'+iduser+' input').val(name);
        
    });
    socket.on('bars', function(BarData){
        newcolor=BarData.newcolor;
        iduser=BarData.iduser;
        barnom=BarData.barnom;
        total=BarData.total;
        max=BarData.max;
        min=BarData.min;
        if(total>=100){total=100;}
        if(total<=0){total=0;}
        px=total*280/100;
        $('#bar'+barnom+iduser).css({'width':px+'px','background':newcolor,"transition":'1s'});
        $('#'+barnom+iduser).css({'color':newcolor,"transition":'1s'});
        $('#'+barnom+iduser).val(min);
        $('#'+barnom+'max'+iduser).val(max);
        $('#'+barnom+iduser).css({'color':newcolor,"transition":'1s'});
    });
    socket.on('boucls', function(BouclData){
        iduser=BouclData.iduser;
        valeur=BouclData.valeur;
        $('#bouclier-'+iduser).val(valeur);
    });
    socket.on('bouclsecs', function(BouclsecData){
        iduser=BouclsecData.iduser;
        valeur=BouclsecData.valeur;
        $('#boucliersec-'+iduser).val(valeur);
    });
    socket.on('docviss', function(){
        refreshdoc();
    });
    socket.on('pnjviss', function(){
        refreshpnj();
    });
    socket.on('avants', function(AvantData){
        iduser=AvantData.iduser;
        avant=AvantData.avant;
        refreshavant(avant,iduser)
    });
    socket.on('etats', function(EtatData){
        iduser=EtatData.iduser;
        avant=EtatData.avant;
        refreshetat(avant,iduser)
    });
    socket.on('fiches', function(FicheData){
        iduser=FicheData.iduser;
        contenu=FicheData.contenu;
        $('#fiche-'+iduser).html(contenu);
    });
    socket.on('edits', function(FicheData){
        iduser=FicheData.iduser;
        contenu=FicheData.contenu;
        fiche='<input value="X" class="sans" style="position:absolute;top:5px;right:5px;color:red;" type="button" onclick="fermermodif('+iduser+');">'+contenu+
            '<input value="Valider" class="bouton_save" type="button" onclick="validmodif('+iduser+'),fermermodif();"> <input value="Importer" class="bouton_save" type="button" onclick="importation('+iduser+')"> <input value="Exporter" class="bouton_save" type="button" onclick="exportation('+iduser+')"></div>';
            $('#edit-'+iduser).html(fiche)
    });
    socket.on('colors', function(ColorData){
        iduser=ColorData.iduser;
        color1=ColorData.color1;
        color2=ColorData.color2;
        $('#fiche_joueur'+iduser).css({'background':color1});
        $('#fiche_joueur'+iduser+' input').css({'color':color2});  
        $('#tour'+iduser).css({'background':color1});
    });
    /*Musique*/
    socket.on('musicales', function(idmusi){
        idvideo=idmusi.idvideo;
        action=idmusi.action;
        valeur=idmusi.valeur;
        synchromus(idvideo,action,valeur);
    });
    socket.on('updatevids', function(idmusi){
        idvideo=idmusi.idvideo;
        value=idmusi.value;
        source=idmusi.source;
        urlmusic=idmusi.urlmusic;
        updateVideo(idvideo,value,source,urlmusic);
    });
    /*socket.on('temps', function(TempsData){
        idvideo=TempsData.idvideo;
        timervideo=TempsData.timer;
    });
    socket.on('visimuss', function(MusData){
        idvideo=MusData.idvideo;
        urlmusic=MusData.urlmusic;
        nommusique=MusData.nommusique;
        opacity=MusData.opacity;
        controlemusique(idvideo,urlmusic,nommusique,opacity)
    });*/
    /*plateau*/
    socket.on('plateaunodes', function(PlateauData){
        obj=PlateauData.obj;
        player=PlateauData.player;
        idplateau=PlateauData.idplateau;
        if(idplayer==player || player==0){
            loadcanvas(obj);
        }
    });
});
