function mouseButton(ev,id) {
    //name=$('#joueur'+id+' input.nom').val();
    if (ev.which == 1) {
    }
    if (ev.which == 3) {
        synchotour(id)
    }
}
function modifnom(id){
    nom=$('#joueur'+id+' input.nom').val();
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            refreshnom:'oui',
            id_player: id,
            nom:nom,
        },
        success: function(content){
            synchronom(id,nom);
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    });
}
function modifeditnom(id){
    nom=$('#nomedit'+id).val();
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            refreshnom:'oui',
            id_player: id,
            nom:nom,
        },
        success: function(content){
            synchronom(id,nom);
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    });
}
function modifeditimage(id){
    img=$('#imgedit'+id).val();
    $('#joueuredit'+id+' .image').css({'background':"url('"+img+"') no-repeat top center","background-size":"cover"})
    $('#joueur'+id+' .image').css({'background':"url('"+img+"') no-repeat top center","background-size":"cover"});
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            saveimg:'oui',
            id_player: id,
            img:img,
            nom:$('#joueur'+id+' input.nom').val(),
            color1:$('#couleurprincipale'+id).val(),
            color2:$('#couleursecondaire'+id).val(),
        },
        success: function(content){
            //synchroimg(id,img);
        },
        fail: function(content){
            alert("Echec AJAX : "+content);
        },
    });
}
function augmente(bar,id_player){
    min=$('#'+bar+id_player).val();
    min=parseInt(min)+1;
    $('#'+bar+id_player).val(min);
    modifbar(bar,id_player);
}
function diminue(bar,id_player){
    min=$('#'+bar+id_player).val();
    min=parseInt(min)-1;
    $('#'+bar+id_player).val(min);
    modifbar(bar,id_player);
}
function modifbar(bar,id_player){
    max=$("#"+bar+"max"+id_player).val(),
    min=$("#"+bar+id_player).val(),
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            savebar:'oui',
            id_player: id_player,
            nombar:bar,
            max:max,
            min:min
        },
        success: function(content){
            total=parseInt(min)*100/parseInt(max);
            color=$('#'+bar+'max'+id_player).css('color');
            if(total<=15){
                //newcolor='linear-gradient(to right, rgb(165, 42, 42) 60%,'+color+' 100%)';
                newcolor='#8B0000';
            }else {
                newcolor=color;
            }
            synchrobar(id_player,bar,newcolor,total,max,min);   
        },
        fail: function(content){
            
        },
    });
}
function modifbouclier(id){
    valeur=$('#bouclier-'+id).val();
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            savebouclier:'oui',
            id_player: id,
            valeur:valeur,
            nom:$('#joueur'+id+' input.nom').val(),
            color1:$('#couleurprincipale'+id).val(),
            color2:$('#couleursecondaire'+id).val(),
        },
        success: function(content){
            synchrobouclier(id,valeur);
        },
        fail: function(content){
            
        },
    });
}
function modifboucliersec(id){
    valeur=$('#boucliersec-'+id).val();
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            saveboucliersec:'oui',
            id_player: id,
            valeur:$('#boucliersec-'+id).val(),
            nom:$('#joueur'+id+' input.nom').val(),
            color1:$('#couleurprincipale'+id).val(),
            color2:$('#couleursecondaire'+id).val(),
        },
        success: function(content){
            synchroboucliersec(id,valeur);
        },
        fail: function(content){
            
        },
    });
}
function savecolor(iduser){
    color1=$('#couleurprincipale'+iduser).val();
    color2=$('#couleursecondaire'+iduser).val();
    nom=$('#joueur'+iduser+' input.nom').val();
    $.ajax({
        type: "POST",
        url: "./controllers/controllersave.php",
        async:true,
        ifModified:true,
        data: {
            savecolor:'oui',
            id_player: iduser,
            nom:nom,
            color1:color1,
            color2:color2,
        },
        success: function(content){
            synchrocolor(iduser,color1,color2);
        },
        fail: function(content){
            
        },
    }); 
}