<?php
class liste{
	private $_nom_partie;
	private $_nom_template;
	private $_nom_mj;
	function __construct($nom_partie,$nom_template,$nom_mj){
		$this->_nom_partie = $nom_partie;
        $this->_nom_template = $nom_template;
        $this->_nom_mj = $nom_mj;
	}
	public function tablerejoins($iduser){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `groupe_liste`  WHERE id_user = '".$iduser."'");
		$groupe= array();
		$idgroupe=array();

		while ($result = mysqli_fetch_assoc($sql)){
			$sql2=mysqli_query($mysqli,"SELECT * FROM `tables`  WHERE id_user = '".$iduser."' AND groupe = '".$result['id']."'");
			$idpartie=array();
			while ($resulta = mysqli_fetch_assoc($sql2)){
				$idpartie[]=$resulta['id_liste'];
			}
			$groupe[] = array('id'=>$result['id'],'nom'=>$result['nom_groupe'],'partie'=>$idpartie);
			$idgroupe[]= $result['id'];
		}
		$sql3=mysqli_query($mysqli,"SELECT * FROM `tables`  WHERE id_user = '".$iduser."' AND groupe IS NULL");
		$tableau = array();
		while ($resultat= mysqli_fetch_assoc($sql3)){
			$tableau[] = $resultat['id_liste'];
		}
		return $infoliste = array('groupe' => $groupe,'sansgroupe'=>$tableau,'idgroupe'=>$idgroupe);
	}
	public function nomtable($id){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `liste`  WHERE id = '".$id."'");
		$result=mysqli_fetch_assoc($sql);
		$tableau = array();
		$tableau []=$result['name'];
		$tableau []=$result['template'];
		$tableau []=$result['mj'];
		$tableau []=$result['id'];
		$tableau []=$result['color1'];
		$tableau []=$result['color2'];
		return $tableau;
	}
	public function listetemplate(){
		$con = new Model();
		$iduser=$_SESSION['iduser'];
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `template` WHERE id_user=0 OR id_user=".$iduser." ORDER BY `name` ASC"); 
		$tableau = array();
		while ($result = mysqli_fetch_assoc($sql)){
			$tableau[$result['id']] =$result['name'];
		}
		return $tableau;
	}
	public function nomtemplate($id){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `template`  WHERE id = '".$id."'");
		$result=mysqli_fetch_assoc($sql);
		return $this->_nom_template=$result['name'];
	}
	public function nommj($id){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `user`  WHERE id = '".$id."'");
		$result=mysqli_fetch_assoc($sql);
		return $this->_nom_mj=$result['name'];
	}
	public function nouvelletable($nomtable,$iduser,$idtemplate){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql =  mysqli_query($mysqli,"INSERT INTO liste (name,template,mj) VALUES ('".$nomtable."','".$idtemplate."','".$iduser."')");
		$sql2 = mysqli_query($mysqli,"SELECT `id` FROM `liste` WHERE `mj` = '$iduser' and  id=LAST_INSERT_ID()");
   		$resultat_game = mysqli_fetch_array($sql2,MYSQLI_ASSOC);
   		$id_game = $resultat_game['id']; 
		$sql_table=mysqli_query($mysqli,"INSERT INTO tables (id_user, id_liste, visible, spetacteur) VALUES ('".$iduser."', '".$id_game."',0,0)");
	}
	public function changetemplate($idtable,$idtemplate){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql =  mysqli_query($mysqli,"UPDATE `liste` SET `template` = '".$idtemplate."' WHERE `id` = ".$idtable." ORDER BY `name` ASC");
	}
	public function supprimeliste($idtable){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql =  mysqli_query($mysqli,"DELETE FROM `liste` WHERE `id` = ".$idtable);
		$sql =  mysqli_query($mysqli,"DELETE FROM `tables` WHERE `id_liste` = ".$idtable);
		//$sql =  mysqli_query($mysqli,"DELETE FROM `fiche` WHERE `id_table` = ".$idtable);
		$sql =  mysqli_query($mysqli,"DELETE FROM `fiche_block` WHERE `id_table` = ".$idtable);
	}
	public function quitterliste($idtable,$iduser){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql = mysqli_query($mysqli,"DELETE FROM `tables` WHERE `id_liste` = ".$idtable." AND `id_user` = ".$iduser);
	}
	public function changecolor($idtable,$color1,$color2){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql =  mysqli_query($mysqli,"UPDATE `liste` SET `color1` = '".$color1."', `color2` = '".$color2."' WHERE `liste`.`id` = ".$idtable);
	}
	public function listetemplateperso($iduser){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `template` WHERE id_user=".$iduser." ORDER BY `name` ASC"); 
		$tableau = array();
		while ($result = mysqli_fetch_assoc($sql)){
			$tableau[$result['id']] =$result['name'];
		}
		return $tableau;
	}
	public function listegroupe($iduser){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `groupe_liste`  WHERE id_user = '".$iduser."'");
		$nom_groupe= array();
		$idgroupe=array();
		while ($result = mysqli_fetch_assoc($sql)){
			$nom_groupe[] = $result['nom_groupe'];
			$idgroupe[]= $result['id'];
		}
		return $infoliste = array('nom_groupe' => $nom_groupe,'idgroupe' => $idgroupe );
	}
	public function supprimegroupe($idgroupe){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql = mysqli_query($mysqli,"DELETE FROM `groupe_liste` WHERE `id` = ".$idgroupe);
		$sql=mysqli_query($mysqli,"UPDATE `tables` SET `groupe` = null WHERE `groupe` =". $idgroupe);
	}
	public function newgroupe($newgroupe,$iduser){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql = mysqli_query($mysqli,"INSERT INTO groupe_liste (nom_groupe, id_user) VALUES ('".$newgroupe."', '".$iduser."')");
	}
	public function modifgroupe($idgroupe,$nom){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql=mysqli_query($mysqli,"UPDATE `groupe_liste` SET `nom_groupe` = '".$nom."' WHERE `groupe_liste`.`id` = ".$idgroupe);
	}
	public function addgroupe($idgroupe,$idparti){
		$con = new Model();
		$mysqli=($con->connexion());
		print("UPDATE `tables` SET `groupe` = '".$idgroupe."' WHERE `tables`.`id` = ".$idparti);
		$sql=mysqli_query($mysqli,"UPDATE `tables` SET `groupe` = '".$idgroupe."' WHERE `tables`.`id_liste` = ".$idparti);
	}
	public function supgroupe($idparti){
		$con = new Model();
		$mysqli=($con->connexion());

		$sql=mysqli_query($mysqli,"UPDATE `tables` SET `groupe` = null WHERE `tables`.`id_liste` = ".$idparti);
	}
	public function modinompartie($idparti,$nompartie){
		$con = new Model();
		$mysqli=($con->connexion());
		$sql=mysqli_query($mysqli,"UPDATE `liste` SET `name` = '".$nompartie."' WHERE `liste`.`id` = ".$idparti);
	}
	

}