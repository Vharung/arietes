<?php
class tchat{
	private $_idpartie;
	function __construct($idpartie){
		$this->_idpartie = $idpartie;
	}
	public function affichetchat($idpartie,$iduser){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql_dialogue = mysqli_query($mysqli, "SELECT * FROM tchat, user WHERE id_partie = '$idpartie' AND id = id_exp AND (id_dest IN (0,'$iduser') OR id_exp = '$iduser')");
		$content ="";
		$style_other="float:left;background:#81B0B2;color:#fff;";
		$style_other_mp="float:left;background:#D6EAD4;color:#000";
		$style_my="float:right;background:#FFDC7F;color:#000;text-align:right;";
		$style_my_mp="float:right;background:#FFECBC;color:#000;text-align:right;";
		while ($resultat_dialogue = mysqli_fetch_array($sql_dialogue, MYSQLI_ASSOC)) {
		    if ($iduser == $resultat_dialogue['id_exp']&& $resultat_dialogue['id_dest'] == 0){
		        $style=$style_my;
		        $dit='dit';
		    }else if ($iduser == $resultat_dialogue['id_exp'] ){
		    	$nomjoueur=new joueur('','');
		    	$nomjoueur=$nomjoueur->nomjoueurtable($resultat_dialogue['id_dest']);
		    	$dit='dit en mp à '.$nomjoueur;
		    	$style=$style_my_mp;
		    }else if ($iduser == $resultat_dialogue['id_dest']){
		    	$dit='me dit en mp';
		    	$style=$style_other_mp;
		    }else {
		        $style=$style_other;
		        $dit='dit';
		    }
		    
		    switch($resultat_dialogue['type'])
		    {
		        case 1:
		            $intro="";
		            $conclusion="";
		        break;
		        case 2:
		            $dit='fait un jet de dès';
		            $intro="";
		            $conclusion="";
		        break;
		        case 3:
		            $dit='fait un jet de dès';
		            $intro="<span style='color:green'>";
		            $conclusion="</span>";
		        break;
		        case 4:
		            $dit='fait un jet de dès';
		            $intro="<span style='color:crimson'>";
		            $conclusion="</span>";
		        break;
		        case 5:
		            $dit='fait un jet de dès';
		            $intro="<span style='color:red'>";
		            $conclusion="</span>";
		        break;
		        case 6:
		            $dit='fait un jet de dès';
		            $intro="<span style='color:white'>";
		            $conclusion="</span>";
		        break;
		        default:
		            $intro="";
		            $conclusion="";
		        break;
		    }
		    //gestion du timer$
		    $timestamp = strtotime($resultat_dialogue['date']);
		    $timestampNow = time();
		    if ($timestampNow - $timestamp < 120)
		    {
		        $timer="A l'instant";
		    }
		    else if ($timestampNow - $timestamp < 300)
		    {
		        $timer=="Moins de 5 min";
		    }
		    else if ($timestampNow - $timestamp < 900)
		    {
		        $timer="Moins de 15 min";
		    }
		    else if ($timestampNow - $timestamp < 3600)
		    {
		        $timer="Moins d\'une heure";
		    }
		    else if ($timestampNow - $timestamp < 21600)
		    {
		        $timer="Moins de 6 heures";
		    }
		    else {
		        $dt=new DateTime("@$timestamp");
		        $timer='Le '.$dt->format('d/m/Y à H:i:s');;
		    }
		    $content .="<span class='tchat' dit='".$resultat_dialogue['id_exp']."' style=\"".$style."\">";
		    $content .="<span class='reacteur'>".$timer." - ".$resultat_dialogue['name']." ".$dit." :</span><p>";
		    $content .=$intro;
		    $content .= $resultat_dialogue['text_tchat'];
		    $content .=$conclusion;
		    $content .="</p></span>";
		}
		return $content;
	} 
	public function videtchat($idpartie){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql =mysqli_query($mysqli,"DELETE FROM `tchat` WHERE `id_partie` = ".$idpartie);
	}
}