<?php
class partie{
	private $_id_joueur;
	function __construct($idjoueur){
        $this->_id_joueur = $idjoueur;
	}
	public function listemodule($idtemplate){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `template_block`  WHERE id_template = '".$idtemplate."' ORDER BY `block` ASC");
		$tableau = array();
		while ($result = mysqli_fetch_assoc($sql)){
			$module['id']=$result['id'];
			$module['name'] = $result['name'];
			$module['type'] = $result['type'];
			$module['block'] = $result['block'];
			$module['type'] = $result['type'];
			$module['valeur'] = $result['valeur'];
			$tableau[]=$module;
		}
		return $tableau;
	}
	public function joueurvisible($idpartie){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `tables`  WHERE id_liste = '".$idpartie."' AND visible=1");
		$tableau = array();
		while ($result = mysqli_fetch_assoc($sql)){
			$tableau[]=$result['id_user'];
		}
		return $tableau;
	}
	public function pointequipe($idpartie){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql = mysqli_query($mysqli,"SELECT * FROM `fiche_block`  WHERE id_table = '".$idpartie."' AND id_block=1"); 
		$count = mysqli_num_rows($sql);
      	if($count == 1) {
      		while ($result = mysqli_fetch_assoc($sql)){
				$tableau=$result['valeur1'];
			}
      	}else {
      		$tableau=0;
      	}
      	return $tableau;
	}
	public function avatarjoueur($idpartie,$iduser){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `fiche_block`  WHERE id_table = '".$idpartie."' AND id_user='".$iduser."' AND id_block=4");
		$id=null;$nom=null;$image=null;$couleurprincipale=null;$couleursecondaire=null;$bouclier=null;$boucliersec=null;$posture=null;$avantage=null;$etat=null;$turnorder=null;
		while ($result = mysqli_fetch_assoc($sql)){
			$id=$result['id'];
			$nom=$result['nom_comp'];
			$image =$result['valeur1'];
			$couleurprincipale=$result['valeur2'];
			$couleursecondaire=$result['valeur3'];
			$bouclier=$result['valeur4'];
			$boucliersec=$result['valeur5'];
			$posture=$result['valeur6'];
			$avantage=$result['valeur7'];
			$etat=$result['valeur8'];
			$turnorder=$result['valeur9'];				
		}
		return array('id'=>$id,'nom'=>$nom,'image'=>$image,'couleurprincipale'=>$couleurprincipale,'couleursecondaire'=>$couleursecondaire,'bouclier'=>$bouclier,'boucliersec'=>$boucliersec,'posture'=>$posture,'avantage'=>$avantage,'etat'=>$etat,'turnorder'=>$turnorder);
	}
	public function bouclierjoueur($idtemplate){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `template_block`  WHERE id_template = '".$idtemplate."' AND type=5");
		$count = mysqli_num_rows($sql);
      	if($count == 1) {
      		return 1;
      	}else {
      		return 0;
      	}
	}
	public function boucliersecjoueur($idtemplate){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `template_block`  WHERE id_template = '".$idtemplate."' AND type=6");
		$count = mysqli_num_rows($sql);
      	if($count == 1) {
      		return 1;
      	}else {
      		return 0;
      	}
	}
	public function barjoueur($idtemplate){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `template_block`  WHERE id_template = '".$idtemplate."' AND type=7");
		$count = mysqli_num_rows($sql);
		$arraydivision=[];
      	while ($result = mysqli_fetch_assoc($sql)){
		    $arraydivision[$result['name']] = $result['valeur'];
      	}
      	return $arraydivision;
	}
	public function barsjoueur($idpartie,$iduser,$nombar){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `fiche_block`  WHERE id_table = '".$idpartie."' AND id_user='".$iduser."' AND nom_comp='".$nombar."' AND id_block=5");	
		$count = mysqli_num_rows($sql);
      	if($count == 1) {
      		while ($result = mysqli_fetch_assoc($sql)){
				$valeur['min']=$result['valeur1'];
				$valeur['max']=$result['valeur2'];
      		} 
      	}else {
      		$valeur['min']=0;
			$valeur['max']=0;
      	}
      	return $valeur;    			
	}
	public function couleurjoueur($idpartie,$iduser){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `fiche_block`  WHERE id_table = '".$idpartie."' AND id_user='".$iduser."' AND id_block=4 ");
		$count = mysqli_num_rows($sql);
		if($count == 1) {
			while ($result = mysqli_fetch_assoc($sql)){
				$module['color1']=$result['valeur2'];
				$module['color2']=$result['valeur3'];	
			}		
		}else {
			$module['color1']='#4A90E2';
			$module['color2']='#ffffff';
		}
		return $module;
	}
	public function etatmjjoueur($idpartie,$iduser){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `fiche_block`  WHERE id_table = '".$idpartie."' AND id_user='".$iduser."' AND id_block=7");
		$module=array();
		while ($result = mysqli_fetch_assoc($sql)){
			$module[]=$result['valeur1'];
			$module[]=$result['valeur2'];	
		}
		return $module;
	}
	public function fichejoueur($idpartie,$iduser,$idblock){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `fiche_block`  WHERE id_table = '".$idpartie."' AND id_user='".$iduser."' AND id_block=".$idblock);
		$valeur1=null;$valeur2=null;$valeur3=null;$valeur4=null;$valeur5=null;$valeur6=null;$valeur7=null;$valeur8=null;$valeur9=null;$valeur10=null;$id=null;
		while ($result = mysqli_fetch_assoc($sql)){
			$valeur1=$result['valeur1'];
			$valeur2=$result['valeur2'];
			$valeur3=$result['valeur3'];
			$valeur4=$result['valeur4'];
			$valeur5=$result['valeur5'];
			$valeur6=$result['valeur6'];
			$valeur7=$result['valeur7'];
			$valeur8=$result['valeur8'];
			$valeur9=$result['valeur9'];
			$valeur10=$result['valeur10'];	
			$id=$result['id'];			
		}
		return array('0'=>$valeur1,'1'=>$valeur2,'2'=>$valeur3,'3'=>$valeur4,'4'=>$valeur5,'5'=>$valeur6,'6'=>$valeur7,'7'=>$valeur8,'8'=>$valeur9,'9'=>$valeur10,'id'=>$id);
	}
	public function listefichejoueur($idpartie,$iduser,$idblock){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `fiche_block`  WHERE id_table = '".$idpartie."' AND id_user='".$iduser."' AND id_block=".$idblock);
		$valeur1=array();
		$valeur2=array();
		$valeur3=array();
		$valeur4=array();
		$valeur5=array();
		$valeur6=array();
		$valeur7=array();
		$valeur8=array();
		$valeur9=array();
		$valeur10=array();
		$id=array();
		while ($result = mysqli_fetch_assoc($sql)){
			$valeur1[]=$result['valeur1'];
			$valeur2[]=$result['valeur2'];
			$valeur3[]=$result['valeur3'];
			$valeur4[]=$result['valeur4'];
			$valeur5[]=$result['valeur5'];
			$valeur6[]=$result['valeur6'];
			$valeur7[]=$result['valeur7'];
			$valeur8[]=$result['valeur8'];
			$valeur9[]=$result['valeur9'];
			$valeur10[]=$result['valeur10'];	
			$id[]=$result['id'];			
		}
		return array('valeur1'=>$valeur1,'valeur2'=>$valeur2,'valeur3'=>$valeur3,'valeur4'=>$valeur4,'valeur5'=>$valeur5,'valeur6'=>$valeur6,'valeur7'=>$valeur7,'valeur8'=>$valeur8,'valeur9'=>$valeur9,'valeur10'=>$valeur10,'id'=>$id);
	}
	public function docpartie($idpartie){
		$idcreateur=null;$nomdoc=null;$contenu=null;$contenu_mj=null;$visible=null;$id=null;
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `doc`  WHERE idpartie = ".$idpartie);
		while ($result = mysqli_fetch_assoc($sql)){
			$idcreateur[]=$result['iduser'];
			$nomdoc[]=$result['nomdoc'];
			$contenu[]=$result['contenu'];
			$contenu_mj[]=$result['contenu_mj'];
			$visible[]=$result['visible'];	
			$id[]=$result['id'];		
		}
		return array('idcreateur'=>$idcreateur,'nomdoc'=>$nomdoc,'contenu'=>$contenu,'contenu_mj'=>$contenu_mj,'visible'=>$visible,'id'=>$id);
	}
	public function pnjpartie($idpartie){
		$con = new Model();
		$nompnj=null;$image=null;$stat=null;$histoire=null;$notemj=null;$visible=null;$id=null;
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `pnj`  WHERE idpartie = ".$idpartie);
		while ($result = mysqli_fetch_assoc($sql)){
			$nompnj[]=$result['nompnj'];
			$image[]=$result['image'];
			$stat[]=$result['stat'];
			$histoire[]=$result['histoire'];	
			$notemj[]=$result['notemj'];
			$visible[]=$result['visible'];
			$id[]=$result['id'];		
		}
		return array('nompnj'=>$nompnj,'image'=>$image,'stat'=>$stat,'histoire'=>$histoire,'notemj'=>$notemj,'visible'=>$visible,'id'=>$id);
	}
	public function musicpartie($idmj,$idpartie){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `musique`  WHERE iduser = ".$idmj." ORDER BY dossier ASC, sousdossier ASC");
		while ($result = mysqli_fetch_assoc($sql)){
			$idmusique[]=$result['id'];
			$iduser[]=$result['iduser'];
			$dossier[]=$result['dossier'];
			$sousdossier[]=$result['sousdossier'];
			$nommusique[]=$result['nommusique'];
			$url[]=$result['url'];	
			$scr[]=$result['scr'];			
		}
		if(isset($idmusique)){
			/*$playlist=array();
			$lecture=array();
			$visible=array();
			$sql=mysqli_query($mysqli,"SELECT * FROM `playlist`  WHERE idpartie = ".$idpartie);
			while ($result = mysqli_fetch_assoc($sql)){
				$playlist[]=$result['idmusic'];	
				$lecture[]=$result['lecture_en_cours'];	
				$visible[]=$result['visible'];	
			}*/
			return array('idmusic'=>$idmusique,'iduser'=>$iduser,'nommusique'=>$nommusique,'url'=>$url,'scr'=>$scr,'dossier'=>$dossier,'sousdossier'=>$sousdossier,/*'playlist'=>$playlist',lecture'=>$lecture,'visible'=>$visible*/);
		}else{
			return;
		}
	}
	public function lectureplateau($idpartie){
		$con = new Model();
		$mysqli=($con->connexion()); 
		$sql=mysqli_query($mysqli,"SELECT * FROM `plateau`  WHERE id_table = ".$idpartie);
		$id=null;$contenu=null;$img=null;$nom=null;$joueur=null;
		while ($result = mysqli_fetch_assoc($sql)){
			$id[]=$result['id'];
			$contenu[]=$result['contenu'];
			$img[]=$result['img'];
			$nom[]=$result['nom'];
			$joueur[]=$result['joueur'];
		}
		return array('contenu'=>$contenu,'img'=>$img,'nom'=>$nom,'id'=>$id,'joueur'=>$joueur);
	}
}
