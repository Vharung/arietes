<?php
session_start();
require_once('../config/config.php');
require_once('../models/modelsave.php');
require_once('../models/modeltchat.php');
require_once('../models/modeljoueur.php');
$id_partie = $_SESSION['idpartie'];
$id_mj = $_SESSION['idmj'];
$id_player = $_SESSION['iduser'];;
$id_template = $_SESSION['template'];
$nom_joueur=$_SESSION['login_user'];
//tchat
if (!empty ($_POST['text'])){
	$text_tchat=$_POST['text'];
	$id_dest=$_POST['id_dest'];
	$type=$_POST['type'];
	$save=new save($id_partie);
	$save->savetchat($id_partie,$id_player,$id_dest,$type,$text_tchat);
}else if (!empty ($_POST['refreshtchat'])){
    $tchat=new tchat($id_partie);
    $dialogue=($tchat->affichetchat($id_partie,$id_player));
    echo $dialogue; 
}else if (!empty ($_POST['savepoint'])){
    $save=new save($id_partie);
    echo $save->savepoint($id_partie,$id_mj,$_POST['point']);
}else if (!empty ($_POST['saveposture'])){
    $save=new save($id_partie);
    $save->saveposture($id_partie,$_POST['id_player'],$_POST['posture'],$_POST['name'],$_POST['color1'],$_POST['color2']);
}else if (!empty ($_POST['saveavant'])){
    $save=new save($id_partie);
    $save->saveavant($id_partie,$_POST['id_player'],$_POST['avant'],$_POST['name'],$_POST['color1'],$_POST['color2']);
}else if (!empty ($_POST['saveetat'])){
    $save=new save($id_partie);
    $save->saveetat($id_partie,$_POST['id_player'],$_POST['avant'],$_POST['name'],$_POST['color1'],$_POST['color2']);
}else if (!empty ($_POST['refreshnom'])){
    $save=new save($id_partie);
    $save->savename($id_partie,$_POST['id_player'],$_POST['nom']);
}else if (!empty ($_POST['notemj'])){
    $save=new save($id_partie);
    $save->savenote($id_partie,$id_player,$_POST['valeurnb'],$_POST['note']);
}else if (!empty ($_POST['tourmj'])){
    $save=new save($id_partie);
    $save->savetour($id_partie,$_POST['id_player'],$_POST['etat_player'],$_POST['tour_player']);
}else if (!empty ($_POST['turnorder'])){
    $save=new save($id_partie);
    $save->saveturn($id_partie,$_POST['id_player'],$_POST['nb'],$_POST['name'],$_POST['color1'],$_POST['color2']);
}else if (!empty ($_POST['savebar'])){
    $save=new save($id_partie);
    $save->savebar($id_partie,$_POST['id_player'],$_POST['nombar'],$_POST['min'],$_POST['max']);
}else if (!empty ($_POST['savebouclier'])){
    $save=new save($id_partie);
    $save->savebouclier($id_partie,$_POST['id_player'],$_POST['nom'],$_POST['valeur'],$_POST['color1'],$_POST['color2']);
}else if (!empty ($_POST['saveboucliersec'])){
    $save=new save($id_partie);
    $save->saveboucliersec($id_partie,$_POST['id_player'],$_POST['nom'],$_POST['valeur'],$_POST['color1'],$_POST['color2']);
}else if (!empty ($_POST['savevisdoc'])){
    $save=new save($id_partie);
    $save->savevisdoc($_POST['iddoc'],$_POST['vis']);
}else if (!empty ($_POST['savedoc'])){
    $save=new save($id_partie);
    $save->savedoc($_POST['iddoc'],$_POST['titre'],$_POST['contenu'],$_POST['contenumj']);
}else if (!empty ($_POST['newdoc'])){
    $save=new save($id_partie);
    $save->newdoc($id_partie,$id_player);
}else if (!empty ($_POST['refreshdoc'])){
    $save=new save($id_partie);
    $doc=$save->docpartie($_SESSION['idpartie']);
    $listejoueurvisible=$save->joueurvisible($_SESSION['idpartie']);
    $idcreateur=$doc['idcreateur'];$titredoc=$doc['nomdoc'];$contenudoc=$doc['contenu'];$contenumjdoc=$doc['contenu_mj'];$visible=$doc['visible'];$listejoueur=$listejoueurvisible;$id=$doc['id'];
    require '../views/viewModuledoc.php';
}else if (!empty ($_POST['supprdoc'])){
    $save=new save($id_partie);
    $save->supprdoc($_POST['iddoc']);
}else if (!empty ($_POST['savevispnj'])){
    $save=new save($id_partie);
    $save->savevispnj($_POST['iddoc'],$_POST['vis']);
}else if (!empty ($_POST['savepnj'])){
    $save=new save($id_partie);
    $save->savepnj($_POST['iddoc'],$_POST['titre'],$_POST['contenu'],$_POST['contenumj'],$_POST['imgpnj']);
}else if (!empty ($_POST['newpnj'])){
    $save=new save($id_partie);
    $save->newpnj($id_partie);
}else if (!empty ($_POST['refreshpnj'])){
    $save=new save($id_partie);
    $pnj=$save->pnjpartie($_SESSION['idpartie']);
    $listejoueurvisible=$save->joueurvisible($_SESSION['idpartie']);
    $idcreateur=$nompnj=$pnj['nompnj'];$histoire=$pnj['histoire'];$notemj=$pnj['notemj'];$visible=$pnj['visible'];$image=$pnj['image'];$listejoueur=$listejoueurvisible;$id=$pnj['id'];
    require '../views/viewModulepnj.php';
}else if (!empty ($_POST['supprpnj'])){
    $save=new save($id_partie);
    $save->supprpnj($_POST['iddoc']);
}else if (!empty ($_POST['updatemodule'])){
    $save=new save($id_partie);
    $save->updatemodule($_POST['id'],$_POST['valeur1'],$_POST['valeur2'],$_POST['valeur3'],$_POST['valeur4'],$_POST['valeur5'],$_POST['valeur6'],$_POST['valeur7'],$_POST['valeur8'],$_POST['valeur9'],$_POST['valeur10']);
}else if (!empty ($_POST['savemodule'])){
    $save=new save($id_partie);
    $save->savemodule($id_partie,$_POST['idblock'],$_POST['iduser'],$_POST['nom_comp'],$_POST['valeur1'],$_POST['valeur2'],$_POST['valeur3'],$_POST['valeur4'],$_POST['valeur5'],$_POST['valeur6'],$_POST['valeur7'],$_POST['valeur8'],$_POST['valeur9'],$_POST['valeur10']);
}else if (!empty ($_POST['refreshedit'])){
    $save=new save($id_partie);
    $listejoueurvisible=$save->joueurvisible($_SESSION['idpartie']);
    require('../models/modelpartie.php');
    $partie=new partie($id_player);
    $listemodule=$partie->listemodule($_SESSION['template']);
    require('../controllers/controllermodulefiche.php');
    $fiche=$listeuser;
    foreach ($fiche as $value => $cle) {
        if($_POST['iduser']==$value){
            $b=1;
            for($x=1;$x<5;$x++){
                foreach ($cle['colone'.$x] as $key ) {
                    if($key['type']=='stat'){
                        require('../views/viewmoduleeditstat.php');
                        echo $table;
                    }else if($key['type']=='stat_sec'){
                        require('../views/viewmoduleeditstat.php');
                        echo $table;
                    }else if($key['type']=='text'){
                        require('../views/viewmoduleedittext.php');
                        echo $text;
                    }else if($key['type']=='competence'){
                        require('../views/viewmoduleeditcomp.php');
                        echo $comp;
                    }else if($key['type']=='inventaire'){
                        require('../views/viewmoduleeditinve.php');
                        echo $inve;
                    }else if($key['type']=='encombrement'){
                        require('../views/viewmoduleeditenco.php');
                        echo $enco;
                    }else if($key['type']=='connaissance'){
                        require('../views/viewmoduleeditconn.php');
                        echo $conn;
                    }
                    if($b==4){
                        echo '<div class="clear"></div>';
                        $b=0;
                    }
                    $b++;
                }
            }
        }
    }
}else if (!empty ($_POST['ajoutcomp'])) {
    $save=new save($id_partie);
    $save->newmodule($id_partie,$_POST['iduser'],$_POST['idblock'],$_POST['type']);
}else if (!empty ($_POST['supprcomp'])) {
    $save=new save($id_partie);
    $save->supprcomp($_POST['idblock'],$_POST['iduser'],$id_partie);
}else if (!empty ($_POST['refreshfiche'])){
    $save=new save($id_partie);
    $listejoueurvisible=$save->joueurvisible($_SESSION['idpartie']);
    require('../models/modelpartie.php');
    $partie=new partie($id_player);
    $listemodule=$partie->listemodule($_SESSION['template']);
    require('../controllers/controllermodulefiche.php');
    $fiche=$listeuser;
    foreach ($fiche as $value => $cle) {
        if($_POST['iduser']==$value){
            for($x=1;$x<5;$x++){
                echo '<div class="fichecolonne">';
                foreach ($cle['colone'.$x] as $key ) {
                    if($key['type']=='stat'){
                        require('../views/viewmodulefichestat.php');
                        echo $table;
                    }else if($key['type']=='stat_sec'){
                        require('../views/viewmodulefichestat.php');
                        echo $table;
                    }else if($key['type']=='text'){
                        require('../views/viewmodulefichetext.php');
                        echo $text;
                    }else if($key['type']=='competence'){
                        require('../views/viewmodulefichecomp.php');
                        echo $comp;
                    }else if($key['type']=='inventaire'){
                        require('../views/viewmoduleficheinve.php');
                        echo $inve;
                    }else if($key['type']=='encombrement'){
                        require('../views/viewmoduleficheenco.php');
                        echo $enco;
                    }else if($key['type']=='connaissance'){
                        require('../views/viewmoduleficheconn.php');
                        echo $conn;
                    }else if($key['type']=='posture'){
                        require('../views/viewmodulefichepost.php');
                        echo $post;
                    }else if($key['type']=='avantage'){
                        require('../views/viewmoduleficheavan.php');
                        echo $avan;
                    }else if($key['type']=='etat'){
                        require('../views/viewmoduleficheetat.php');
                        echo $etats;
                    }else if($key['type']=='editeur'){
                        require('../views/viewmoduleficheedit.php');
                        echo $edit;
                    }
                    if($x==4){
                        echo '<div class="clear"></div>
                            <input value="Modifier" class="bouton_save" type="button" onclick="ouvremodif('.$value.');">
                        <div class="clear"></div>';
                    }
                }
                echo '</div>';
            }
        }
    }
}else if(!empty ($_POST['saveimg'])){
    $save=new save($id_partie);
    $save->saveimg($id_partie,$_POST['id_player'],$_POST['nom'],$_POST['color1'],$_POST['color2'],$_POST['img']);
}else if(!empty ($_POST['savecolor'])){
    $save=new save($id_partie);
    $save->savecolor($id_partie,$_POST['id_player'],$_POST['nom'],$_POST['color1'],$_POST['color2']);
}else if(!empty ($_POST['savemusic'])){
    $save=new save($id_partie);
    $save->savemusic($id_player,$_POST['dossier'],$_POST['sousdossier'],$_POST['nommusic'],$_POST['url'],$_POST['scr']);
    /*Refresh liste*/
    $music=$save->musicpartie($_SESSION['idmj'],$_SESSION['idpartie']);
    $idmusic=$music['idmusic'];
    $iduser=$music['iduser'];
    $nommusique=$music['nommusique'];
    $url=$music['url'];
    $dossier=$music['dossier'];
    $sousdossier=$music['sousdossier'];
    $scr=$music['scr'];
    require('../views/viewmodulemusicplaylist.php');
    print($listmusics);
}else if(!empty ($_POST['suprmusic'])){
    $save=new save($id_partie);
    $save->suprmusic($_POST['id']);
    /*Refresh liste*/
    $music=$save->musicpartie($_SESSION['idmj'],$_SESSION['idpartie']);
    $idmusic=$music['idmusic'];
    $iduser=$music['iduser'];
    $nommusique=$music['nommusique'];
    $url=$music['url'];
    $dossier=$music['dossier'];
    $sousdossier=$music['sousdossier'];
    $scr=$music['scr'];
    require('../views/viewmodulemusicplaylist.php');
    print($listmusics);
}/*else if(!empty ($_POST['ajoutlecture'])){
    $save=new save($id_partie);
    $save->saveplaylist($id_partie,$_POST['id']);
}else if(!empty ($_POST['supprlecture'])){
    $save=new save($id_partie);
    $save->supprlecture($_POST['id'],$id_partie);
}else if(!empty ($_POST['visiblemusic'])){
    $save=new save($id_partie);
    $save->visiblemusic($_POST['id'],$id_partie,$_POST['visible']);
}else if(!empty ($_POST['lectureencours'])){
    $save=new save($id_partie);
    $save->lectureencours($_POST['id'],$id_partie,$_POST['lecture']);
}*/else if(!empty ($_POST['saveplateau'])){
    $save=new save($id_partie);
    $save->saveplateau($_POST['id'],$id_partie,$_POST['name'],$_POST['contenu'],$id_player);
}else if(!empty ($_POST['changerplateau'])){
    $save=new save($id_partie);
    $retour=$save->changerplateau($_POST['id'],$id_partie);
    echo $retour;
}else if(!empty ($_POST['nouveauplateau'])){
    $save=new save($id_partie);
    $retour=$save->nouveauplateau($id_partie);
    echo $retour;
}else if(!empty ($_POST['suprrplateau'])){
    $save=new save($id_partie);
    $save->suprrplateau($_POST['id']);
}else if(!empty ($_POST['listeaffichage'])){
    $save=new save($id_partie);
    $retour=$save->listeaffichage($id_partie);
    require('../views/viewmodulelistejoueur.php');
}else if(!empty ($_POST['controleplateau'])){
    $save=new save($id_partie);
    $retour=$save->controleplateau($_POST['id'],$id_player);
    echo $retour['contenu'];
}