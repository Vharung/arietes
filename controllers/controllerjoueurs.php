<?php 
class controllerjoueurs{
	public function __construct($url){
		/*if(isset($url) && count($url)>1)
			throw new Exception("Page introuvable");
		else*/ 
			$this->listejoueurs();	
	}
	public function listejoueurs(){
		if(!isset($_SESSION['login_user'])){
			$erreur= "Session expirée";
			$this->_view=new view();
			$this->_view->generate('home',$erreur,null);//view,erreur,données
		}else {
			$erreur=null;
			if(isset($_GET['action'])){
				$action=$_GET['action'];
				$idpartie=$_GET['idpartie'];
				$iduser=$_GET['iduser'];
				if($action=="suppression"){
					$user= new user('','','','');
					$user->retirerpartie($iduser,$idpartie);
					$erreur='Le joueur a été retiré de la partie dans la partie';
				}else if($action=="spetacteur"){
					$user= new user('','','','');
					$user->spetacteurpartie($iduser,$idpartie);
				}else if($action=="visible"){
					$user= new user('','','','');
					$user->visiblepartie($iduser,$idpartie);
				}
			} else if(isset($_GET['ajoutjoueur'])){
				$idpartie=$_GET['idpartie'];
				$nomuser=$_GET['ajoutjoueur'];
				$user= new user('','','','');
				$iduser=$user->iduser($nomuser);
				if(isset($iduser)){
					$checkuser=$user->ajoutpartie($iduser,$idpartie);
					if($checkuser==1){
						$erreur='Le joueur <b>'.$nomuser.'</b> est déjà dans la partie';
					}else {
						$erreur='Le joueur <b>'.$nomuser.'</b> a été ajouté à la partie';
					}
				}else {
					$erreur="speudo inexistant";
				}

			}
			if(isset($_GET['idpartie'])){
				$joueur=new joueur('','');
				$joueurs=$joueur->idjoueurtable($_GET['idpartie']);
				$name_mj=$_GET['idmj'];
				$listejoueur="";
				$idpartie=$_GET['idpartie'];
				foreach ($joueurs as $value){
				 	$nomjoueur=($joueur->nomjoueurtable($value));
				 	$visjoueur=($joueur->visjoueurtable($idpartie,$value));
				 	$spejoueur=($joueur->spejoueurtable($idpartie,$value));
				 	require('views/viewlistejoueur.php');
				}
				require_once('views/viewgestionjoueur.php');
				$data=$_SESSION['listepartie'].$gestionjoueur;
				$this->_view=new view();
				$this->_view->generate('liste',$erreur,$data);//view,erreur,données
			}else {
				$this->_view=new view();
				$this->_view->generate('liste',null,$_SESSION['listepartie']);//view,erreur,données
			}	
		}
	}
}