<?php 
class controllerpartie{    
    private $_modulepointdestin=array('enable' => FALSE, 'data' => array('value' => null, 'nbpoint' => null));
    private $_modulelanceur=array('enable' => FALSE, 'data' => array('default_des' => null,'valeurreussite' => null,'valeurcritique' => null,'valeurdesmax' => null,'valeurbonusoffensif' => null,'valeurmalusoffensif' => null,'iduser'=>null,'couleurprincipale'=>null,'couleursecondaire'=>null));
    private $_moduleposture=array('enable' => FALSE,'data' => array('iduser'=>null));
    private $_moduletchat=array('enable' => FALSE,'data' => array('listejoueurvisible'=>null,'content'=>null,'iduser'=>null));
    private $_moduleregle=array('enable' => FALSE,'data' => array('regle'=>null,'titreregle'=>null));
    private $_moduledoc=array('enable' => FALSE,'data' => array('idcreateur'=>null,'titredoc'=>null,'contenudoc'=>null,'contenumjdoc'=>null,'visible'=>null));
    private $_modulenotemj=array('enable' => FALSE,'data' => array('namenotemj'=>null,'infonotemj'=>null,'listejoueur'=>null,'mjjoueurnom'=>null,'mjjoueuretatjoueur'=>null,'mjjoueurtouretatjoueur'=>null,'mjturnorder'=>null));
    private $_modulepnj=array('enable' => FALSE,'data' => array('nompnj'=>null,'image'=>null,'stat'=>null,'histoire'=>null,'notemj'=>null,'visible'=>null));
    private $_modulemusic=array('enable' => FALSE,'data' => array('idmusic'=>null,'iduser'=>null,'nommusique'=>null,'url'=>null,'scr'=>null,'playlist'=>null,'dossier'=>null,'sousdossier'=>null,'lecture'=>null,'visible'=>null));
    private $_moduleavatar=array('enable' => FALSE, 'data' => array('id'=>null,'nom' => null,'image'=>null,'couleurprincipale'=>null,'bouclier'=>null,'boucliersec'=>null,'posture'=>null,'avantage'=>null,'etat'=>null,'barcolor'=>null,'barfinal'=>null));
    private $_modulecontentfiche=array('enable' => FALSE, 'data' =>array('fiche'=>null));
    private $_modulemenufiche=array('enable' => FALSE,'data' => array('menufiche'=>null));
    private $_moduleinfoplateau=array('enable' => TRUE,'data' => array('contenu'=>null,'img'=>null,'nom'=>null,'id'=>null,'joueur'=>null));
    private $_moduleplateau=array('contenu'=>'','img'=>'','result'=>'','id'=>'');


    public function __construct($url){
            $this->affichepartie();   
    }

    public function affichepartie(){
        if(!isset($_SESSION['login_user'])){
            $erreur= "Session expirée";
            $this->_view=new view();
            $this->_view->generate('home',$erreur,null);
        }else {
            $iduser=$_SESSION['iduser'];
            $activernote=false;

            $moduleediteurdefiche="";
            if(isset($_GET['idtemplate'])){
                 $_SESSION['template']=$_GET['idtemplate'];
            }
            if(isset($_GET['idpartie'])){
                 $_SESSION['idpartie']=$_GET['idpartie'];
            }
            if(isset($_GET['idmj'])){
                 $_SESSION['idmj']=$_GET['idmj'];
            }          
            $partie=new partie($iduser);
            $listemodule=$partie->listemodule($_SESSION['template']);
            $bouclierjoueur=$partie->bouclierjoueur($_SESSION['idpartie']);
            if($bouclierjoueur==1){$bouclierdisplay='block';}
            $boucliersecjoueur=$partie->boucliersecjoueur($_SESSION['idpartie']);
            if($boucliersecjoueur==1){$boucliersecdisplay='block';}
            $bars=$partie->barjoueur($_SESSION['idpartie']);
            foreach ($listemodule as $value) {

//--------------------------------------------------------------------------- module du compteur de point -----------------------------------------------------------------------------------\\
                if($value['type']==1){
                    $nbpoint=$partie->pointequipe($_SESSION['idpartie']);
                    $this->_modulepointdestin=array('enable' => TRUE, 'data' => array('value' => $value, 'nbpoint' => $nbpoint));
                }

//----------------------------------------------------------------------------- module du lanceur de dés -------------------------------------------------------------------------------------\\

                else if($value['type']==2){
                    $des=(explode("|",$value['valeur']));
                    $couleur=$partie->couleurjoueur($_SESSION['idpartie'],$_SESSION['iduser']);
                    if(isset($couleur[0])){
                        $couleurprincipale=$couleur[0];
                    }else {
                        $couleurprincipale='#4A90E2';
                    }
                    if(isset($couleur[1])){
                        $couleursecondaire=$couleur[1];
                    }else {
                        $couleursecondaire='#ffffff';
                    }
                    $this->_modulelanceur=array('enable' => TRUE, 'data' => array('default_des' => $des[0],'valeurreussite' => $des[1],'valeurcritique' => $des[2],'valeurdesmax' => $des[3],'valeurbonusoffensif' => $des[4],'valeurmalusoffensif' => $des[5],'iduser'=>$iduser,'couleurprincipale'=>$couleurprincipale,'couleursecondaire'=>$couleursecondaire));
                }

//------------------------------------------------------------------------------- module des postures ---------------------------------------------------------------------------------------\\

                else if($value['type']==3){
                    $this->_moduleposture=array('enable' => TRUE,'data' => array('iduser'=>$iduser));
                }

//-------------------------------------------------------------------------------- module des avatars ----------------------------------------------------------------------------------------\\

                else if($value['type']==4){
                    $id=[];$nomavatar=[];$image=[];$couleurprinc=[];$bouclier=[];$boucliersec=[];$posture=[];$avantage=[];$etat=[];$bars=[];$barfinals=[];//bug
                    $collone=1;
                    $listejoueurvisible=$partie->joueurvisible($_SESSION['idpartie']);
                    foreach ($listejoueurvisible as $key) {
                        $avatarjoueur=$partie->avatarjoueur($_SESSION['idpartie'],$key);
                        if(isset($avatarjoueur['nom'])){
                            $nomav=$avatarjoueur['nom'];
                        }else {
                            $nom= new user('','','','');
                            $nomav=$nom->nomuser($key);
                        }
                        $bouclierjoueur=$partie->bouclierjoueur($_SESSION['template']);
                        if($bouclierjoueur==1){
                            if(isset($avatarjoueur['bouclier'])){
                               $bouclier[]=$avatarjoueur['bouclier'];
                            }else {
                                $bouclier[]=0;
                            }
                            
                        }else{
                            $bouclier[]=null;
                        }
                        $boucliersecjoueur=$partie->boucliersecjoueur($_SESSION['template']);
                        if($boucliersecjoueur==1){
                            if(isset($avatarjoueur['boucliersec'])){
                               $boucliersec[]=$avatarjoueur['boucliersec'];
                            }else {
                                $boucliersec[]=0;
                            }
                        }else{
                            $boucliersec[]=null;
                        }
                        $id[]=$key;
                        $nomavatar[]=$nomav;
                        $image[]=$avatarjoueur['image'];
                        $couleurprinc[]=$avatarjoueur['couleurprincipale'];
                        $posture[]=$avatarjoueur['posture'];
                        $avantage[]=$avatarjoueur['avantage'];
                        $etat[]=$avatarjoueur['etat'];
                        //modif des bars 
                        $bars=$partie->barjoueur($_SESSION['template']);
                        foreach ($bars as $nombar=>$couleurbar) {
                            $barjoueur=$partie->barsjoueur($_SESSION['idpartie'],$key,$nombar);
                            $barfinal[$nombar]=$barjoueur;              
                        }
                        $barfinals[]=$barfinal;
                    }
                    $this->_moduleavatar=array('enable' => TRUE, 'data' => array('id'=>$id,'nom' => $nomavatar,'image'=>$image,'couleurprincipale'=>$couleurprinc,'bouclier'=>$bouclier,'boucliersec'=>$boucliersec,'posture'=>$posture,'avantage'=>$avantage,'etat'=>$etat,'barcolor'=>$bars,'barfinal'=>$barfinals));
                }

//---------------------------------------------------------------------------------- module du tchat ------------------------------------------------------------------------------------------\\

                else if($value['type']==8){
                    $tchat=new tchat('');
                    $listejoueurvisible=$partie->joueurvisible($_SESSION['idpartie']);
                    $content=$tchat->affichetchat($_GET['idpartie'],$iduser);
                    $this->_moduletchat=array('enable' => TRUE,'data' => array('listejoueurvisible'=>$listejoueurvisible,'content'=>$content,'iduser'=>$iduser));
                }

//-------------------------------------------------------------------------------- module des note du mj ---------------------------------------------------------------------------------------\\

                else if($value['type']==9 && $iduser==$_SESSION['idmj'] ){
                    $namenotemj=[];$infonotemj=[];$listejoueurvisible=[];$mjjoueurnom=[];$mjjoueuretatjoueur=[];$mjjoueurtouretatjoueur=[];$mjjoueurid=[];$mjturnorder=[];//bug
                    $nomnotemj=(explode("|",$value['valeur']));
                    $contenunotemj=$partie->fichejoueur($_SESSION['idpartie'],$iduser,9);
                    for($i=0;$i<count($nomnotemj);$i++){
                        $namenotemj[]=$nomnotemj[$i];
                        $infonotemj[]=$contenunotemj[$i];
                    }
                    $avatarjoueur=$partie->avatarjoueur($_SESSION['idpartie'],$_SESSION['idmj']);
                    if(isset($avatarjoueur['couleurprincipale'])){
                        $couleurprincipale=$avatarjoueur['couleurprincipale'];
                    }else {
                        $couleurprincipale='#fff';
                    }
                    if(isset($avatarjoueur['couleursecondaire'])){
                        $couleursecondaire=$avatarjoueur['couleursecondaire'];
                    }else {
                        $couleursecondaire='#000';
                    }
                    $listejoueurvisible=$partie->joueurvisible($_SESSION['idpartie']);
                    $etatdesjoueurs='';
                    foreach ($listejoueurvisible as $value) {
                        $avatarjoueur=$partie->avatarjoueur($_SESSION['idpartie'],$value);
                        if(isset($avatarjoueur['nom'])){
                            $nom=$avatarjoueur['nom'];
                            if(isset($avatarjoueur['turnorder'])){
                                $turnorder=$avatarjoueur['turnorder'];
                            }else{
                                 $turnorder='0';
                            }
                        }else {
                            $nom= new user('','','','');
                            $nom=$nom->nomuser($value);
                            $turnorder='0';
                        }
                        $etatjoueurmj=$partie->etatmjjoueur($_SESSION['idpartie'],$value);
                        if(isset($etatjoueurmj[0])){
                            $etatjoueur=$etatjoueurmj[0];
                        }else {
                            $etatjoueur='';
                        }
                        if(isset($etatjoueurmj[1])){
                            $touretatjoueur=$etatjoueurmj[1];
                        }else {
                            $touretatjoueur='';
                        }
                        $mjjoueurnom[]=$nom;
                        $mjjoueuretatjoueur[]=$etatjoueur;
                        $mjjoueurtouretatjoueur[]=$touretatjoueur;
                        $mjjoueurid[]=$value;
                        $mjturnorder[]=$turnorder;
                        
                    }
                    $this->_modulenotemj=array('enable' => TRUE,'data' => array('namenotemj'=>$namenotemj,'infonotemj'=>$infonotemj,'listejoueur'=>$listejoueurvisible,'mjjoueurnom'=>$mjjoueurnom,'mjjoueuretatjoueur'=>$mjjoueuretatjoueur,'mjjoueurtouretatjoueur'=>$mjjoueurtouretatjoueur,'mjjoueurid'=>$mjjoueurid,'mjturnorder'=>$mjturnorder));

                }

//---------------------------------------------------------------------------------- module des fiches ------------------------------------------------------------------------------------------\\

                else if($value['type']==10){
                    $listeuser=[];$menufiche=[];//bug
                    $listejoueurvisible=$partie->joueurvisible($_SESSION['idpartie']);
                    foreach ($listejoueurvisible as $value) {
                        $joueurmenu=array();
                        if($iduser==$value || $iduser==$_SESSION['idmj']){
                            $avatarjoueur=$partie->avatarjoueur($_SESSION['idpartie'],$value);
                            if(isset($avatarjoueur['nom'])){
                                $nom=$avatarjoueur['nom'];
                            }else {
                                $nom= new user('','','','');
                                $nom=$nom->nomuser($value);
                            }
                            $couleur=$partie->couleurjoueur($_SESSION['idpartie'],$value);
                            $joueurmenu['id']=$value;
                            $joueurmenu['nom']= $nom;
                            $joueurmenu['couleurprincipale']=$couleur['color1'];
                            $joueurmenu['couleursecondaire']=$couleur['color2'];
                            $menufiche[]=$joueurmenu;
                        }
                    }
                    $this->_modulemenufiche=array('enable' => TRUE,'data' => array('menufiche'=>$menufiche));
                    require('controllers/controllermodulefiche.php');
                    $this->_modulecontentfiche=array('enable' => TRUE, 'data' =>array('fiche'=>$listeuser));
                }

//---------------------------------------------------------------------------------- module de régle --------------------------------------------------------------------------------------------\\

                else if($value['type']==50){
                    $this->_moduleregle=array('enable' => TRUE,'data' => array('regle'=>$value['valeur'],'titreregle'=>$value['name']));
                }

//----------------------------------------------------------------------------------- module de doc ----------------------------------------------------------------------------------------------\\
                
                else if($value['type']==51){
                    $listejoueurvisible=$partie->joueurvisible($_SESSION['idpartie']);
                    $doc=$partie->docpartie($_SESSION['idpartie']);
                    $this->_moduledoc=array('enable' => TRUE,'data' => array('idcreateur'=>$doc['idcreateur'],'titredoc'=>$doc['nomdoc'],'contenudoc'=>$doc['contenu'],'contenumjdoc'=>$doc['contenu_mj'],'visible'=>$doc['visible'],'listejoueur'=>$listejoueurvisible,'id'=>$doc['id']));
                }

//----------------------------------------------------------------------------------- module de pnj ----------------------------------------------------------------------------------------------\\

                else if($value['type']==52){
                    $pnj=$partie->pnjpartie($_SESSION['idpartie']);
                    $this->_modulepnj=array('enable' => TRUE,'data' => array('nompnj'=>$pnj['nompnj'],'image'=>$pnj['image'],'stat'=>$pnj['stat'],'histoire'=>$pnj['histoire'],'notemj'=>$pnj['notemj'],'visible'=>$pnj['visible'],'listejoueur'=>$listejoueurvisible,'id'=>$pnj['id']));
                }

//---------------------------------------------------------------------------------- module de musique ---------------------------------------------------------------------------------------------\\

                else if($value['type']==53){
                    $music=$partie->musicpartie($_SESSION['idmj'],$_SESSION['idpartie']);
                    $this->_modulemusic=array('enable' => TRUE,'data' => array('idmusic'=>$music['idmusic'],'iduser'=>$music['iduser'],'nommusique'=>$music['nommusique'],'url'=>$music['url'],'scr'=>$music['scr'],'dossier'=>$music['dossier'],'sousdossier'=>$music['sousdossier']/*,'playlist'=>$music['playlist'],'lecture'=>$music['lecture'],'visible'=>$music['visible']*/));                   
                }
            }


//-------------------------------------------------------------------------------- module plateau ----------------------------------------------------------------------------------------\\

            //_moduleinfoplateau
            $plateau=$partie->lectureplateau($_SESSION['idpartie']);
            $this->_moduleinfoplateau=array('enable' => TRUE,'data' => array('contenu'=>$plateau['contenu'],'img'=>$plateau['img'],'nom'=>$plateau['nom'],'id'=>$plateau['id'],'joueur'=>$plateau['joueur']));
            //private $_moduleinfoplateau=array('enable' => TRUE,'data' => array('nomscene'=>null,'joueursurscene'=>null,'scene'=>null));



//-------------------------------------------------------------------------------- export vers viewer -------------------------------------------------------------------------------------\\

            $data=array('modulepointdestin' => $this->_modulepointdestin,
                'modulelanceur' => $this->_modulelanceur,
                'moduleposture' => $this->_moduleposture,
                'moduleavatar' => $this->_moduleavatar,
                'moduletchat' => $this->_moduletchat,
                'modulenotemj' => $this->_modulenotemj,
                'modulemenufiche' => $this->_modulemenufiche,
                'modulecontentfiche' => $this->_modulecontentfiche,
                'moduleregle' => $this->_moduleregle,
                'modulepnj' => $this->_modulepnj,
                'moduledoc' => $this->_moduledoc,                
                'modulemusic' => $this->_modulemusic,
                'moduleinfoplateau' => $this->_moduleinfoplateau,
                'moduleediteurdefiche' => $moduleediteurdefiche);
            $this->_view=new view();
            $this->_view->generate('partie',null,$data);//view,erreur,données
        }   
    }
}
