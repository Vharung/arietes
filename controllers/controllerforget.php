<?php 
class controllerforget{
	public function __construct($url){
		/*if(isset($url) && count($url)>1)
			throw new Exception("Page introuvable");
		else*/ 
			$this->oublie();	
	}
	public function oublie(){
		if(isset($_POST['recup_submit'],$_POST['recup_mail'])){
	        if(!empty($_POST['recup_mail'])){
	            $recup_mail=htmlspecialchars($_POST['recup_mail']);
	            if(filter_var($recup_mail,FILTER_VALIDATE_EMAIL)){
	                $user = new user("","","","");
	                $id_session=($user->recupmail($recup_mail));
	                if($id_session==""){
	                    $erreur= "Mail Introuvable";
	                   	$this->_view=new view();
						$this->_view->generate('home',$erreur,null);//css,view,erreur
	                }else {
	                    $_SESSION['recup_mail']=$recup_mail;
	                    $recup_code ="";
	                    for ($i=0; $i <8 ; $i++) { 
	                        $recup_code.=mt_rand(0,9);
	                    }
	                    $_SESSION['recup_code'] =$recup_code;
	                    $mail = new mail($recup_mail,$recup_code);
	                    $mail->recupcode($recup_mail,$recup_code);
	                    $header="MIME-Version: 1.0\r\n";
	                    $header.='From:"Arietes.com"<support@arietes.com>'."\n";
	                    $header.='Content-Type:text/html; charset="utf-8"'."\n";
	                    $header.='Content-Transfer-Encoding: 8bit';
	                    $message = '
	                     <html>
	                     <head>
	                       <title>Mail de récupération</title>
	                       <meta charset="utf-8" />
	                     </head>
	                     <body>
	                       <font color="#303030";>
	                         <div align="left">
	                           <table width="600px">
	                             <tr>
	                               <td>
	                                 
	                                 <div align="left"><h1>Bonjour<b>'.$id_session.'</b>,</h1>
	                                 <p>Votre code de récupération est : <b>'.$recup_code.'</b><br>
	                                 A bientot.</p>
	                                 </div>
	                               </td>
	                             </tr>
	                             <tr>
	                               <td align="left">
	                                 <font size="2">
	                                   Ceci est un email automatique, merci de ne pas y répondre.
	                                 </font>
	                               </td>
	                             </tr>
	                           </table>
	                         </div>
	                       </font>
	                     </body>
	                     </html>
	                     ';
	                    //mail($recup_mail, 'Mail de récupération', $message, $header);
	                    $this->_view=new view();
						$this->_view->generate('recuperation',null,$recup_mail);
	                } 
	            }else {
	                $erreur="Mail invalide";
	               	$this->_view=new view();
					$this->_view->generate('home',$erreur,null);//css,view,erreur,donnée
	            }
	        }else {
	         	$erreur="Veuillez renseignez votre adresse mail.";
	         	$this->_view=new view();
				$this->_view->generate('home',$erreur,null);//css,view,erreur
	        }
	    }else {
			$this->_view=new view();
			$this->_view->generate('home',null,null);
		}
	}
}
