<?php 
class ControllerListe{
	public function __construct($url){ 
			$this->afficheliste();	//appel fonction
	}
	public function afficheliste(){
		if(!isset($_SESSION['login_user'])){
			$erreur= "Session expirée";
			$this->_view=new view();
			$this->_view->generate('home',$erreur,null);//view,erreur,données
		}else if(isset($_GET['action'])){
			$idpartie=$_GET['idpartie'];
			$iduser=$_GET['iduser'];
			$idmj=$_GET['idmj'];
			if($idmj==$iduser && $idmj==$_SESSION['iduser']){
				$liste= new liste('','','');
				$liste->supprimeliste($idpartie);
				header('Location: index.php?url=liste');
			}else if($iduser==$_SESSION['iduser']){
				$liste= new liste('','','');
				$liste->quitterliste($idpartie,$iduser);
				header('Location: index.php?url=liste');
			}else {
				$erreur='Vous n\'avez pas les droits';
				$this->_view=new view();
				$this->_view->generate('liste',$erreur,$_SESSION['listepartie']);
			}
		}else if(isset($_GET['videtchat'])){
			$idpartie=$_GET['idpartie'];
			$vide= new tchat($idpartie);
			$vide->videtchat($idpartie);
			$erreur='Tchat vidé';
			$this->_view=new view();
			$this->_view->generate('liste',$erreur,$_SESSION['listepartie']);
		}else if(isset($_GET['color1'])){
			$idpartie=$_GET['idliste'];
			$color1=$_GET['color1'];
			$color2=$_GET['color2'];
			$color= new liste('','','');
			$color->changecolor($idpartie,$color1,$color2);
			header('Location: index.php?url=liste');
		}else {
		    $iduser=$_SESSION['iduser']; 
			$liste = new liste('','','');
		    $liste_partie=$liste->tablerejoins($iduser);
		    $liste_complete="";
		  

		    foreach ($liste_partie['groupe'] as $cle => $valeur){
		    	$nomgroupe= $valeur['nom'];
		    	require('views/viewdebutliste.php');
		    			foreach ($valeur['partie'] as $clef => $vale) {
		    				$idpartie=$vale;
		    				$nom_partie=($liste->nomtable($vale));
						    $idtemplate=$nom_partie[1];
						    $name_template=($liste->nomtemplate($nom_partie[1]));
						    $name_mj=($liste->nommj($nom_partie[2]));
						    if(isset($nom_partie[4])){
						    	$color1=$nom_partie[4];
						    }else {
						    	$color1="#778899";
						    }
						    if(isset($nom_partie[5])){
						    	$color2=$nom_partie[5];
						    }else {
						    	$color2="#ffffff";
						    }
						    require('views/viewlistepartie.php');
			    		}
		    			
		    		
		    	require('views/viewendliste.php');	    		
		    }
		    foreach ($liste_partie['sansgroupe'] as $cle => $valeur){
		    		$idpartie=$valeur;
		    		$anciengroupe="";
		    		$nomgroupe="";
	    			$nom_partie=($liste->nomtable($valeur));
				    $idtemplate=$nom_partie[1];
				    $name_template=($liste->nomtemplate($nom_partie[1]));
				    $name_mj=($liste->nommj($nom_partie[2]));
				    if(isset($nom_partie[4])){
				    	$color1=$nom_partie[4];
				    }else {
				    	$color1="#778899";
				    }
				    if(isset($nom_partie[5])){
				    	$color2=$nom_partie[5];
				    }else {
				    	$color2="#ffffff";
				    }
				    require('views/viewlistepartie.php');
	    		
		    }

			$_SESSION['listepartie']=$liste_complete;
			//echo $liste_complete;
			$this->_view=new view();
			$this->_view->generate('liste',null,$_SESSION['listepartie']);//view,erreur,données
		}	
	}
}