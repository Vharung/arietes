<?php 
class controllergestiongroupe{
	public function __construct($url){
		/*if(isset($url) && count($url)>1)
			throw new Exception("Page introuvable");
		else*/ 
			$this->affichegroupe();	//appel fonction
	}
	public function affichegroupe(){

		if(!isset($_SESSION['login_user'])){
			$erreur= "Session expirée";
			$this->_view=new view();
			$this->_view->generate('home',$erreur,null);//view,erreur,données
		}else if(isset($_GET['action'])){
			if($_GET['action']=='suppression'){
				$liste = new liste('','','');
				$liste->supprimegroupe($_GET['idgroupe']);
			}else if($_GET['action']=='newgroupe'){
				$liste = new liste('','','');
				$liste->newgroupe($_GET['newgroupe'],$_SESSION['iduser']);
			}else if($_GET['action']=='modifier'){
				$liste = new liste('','','');
				$liste->modifgroupe($_GET['idgroupe'],$_GET['nom']);
			}else if($_GET['action']=='addgroupe'){
				$liste = new liste('','','');
				$liste->addgroupe($_GET['idgroupe'],$_GET['idpartie']);
			}else if($_GET['action']=='supgroupe'){
				$liste = new liste('','','');
				$liste->supgroupe($_GET['idpartie']);
			}else if($_GET['action']=='modinompartie'){
				$liste = new liste('','','');
				$liste->modinompartie($_GET['idpartie'],$_GET['nom']);
			}
		}
			$iduser=$_SESSION['iduser']; 
			$liste = new liste('','','');
		    $liste_partie=$liste->tablerejoins($iduser);
	    	$listegroupe="";
	    	$listesans='';
	    	foreach ($liste_partie['sansgroupe'] as $cle => $valeur){
		    		$idpartie=$valeur;
		    		$anciengroupe="";
		    		$nomgroupe="";
	    			$nom_partie=($liste->nomtable($valeur));
		    		require('views/viewlisteoptionderoulant.php');	    		
		    }

		    foreach ($liste_partie['groupe'] as $cle => $valeur){
		    	$nomgroupe= $valeur['nom'];
		    	$idgroupe= $liste_partie['idgroupe'][$cle];
		    	require('views/viewlistegroupe.php');
		    			foreach ($valeur['partie'] as $clef => $vale) {
		    				$idpartie=$vale;
		    				$nom_partie=($liste->nomtable($vale));
						    require('views/viewlistegroupepartie.php');
						}
		    		if ($listesans!= '') {
		    			 require('views/viewlistederoulantpartie.php');
		    		}
		    		
		    }
		    
		/*require_once('views/viewgestiongroupe.php');
		$data=$_SESSION['listepartie'].$gestiongroupe;
	    $this->_view=new view();
		$this->_view->generate('liste',null,$data);//view,erreur,donnée	   */
		
		//rafrachir liste partie
		$liste = new liste('','','');
		    $liste_partie=$liste->tablerejoins($iduser);
		    $liste_complete="";
		  

		    foreach ($liste_partie['groupe'] as $cle => $valeur){
		    	$nomgroupe= $valeur['nom'];
		    	require('views/viewdebutliste.php');
		    			foreach ($valeur['partie'] as $clef => $vale) {
		    				$idpartie=$vale;
		    				$nom_partie=($liste->nomtable($vale));
						    $idtemplate=$nom_partie[1];
						    $name_template=($liste->nomtemplate($nom_partie[1]));
						    $name_mj=($liste->nommj($nom_partie[2]));
						    if(isset($nom_partie[4])){
						    	$color1=$nom_partie[4];
						    }else {
						    	$color1="#778899";
						    }
						    if(isset($nom_partie[5])){
						    	$color2=$nom_partie[5];
						    }else {
						    	$color2="#ffffff";
						    }
						    require('views/viewlistepartie.php');
			    		}
		    			
		    		
		    	require('views/viewendliste.php');	    		
		    }
		    foreach ($liste_partie['sansgroupe'] as $cle => $valeur){
		    		$idpartie=$valeur;
		    		$anciengroupe="";
		    		$nomgroupe="";
	    			$nom_partie=($liste->nomtable($valeur));
				    $idtemplate=$nom_partie[1];
				    $name_template=($liste->nomtemplate($nom_partie[1]));
				    $name_mj=($liste->nommj($nom_partie[2]));
				    if(isset($nom_partie[4])){
				    	$color1=$nom_partie[4];
				    }else {
				    	$color1="#778899";
				    }
				    if(isset($nom_partie[5])){
				    	$color2=$nom_partie[5];
				    }else {
				    	$color2="#ffffff";
				    }
				    require('views/viewlistepartie.php');
	    		
		    }
		    require_once('views/viewgestiongroupe.php');
			$data=$liste_complete.$gestiongroupe;
			//echo $liste_complete;
			$this->_view=new view();
			$this->_view->generate('liste',null,$data);//view,erreur,données
	}
}