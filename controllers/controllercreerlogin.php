<?php 
class controllercreerlogin{
	public function __construct($url){
		/*if(isset($url) && count($url)>1)
			throw new Exception("Page introuvable");
		else*/
			$this->creerlogin();	
	}
	public function creerlogin(){
		/*if(!isset($_SESSION['login_user'])){
			$erreur= "Session expirée";
			$this->_view=new view();
			$this->_view->generate('home',$erreur,null);//view,erreur,données
		}else*/ if(isset($_POST['pseudo'],$_POST['mdp'])){
		    $pseudo=$_POST['pseudo'];
		    $user = new user("","","","");
		    if(empty($pseudo)){
		        $erreur="Le champ Pseudo est vide.";
		    } else if(!preg_match("#^[a-z0-9]+$#",$pseudo)){
		        $erreur= "Le Pseudo doit être renseigné en lettres minuscules sans accents, sans caractères spéciaux.";
		    } else if(strlen($pseudo)>25){
		        $erreur= "Le pseudo est trop long, il dépasse 25 caractères.";
		    } else if(empty($pseudo)){
		        $erreur= "Le champ Mot de passe est vide.";
		     } else if(empty($_POST['email'])){
		       	$erreur= "Le champ Email est vide.";
		    } else if($user->iduser($pseudo)!=''){
		        $erreur= "Ce pseudo est déjà utilisé.";
		    } else {
		        $user->newuser($pseudo,$_POST['mdp'],$_POST['email']);
		        $erreur ="Compte créé";
		    } 
		    $this->_view=new view();
			$this->_view->generate('home',$erreur,null);
		}else {
			$this->_view=new view();
			$this->_view->generate('home',null,null);
		}
	}
}