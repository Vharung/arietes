<?php
session_start();
class router{
	private $_ctrl;
	public function routeReq(){
		try{ //chargement automatique des classes
			spl_autoload_register(function($class){
				$class = strtolower($class);
				require_once('models/model'.$class.'.php');
			});
			$url='';
			//le controller est inclus selon action de l'utilisateur
			if(isset($_GET['url'])){
				$url= explode('/',filter_var($_GET['url'],FILTER_SANITIZE_URL));;
				$controller=$url[0];
				$controllerClass="controller".$controller;
				$controllerFile="controllers/".$controllerClass.".php";
				require_once('config/config.php');
				require_once('views/view.php');
                require_once('views/viewmod.php');
				if(file_exists($controllerFile)){
					require_once($controllerFile);
					$this->_ctrl=new $controllerClass($url);
				}else 
					throw new Exception('Page introuvable '.$controllerFile);	
			}else {
				require_once('views/view.php');
                require_once('views/viewmod.php');
				require_once('controllers/controllerhome.php');
				$this->_ctrl= new ControllerHome($url);
				require_once ('views/viewfooter.php');
			}
		}
		//Gestion des erreurs
		catch(Exception $e){
			$errorMsg =$e->getMessage();
			require_once('views/viewerror.php');
		}
	}
}