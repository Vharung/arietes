<?php
//private $_modulepointdestin=array('enable' => FALSE, 'data' => array('value' => null, 'nbpoint' => null));
$listeuser=array();


$listejoueurvisible=$partie->joueurvisible($_SESSION['idpartie']);
foreach ($listejoueurvisible as $value) {
    $collone10=array();
    $collone20=array();
    $collone30=array();
    $collone40=array();
    foreach ($listemodule as $key) {

//-------------------------------------------------------------------------------- module des stat ------------------------------------------------------------------------------------------\\

        
        if($key['type']==11){
            $modulestat= array();
            $stat=explode("|",$key['valeur']);
            $statjoueur=$partie->fichejoueur($_SESSION['idpartie'],$value,$key['id']);
            $result = count($stat);
            $modulestat['type']='stat';
            $modulestat['name']=$key['name'];
            $modulestat['id']=$statjoueur['id'];
            $modulestat['idblock']=$key['id'];
            for($i=0;$i<$result;$i++){
                if(isset($statjoueur[$i])){
                    $valeurstat=$statjoueur[$i];
                }else {
                    $valeurstat=0;
                }
                $modulestat[$stat[$i]]=$valeurstat;
            }
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$modulestat;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$modulestat;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$modulestat;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$modulestat;
            }
        }

//-------------------------------------------------------------------------------- module de texte ------------------------------------------------------------------------------------------\\

        if($key['type']==12){
            $histjoueur=$partie->fichejoueur($_SESSION['idpartie'],$value,$key['id']);
            $moduletext['type']='text';
            $moduletext['name']=$key['name'];
            $moduletext['id']=$histjoueur['id'];
            $moduletext['idblock']=$key['id'];
            if(isset($moduletext)){
                $moduletext['text']=$histjoueur[0];
            }else {
                $moduletext['text']='';
            }            
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$moduletext;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$moduletext;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$moduletext;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$moduletext;
            }
            
        }

//------------------------------------------------------------------------------ module de competence ----------------------------------------------------------------------------------------\\

        if($key['type']==13){
            $compjoueur=$partie->listefichejoueur($_SESSION['idpartie'],$value,$key['id']);
            $modulecompetence['type']='competence';
            $modulecompetence['name']=$key['name'];
            $modulecompetence['id']=$compjoueur['id'];
            $modulecompetence['idblock']=$key['id'];
            $modulecompetence['liste']=$compjoueur;
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$modulecompetence;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$modulecompetence;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$modulecompetence;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$modulecompetence;
            }
        }

//------------------------------------------------------------------------------ module de inventaire ---------------------------------------------------------------------------------------\\

        if($key['type']==14){
            $invjoueur=$partie->listefichejoueur($_SESSION['idpartie'],$value,$key['id']);
            $moduleinventaire['type']='inventaire';
            $moduleinventaire['name']=$key['name'];
            $moduleinventaire['id']=$invjoueur['id'];
            $moduleinventaire['idblock']=$key['id'];
            $moduleinventaire['liste']=$invjoueur;
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$moduleinventaire;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$moduleinventaire;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$moduleinventaire;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$moduleinventaire;
            }
        }

//----------------------------------------------------------------------------- module d' encombrement ---------------------------------------------------------------------------------------\\

        if($key['type']==15){
            $encjoueur=$partie->fichejoueur($_SESSION['idpartie'],$value,$key['id']);
            $moduleencombrer['type']='encombrement';
            $moduleencombrer['name']=$key['name'];
            $moduleencombrer['id']=$encjoueur['id'];
            $moduleencombrer['idblock']=$key['id'];
            if(isset($encjoueur[0])){
                $moduleencombrer['min']=$encjoueur[0];
            }else {
                $moduleencombrer['min']=0;
            }
            if(isset($encjoueur[1])){
                $moduleencombrer['max']=$encjoueur[1];
            }else {
                $moduleencombrer['max']=0;
            }
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$moduleencombrer;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$moduleencombrer;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$moduleencombrer;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$moduleencombrer;
            }
        }

//----------------------------------------------------------------------------- module de connaissance ---------------------------------------------------------------------------------------\\

        if($key['type']==16){
            $conjoueur=$partie->listefichejoueur($_SESSION['idpartie'],$value,$key['id']);
            $moduleconnaissance['type']='connaissance';
            $moduleconnaissance['name']=$key['name'];
            $moduleconnaissance['id']=$conjoueur['id'];
            $moduleconnaissance['idblock']=$key['id'];
            $moduleconnaissance['liste']=$conjoueur;
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$moduleconnaissance;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$moduleconnaissance;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$moduleconnaissance;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$moduleconnaissance;
            }
        }

//----------------------------------------------------------------------------- module de posture ---------------------------------------------------------------------------------------\\

       if($key['type']==17){
            $avatarjoueur=$partie->avatarjoueur($_SESSION['idpartie'],$value,$key['id']);
            $moduleposture['type']='posture';
            $moduleposture['name']=$key['name'];
            $moduleposture['id']=$avatarjoueur['id'];
            $moduleposture['valeur']=$avatarjoueur['posture'];
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$moduleposture;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$moduleposture;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$moduleposture;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$moduleposture;
            }
        }

//---------------------------------------------------------------------------- module de avantage ---------------------------------------------------------------------------------------\\

       if($key['type']==18){
            $avatarjoueur=$partie->avatarjoueur($_SESSION['idpartie'],$value,$key['id']);
            $moduleavantage['type']='avantage';
            $moduleavantage['name']=$key['name'];
            $moduleavantage['id']=$avatarjoueur['id'];
            $moduleavantage['valeur']=$avatarjoueur['avantage'];
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$moduleavantage;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$moduleavantage;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$moduleavantage;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$moduleavantage;
            }
        }

//------------------------------------------------------------------------------ module de etat  -----------------------------------------------------------------------------------------\\

       if($key['type']==19){
            $avatarjoueur=$partie->avatarjoueur($_SESSION['idpartie'],$value,$key['id']);
            $moduleetat['type']='etat';
            $moduleetat['name']=$key['name'];
            $moduleetat['valeur']=$avatarjoueur['avantage'];
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$moduleetat;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$moduleetat;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$moduleetat;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$moduleetat;
            }
        }

//------------------------------------------------------------------------------ module de editeur  -----------------------------------------------------------------------------------------\\

       if($key['type']==20){
            $avatarjoueur=$partie->avatarjoueur($_SESSION['idpartie'],$value,$key['id']);
            $moduleediteur['type']='editeur';
            $moduleediteur['id']=$avatarjoueur['id'];
            if(isset($avatarjoueur['couleurprincipale'])){
                $moduleediteur['color1']=$avatarjoueur['couleurprincipale'];
            }else {
                $moduleediteur['color1']="#4A90E2";
            }
            if(isset($avatarjoueur['couleursecondaire'])){
                $moduleediteur['color2']=$avatarjoueur['couleursecondaire'];
            }else {
                $moduleediteur['color2']="#ffffff";
            }
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$moduleediteur;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$moduleediteur;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$moduleediteur;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$moduleediteur;
            }
        }

//------------------------------------------------------------------------------ module de stat_sec  -----------------------------------------------------------------------------------------\\

       if($key['type']==21){
            $modulestat= array();
            $stat=explode("|",$key['valeur']);
            $statjoueur=$partie->fichejoueur($_SESSION['idpartie'],$value,$key['id']);
            $result = count($stat);
            $modulestat['type']='stat_sec';
            $modulestat['name']=$key['name'];
            $modulestat['id']=$statjoueur['id'];
            $modulestat['idblock']=$key['id'];
            for($i=0;$i<$result;$i++){
                if(isset($statjoueur[$i])){
                    $valeurstat=$statjoueur[$i];
                }else {
                    $valeurstat=0;
                }
                $modulestat[$stat[$i]]=$valeurstat;
            }
            if($key['block']>=10 && $key['block']<20){
                $collone10[]=$modulestat;
            }else if($key['block']>=20 && $key['block']<30){
                $collone20[]=$modulestat;
            }else if($key['block']>=30 && $key['block']<40){
                $collone30[]=$modulestat;
            }else if($key['block']>=40 && $key['block']<50){
                $collone40[]=$modulestat;
            }
        }

//--------------------------------------------------------------------------------- fin des modules -------------------------------------------------------------------------------------------\\

    }

    $listeuser[$value]= array('colone1' => $collone10,'colone2' => $collone20,'colone3' => $collone30,'colone4' => $collone40);
    
}
