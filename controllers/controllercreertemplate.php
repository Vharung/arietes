<?php
class controllercreertemplate{
	private $_modulepointdestin=array('enable' => FALSE, 'value' => null, 'nbpoint' => null);
	private $_modulelanceur=array('enable' => FALSE, 'default_des' => null,'valeurreussite' => null,'valeurcritique' => null,'valeurdesmax' => null,'valeurbonusoffensif' => null,'valeurmalusoffensif' => null);
	private $_moduleavatar=array('enable' => FALSE);
	private $_modulebouclier=array('enable' => FALSE);
	private $_moduleboucliersec=array('enable' => FALSE);
	private $_moduletchat=array('enable' => FALSE);
	private $_modulenotemj=array('enable' => FALSE,'nomnotemj'=>null);
	private $_moduleregle=array('enable' => FALSE,'regle'=>null);
	private $_moduledoc=array('enable' => FALSE);
    private $_modulepnj=array('enable' => FALSE);
    private $_modulemusic=array('enable' => FALSE);
    private $_modulemenufiche=array('enable' => FALSE,'encombrement'=>null,'posture'=>null,'avantage'=>null,'etat'=>null,'module'=>null,'stat'=>null,'stat_sec'=>null);


	public function __construct($url){
		/*if(isset($url) && count($url)>1)
			throw new Exception("Page introuvable");
		else*/ 
			$this->affichetemplate();	
	}
	public function affichetemplate(){
        if(!isset($_SESSION['login_user'])){
            $erreur= "Session expirée";
            $this->_view=new view();
            $this->_view->generate('home',$erreur,null);//view,erreur,données
        }else {
            $iduser=$_SESSION['iduser'];           		
    		$idtemplate=0;
    		$listemodule=null;
    		$barfinals=array();
    		if(isset($_GET['template']) && $_GET['template']>0){
    			$idtemplate=$_GET['template'];
    			$partie=new partie($iduser);
                $listemodule=$partie->listemodule($idtemplate);
                foreach ($listemodule as $value) {

    //--------------------------------------------------------------------------- module du compteur de point -----------------------------------------------------------------------------------\\
               
    	            if($value['type']==1){
    	                $nbpoint=$partie->pointequipe($_GET['template']);
    	                $this->_modulepointdestin=array('enable' => TRUE, 'value' => $value, 'nbpoint' => $nbpoint);
    	            }

    //----------------------------------------------------------------------------- module du lanceur de dés -------------------------------------------------------------------------------------\\

                    else if($value['type']==2){
                        $des=(explode("|",$value['valeur']));
                        $this->_modulelanceur=array('enable' => TRUE, 'default_des' => $des[0],'valeurreussite' => $des[1],'valeurcritique' => $des[2],'valeurdesmax' => $des[3],'valeurbonusoffensif' => $des[4],'valeurmalusoffensif' => $des[5]);
                    }

    //-------------------------------------------------------------------------------- module des avatars ----------------------------------------------------------------------------------------\\

                    else if($value['type']==4 ){
                        //$value['type']==7
                        $this->_moduleavatar=array('enable' => TRUE);
                    }else if($value['type']==5){
                    	 $this->_modulebouclier=array('enable' => TRUE);
                    }else if($value['type']==6){
                    	 $this->_moduleboucliersec=array('enable' => TRUE);
                    }else if($value['type']==7){
                        $barfinal['name']=$value['name'];  
                        $barfinal['color']=$value['valeur']; 
                        $barfinals[]=$barfinal;                  
                    }

    //---------------------------------------------------------------------------------- module du tchat ------------------------------------------------------------------------------------------\\

                    else if($value['type']==8){
                        $this->_moduletchat=array('enable' => TRUE);
                    }

    //-------------------------------------------------------------------------------- module des note du mj ---------------------------------------------------------------------------------------\\

    				else if($value['type']==9){
                        $nomnotemj=(explode("|",$value['valeur']));
                        $this->_modulenotemj=array('enable' => TRUE,'nomnotemj'=>$nomnotemj);
                    }

    //---------------------------------------------------------------------------------- module des fiches ------------------------------------------------------------------------------------------\\
    		/*[12] => Array
            (
                [id] => 15
                [name] => Statistique
                [type] => 11
                [block] => 11
                [valeur] => Phy|Soc|Men
            )*/
                    


                    else if($value['type']==10){
                        $encombrement=null;
                        $posture=null;
                        $avantage=null;
                        $etat=null;
                        $module=null;
                        $stat=null;
                        $stat_sec=null;
    	            	foreach ($listemodule as $key) {
    	            		if($key['type']==11){
                                $stat=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block'],'type'=>'stat','nombre'=>$key['valeur']);//stat
                            }if($key['type']==21){
                                $stat_sec=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block'],'type'=>'stat_sec','nombre'=>$key['valeur']);//stat
                            }else if($key['type']==12){
    	            			$module[]=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block'],'type'=>'text');//histoire
    	            		}else if($key['type']==13){
    	            			$module[]=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block'],'type'=>'comp');//competence
    	            		}else if($key['type']==14){
    	            			$module[]=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block'],'type'=>'inve');//inventaire
    	            		}else if($key['type']==15){
    	            			$encombrement=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block']);//encombrement
    	            		}else if($key['type']==16){
    	            			$module[]=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block'],'type'=>'conn');//connaissance
    	            		}else if($key['type']==17){
    	            			$posture=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block']);//posture
    	            		}else if($key['type']==18){
    	            			$avantage=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block']);//avantage
    	            		}else if($key['type']==19){
    	            			$etat=array('id'=>$key['id'],'name'=>$key['name'],'block'=>$key['block']);//etat
    	            		}
    	            	}
    	            	$this->_modulemenufiche=array('enable' => TRUE,'encombrement'=>$encombrement,'posture'=>$posture,'avantage'=>$avantage,'etat'=>$etat,'module'=>$module,'stat'=>$stat,'stat_sec'=>$stat_sec);
                    }

    //---------------------------------------------------------------------------------- module de régle --------------------------------------------------------------------------------------------\\

                    else if($value['type']==50){
                        $this->_moduleregle=array('enable' => TRUE,'regle'=>$value['valeur']);
                    }

    //----------------------------------------------------------------------------------- module de doc ----------------------------------------------------------------------------------------------\\
                    
                    else if($value['type']==51){
                        $this->_moduledoc=array('enable' => TRUE);
                    }

    //----------------------------------------------------------------------------------- module de pnj ----------------------------------------------------------------------------------------------\\

                    else if($value['type']==52){
                        $this->_modulepnj=array('enable' => TRUE);
                    }

    //---------------------------------------------------------------------------------- module de musique ---------------------------------------------------------------------------------------------\\

                    else if($value['type']==53){
                        $this->_modulemusic=array('enable' => TRUE);                   
                    }
    	        }          
    		}		
    		$liste=new liste('','','');
    		$listetemplate=$liste->listetemplateperso($iduser);
    		$data=array('modulepointdestin' => $this->_modulepointdestin,
                    'listetemplate' => $listetemplate,
                    'idtemplate' => $idtemplate,
                    'listemodule' => $listemodule,
                    'modulelanceur' => $this->_modulelanceur,
                    'moduleavatar' => $this->_moduleavatar,
                    'modulebouclier' => $this->_modulebouclier,
                    'moduleboucliersec' => $this->_moduleboucliersec,
                    'bar'=>$barfinals,
                	'moduletchat' => $this->_moduletchat,
                	'modulemenufiche' =>$this->_modulemenufiche,
                	'modulenotemj'=>$this->_modulenotemj,
                	'moduleregle'=>$this->_moduleregle,
                	'modulepnj' => $this->_modulepnj,
                    'moduledoc' => $this->_moduledoc,                
                    'modulemusic' => $this->_modulemusic);
    		$this->_view=new view();
    		$this->_view->generate('template',null,$data);
        }
	}
}