<?php
class controllercreerpartie{
	public function __construct($url){
		/*if(isset($url) && count($url)>1)
			throw new Exception("Page introuvable");
		else*/ 
			$this->affichecreer();	
	}
	public function affichecreer(){
		if(!isset($_SESSION['login_user'])){
			$erreur= "Session expirée";
			$this->_view=new view();
			$this->_view->generate('home',$erreur,null);//view,erreur,données
		}else if(isset($_POST['mjpartie'])){
			$iduser=$_POST['mjpartie'];
			$nomtable=$_POST['nompartie'];
			$idtemplate= $_POST['template'];
			$liste = new liste('','','');
		    $liste_partie=$liste->nouvelletable($nomtable,$iduser,$idtemplate); 
		    $liste_complete="";
		    require_once('controllerliste.php');
		    $liste=new ControllerListe('liste');
		}else {
			$listetemplate=new liste('','','');
			$listetemplate=$listetemplate->listetemplate();
			$listetemplate_complete='<select style="color:#000;margin-top:5px" name="template">';
			foreach($listetemplate as $x => $x_value) {
			    $id_template=$x;
			    $name_template=$x_value;
			    require('views/viewlistetemplate.php');
			}
			$listetemplate_complete.='</select>';
			$iduser=$_SESSION['iduser'];
			require_once('views/viewcreerpartie.php');
		    $data=$_SESSION['listepartie'].$listetemplate_complete;
		    $this->_view=new view();
			$this->_view->generate('liste',null,$data);
		}
	}
}