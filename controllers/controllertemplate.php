<?php 
class ControllerTemplate{
	public function __construct($url){
		/*if(isset($url) && count($url)>1)
			throw new Exception("Page introuvable");
		else*/ 
			$this->listeTemplate();	
	}
	public function listeTemplate(){
		if(!isset($_SESSION['login_user'])){
			$erreur= "Session expirée";
			$this->_view=new View();
			$this->_view->generate('home',$erreur,null);//view,erreur,données
		}else if(isset($_GET['action'])){
			$id_template=$_GET['template'];
			$idpartie=$_GET['idpartie'];
			$liste=new liste('','','');
			$nom_partie=($liste->changetemplate($idpartie,$id_template));
			header('Location: index.php?url=liste');
		}else if(isset($_GET['idpartie'])){
			$name_template=$_GET['name_template'];
			$idpartie=$_GET['idpartie'];
			$iduser=$_SESSION['iduser'];
			$erreur=null;
			$liste=new liste('','','');
			$nom_partie=($liste->nomtable($idpartie));
			$listetemplate=$liste->listetemplate($iduser);
			$listetemplate_complete="";
			foreach($listetemplate as $x => $x_value) {
			    $id_template=$x;
			    $name_template=$x_value;
			    require('views/viewListetemplate.php');
			}
			require ('views/viewModifpartie.php');
			$data=$modifpartie.$_SESSION['listepartie'];
			$this->_view=new View();
			$this->_view->generate('liste',null,$data);//view,erreur,données
		}else {
			$this->_view=new View();
			$this->_view->generate('liste',null,$_SESSION['listepartie']);//view,erreur,données	
		}
	}
}
