-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 14 juil. 2020 à 16:18
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `arietes`
--

-- --------------------------------------------------------

--
-- Structure de la table `claque`
--

CREATE TABLE `claque` (
  `id` int(11) NOT NULL,
  `id_partie` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idtableau` int(11) NOT NULL,
  `src` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `claque` int(1) NOT NULL,
  `x` int(4) NOT NULL,
  `y` int(4) NOT NULL,
  `angle` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `doc`
--

CREATE TABLE `doc` (
  `id` int(11) NOT NULL,
  `idpartie` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `nomdoc` text COLLATE utf8_unicode_ci NOT NULL,
  `contenu` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `contenu_mj` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doc`
--

INSERT INTO `doc` (`id`, `idpartie`, `iduser`, `nomdoc`, `contenu`, `contenu_mj`, `visible`) VALUES
(1, 1, 1, 'Theod 2 ', 'Super fort', 'spycopate', ''),
(2, 1, 2, 'Lettre de salveri', 'tu es un homme mort', NULL, '');

-- --------------------------------------------------------

--
-- Structure de la table `fiche_block`
--

CREATE TABLE `fiche_block` (
  `id` int(11) NOT NULL,
  `id_table` int(11) NOT NULL,
  `id_block` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nom_comp` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `valeur1` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur2` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur3` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur4` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur5` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur6` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur7` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur8` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur9` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valeur10` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `fiche_block`
--

INSERT INTO `fiche_block` (`id`, `id_table`, `id_block`, `id_user`, `nom_comp`, `valeur1`, `valeur2`, `valeur3`, `valeur4`, `valeur5`, `valeur6`, `valeur7`, `valeur8`, `valeur9`, `valeur10`) VALUES
(827, 1, 4, 1, 'key ', 'https://parismatch.be/app/uploads/2018/04/Macaca_nigra_self-portrait_large-e1524567086123-1100x715.jpg', '#4a90e2', '#ffffff', '2', NULL, '0', '1', '7', NULL, NULL),
(828, 1, 5, 1, 'PSY', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(829, 1, 5, 1, 'PV', '30', '29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(896, 1, 7, 1, 'Etat', 'malade', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(919, 1, 19, 1, 'module_enco', '19', '32', '', '', '', '', '', '', '', ''),
(920, 1, 14, 1, 'module_text', 'Je suis alauch', '', '', '', '', '', '', '', '', ''),
(921, 1, 15, 1, 'module_text', '75', '35', '60', '', '', '', '', '', '', ''),
(922, 1, 34, 1, 'module_text', '0', '0', '0', '0', '0', '0', '', '', '', ''),
(923, 1, 16, 1, 'module_text', 'Combat', '', '5', '', '', '', '', '', '', ''),
(924, 1, 16, 1, 'module_text', 'DiscrÃ©tion', '', '-10', '', '', '', '', '', '', ''),
(925, 1, 16, 1, 'module_text', 'IntimidÃ©', '', '10', '', '', '', '', '', '', ''),
(926, 1, 16, 1, 'module_text', 'Pister', '', '5', '', '', '', '', '', '', ''),
(927, 1, 16, 1, 'module_text', 'Resitance Ã  la douleur', '', '5', '', '', '', '', '', '', ''),
(928, 1, 17, 1, 'module_text', 'Level', '', '1', '', '', '', '', '', '', ''),
(929, 1, 17, 1, 'module_text', 'Dragon', '', 'Race', '', '', '', '', '', '', ''),
(930, 1, 17, 1, 'module_text', 'Corbeau', '', 'Clan', '', '', '', '', '', '', ''),
(931, 1, 17, 1, 'module_text', 'Aucune', '', 'Culte', '', '', '', '', '', '', ''),
(932, 1, 21, 1, 'module_text', 'Honneur du guerrier', '', 'Savoir', '', '', '', '', '', '', ''),
(933, 1, 21, 1, 'module_text', 'AgilitÃ© sans armure', '', 'Talent', '', '', '', '', '', '', ''),
(934, 1, 22, 1, 'module_text', 'Repos du guerrier', 'rÃ©cupÃ¨re 1 PV par par heure. Cible : Sur soi pour Passif', 'offert', '', '', '', '', '', '', ''),
(935, 1, 22, 1, 'module_text', 'Duel', 'DÃ©fit une cible dans un combat Ã   mort,aucune aide extÃ©rieur ne peut Ãªtre apportÃ© aux participants de ce duel(jet de social rÃ©ussit). Cible : Sur soi et cible pour Persistant', '0', '', '', '', '', '', '', ''),
(936, 1, 22, 1, 'module_text', 'Duel', 'DÃ©fit une cible dans un combat Ã   mort,aucune aide extÃ©rieur ne peut Ãªtre apportÃ© aux participants de ce duel(jet de social rÃ©ussit). Cible : Sur soi et cible pour Persistant', '0', '', '', '', '', '', '', ''),
(937, 1, 18, 1, 'module_text', 'hache de bataille', '', '10', '1', '', '', '', '', '', ''),
(938, 1, 20, 1, 'module_text', '4', '', '1', '4', '1', '', '', '', '', ''),
(939, 1, 20, 1, 'module_text', '4', '', '0', '0', '', '', '', '', '', ''),
(940, 1, 20, 1, 'module_text', 'Coffre', '', '1', '5', '', '', '', '', '', ''),
(941, 1, 5, 2, 'PV', '4', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(942, 1, 5, 2, 'PSY', '3', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(943, 1, 4, 2, 'ultimate', NULL, '#4a90e2', '#ffffff', '2', NULL, NULL, NULL, NULL, NULL, NULL),
(944, 1, 19, 2, 'module_enco', '0', '0', '', '', '', '', '', '', '', ''),
(945, 1, 14, 2, 'module_text', '', '', '', '', '', '', '', '', '', ''),
(946, 1, 15, 2, 'module_text', '0', '0', '0', '', '', '', '', '', '', ''),
(947, 1, 34, 2, 'module_text', '0', '0', '0', '0', '0', '0', '', '', '', ''),
(948, 1, 4, 3, 'test', NULL, '#4a90e2', '#ffffff', '3', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `groupe_liste`
--

CREATE TABLE `groupe_liste` (
  `id` int(11) NOT NULL,
  `nom_groupe` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `groupe_liste`
--

INSERT INTO `groupe_liste` (`id`, `nom_groupe`, `id_user`) VALUES
(12, 'Liber', 1),
(18, 'test', 1);

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE `liste` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `template` int(11) NOT NULL,
  `mj` int(11) NOT NULL,
  `color1` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `liste`
--

INSERT INTO `liste` (`id`, `name`, `template`, `mj`, `color1`, `color2`) VALUES
(1, 'Liber aventure 1', 1, 1, '#778899', '#ffffff'),
(2, 'Liber aventure 2', 3, 2, '#A52A2A', '#FFEBCD'),
(4, 'test2', 4, 1, NULL, NULL),
(5, 'test', 5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `musique`
--

CREATE TABLE `musique` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `dossier` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sousdossier` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nommusique` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `musique`
--

INSERT INTO `musique` (`id`, `iduser`, `dossier`, `sousdossier`, `nommusique`, `url`, `scr`) VALUES
(14, 1, '', '', 'a', 'ICySBT2YIiM', 'youtube'),
(15, 1, '', '', 'b', 'XhieJB1dPio', 'youtube');

-- --------------------------------------------------------

--
-- Structure de la table `plateau`
--

CREATE TABLE `plateau` (
  `id` int(11) NOT NULL,
  `joueur` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `contenu` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_table` int(11) NOT NULL,
  `img` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `modif` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `plateau`
--

INSERT INTO `plateau` (`id`, `joueur`, `nom`, `contenu`, `id_table`, `img`, `modif`) VALUES
(1, '0', 'test', '{\"version\":\"2.4.5\",\"objects\":[{\"type\":\"image\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":789,\"top\":278,\"width\":298,\"height\":333,\"fill\":\"rgb(0,0,0)\",\"stroke\":null,\"strokeWidth\":0,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":0.5,\"scaleY\":0.5,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"calque\":\"pion\",\"droit\":0,\"crossOrigin\":\"\",\"cropX\":0,\"cropY\":0,\"src\":\"https://localhost/bibliotheque/effet/Nebula-a.png\",\"filters\":[]},{\"type\":\"image\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":1014,\"top\":250,\"width\":584,\"height\":580,\"fill\":\"rgb(0,0,0)\",\"stroke\":null,\"strokeWidth\":0,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":0.5,\"scaleY\":0.5,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"calque\":\"pion\",\"droit\":0,\"crossOrigin\":\"\",\"cropX\":0,\"cropY\":0,\"src\":\"https://localhost/bibliotheque/effet/explosion.png\",\"filters\":[]}]}', 1, 'j1p1-1', 1),
(2, '', 'ali', '{\"version\":\"2.4.5\",\"objects\":[{\"type\":\"image\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":500,\"top\":100,\"width\":399,\"height\":400,\"fill\":\"rgb(0,0,0)\",\"stroke\":null,\"strokeWidth\":0,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":0.5,\"scaleY\":0.5,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"crossOrigin\":\"\",\"cropX\":0,\"cropY\":0,\"src\":\"http://localhost/bibliotheque/token/46.png\",\"filters\":[]},{\"type\":\"image\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":1274.18,\"top\":290.48,\"width\":512,\"height\":512,\"fill\":\"rgb(0,0,0)\",\"stroke\":null,\"strokeWidth\":0,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":0.5,\"scaleY\":0.5,\"angle\":104.63,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"crossOrigin\":\"\",\"cropX\":0,\"cropY\":0,\"src\":\"http://localhost/bibliotheque/token/33.png\",\"filters\":[]},{\"type\":\"image\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":578.77,\"top\":383.85,\"width\":206,\"height\":245,\"fill\":\"rgb(0,0,0)\",\"stroke\":null,\"strokeWidth\":0,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":0.5,\"scaleY\":0.5,\"angle\":103,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"crossOrigin\":\"\",\"cropX\":0,\"cropY\":0,\"src\":\"http://localhost/bibliotheque/token/70.png\",\"filters\":[]}]}', 1, 'j1p1-2', 1),
(4, '', 'test3', '{\"version\":\"2.4.5\",\"objects\":[{\"type\":\"circle\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":895,\"top\":285,\"width\":100,\"height\":100,\"fill\":\"#005e7a\",\"stroke\":null,\"strokeWidth\":1,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":1,\"scaleY\":1,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"radius\":50,\"startAngle\":0,\"endAngle\":6.283185307179586},{\"type\":\"triangle\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":500,\"top\":100,\"width\":70,\"height\":70,\"fill\":\"#005e7a\",\"stroke\":null,\"strokeWidth\":1,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":1,\"scaleY\":1,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0},{\"type\":\"rect\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":857,\"top\":134,\"width\":100,\"height\":50,\"fill\":\"#215833\",\"stroke\":null,\"strokeWidth\":1,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":1,\"scaleY\":1,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"rx\":0,\"ry\":0}]}', 1, 'j1p1-3', NULL),
(5, '', 'test2', '{\"version\":\"2.4.5\",\"objects\":[{\"type\":\"image\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":845,\"top\":383,\"width\":311,\"height\":304,\"fill\":\"rgb(0,0,0)\",\"stroke\":null,\"strokeWidth\":0,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":0.5,\"scaleY\":0.5,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"crossOrigin\":\"\",\"cropX\":0,\"cropY\":0,\"src\":\"http://localhost/bibliotheque/objet/3.png\",\"filters\":[]},{\"type\":\"image\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":1068,\"top\":267,\"width\":229,\"height\":142,\"fill\":\"rgb(0,0,0)\",\"stroke\":null,\"strokeWidth\":0,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":0.5,\"scaleY\":0.5,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"crossOrigin\":\"\",\"cropX\":0,\"cropY\":0,\"src\":\"http://localhost/bibliotheque/objet/24.png\",\"filters\":[]},{\"type\":\"image\",\"version\":\"2.4.5\",\"originX\":\"left\",\"originY\":\"top\",\"left\":605,\"top\":319,\"width\":280,\"height\":280,\"fill\":\"rgb(0,0,0)\",\"stroke\":null,\"strokeWidth\":0,\"strokeDashArray\":null,\"strokeLineCap\":\"butt\",\"strokeDashOffset\":0,\"strokeLineJoin\":\"miter\",\"strokeMiterLimit\":4,\"scaleX\":0.5,\"scaleY\":0.5,\"angle\":0,\"flipX\":false,\"flipY\":false,\"opacity\":1,\"shadow\":null,\"visible\":true,\"clipTo\":null,\"backgroundColor\":\"\",\"fillRule\":\"nonzero\",\"paintFirst\":\"fill\",\"globalCompositeOperation\":\"source-over\",\"transformMatrix\":null,\"skewX\":0,\"skewY\":0,\"crossOrigin\":\"\",\"cropX\":0,\"cropY\":0,\"src\":\"http://localhost/bibliotheque/objet/29.png\",\"filters\":[]}]}', 1, 'j1p1-4', NULL),
(7, '', 'Nouveau', '{\"version\":\"2.4.5\",\"objects\":[]}', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `playlist`
--

CREATE TABLE `playlist` (
  `id` int(11) NOT NULL,
  `idpartie` int(11) NOT NULL,
  `idmusic` int(11) NOT NULL,
  `lecture_en_cours` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `playlist`
--

INSERT INTO `playlist` (`id`, `idpartie`, `idmusic`, `lecture_en_cours`, `visible`) VALUES
(12, 1, 14, NULL, 1),
(13, 1, 15, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pnj`
--

CREATE TABLE `pnj` (
  `id` int(11) NOT NULL,
  `idpartie` int(11) NOT NULL,
  `nompnj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) DEFAULT NULL,
  `histoire` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `notemj` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `pnj`
--

INSERT INTO `pnj` (`id`, `idpartie`, `nompnj`, `image`, `stat`, `histoire`, `notemj`, `visible`) VALUES
(1, 1, 'Theod 2', 'img/image.jpg', NULL, 'Super ', 'spycopate', '3'),
(4, 1, 'Nouveau PNJ', NULL, NULL, NULL, NULL, '2');

-- --------------------------------------------------------

--
-- Structure de la table `tables`
--

CREATE TABLE `tables` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_liste` int(11) NOT NULL,
  `visible` int(11) NOT NULL,
  `spetacteur` int(11) NOT NULL,
  `groupe` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `tables`
--

INSERT INTO `tables` (`id`, `id_user`, `id_liste`, `visible`, `spetacteur`, `groupe`) VALUES
(1, 1, 1, 1, 0, 12),
(4, 1, 4, 1, 0, 4),
(9, 2, 1, 1, 0, NULL),
(10, 1, 2, 1, 0, 12),
(11, 3, 1, 1, 0, NULL),
(12, 1, 5, 0, 0, 18),
(13, 2, 4, 1, 0, 4);

-- --------------------------------------------------------

--
-- Structure de la table `tchat`
--

CREATE TABLE `tchat` (
  `id_tchat` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_partie` int(11) NOT NULL,
  `id_exp` int(11) NOT NULL,
  `id_dest` int(11) NOT NULL COMMENT '0 si pas mp. sinon id du destinataire',
  `type` int(11) NOT NULL COMMENT '1 message 2 lancer 3 annonce green 4 annonce crimson',
  `text_tchat` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `tchat`
--

INSERT INTO `tchat` (`id_tchat`, `date`, `id_partie`, `id_exp`, `id_dest`, `type`, `text_tchat`) VALUES
(887, '2019-04-04 13:53:56', 1, 1, 0, 4, '11'),
(888, '2019-04-04 13:54:03', 1, 1, 0, 4, '60'),
(889, '2019-04-04 13:54:16', 1, 1, 0, 4, '39'),
(890, '2019-04-04 13:54:20', 1, 1, 0, 4, '89'),
(891, '2019-04-04 13:54:26', 1, 1, 0, 4, '47'),
(892, '2019-04-04 13:56:38', 1, 1, 0, 4, '54'),
(893, '2019-04-04 13:56:45', 1, 1, 0, 4, '88'),
(894, '2019-04-04 13:57:01', 1, 1, 0, 6, '3'),
(895, '2019-04-04 14:11:40', 1, 1, 0, 4, '31'),
(896, '2019-04-18 09:42:00', 1, 1, 0, 4, '55'),
(897, '2019-04-18 09:43:16', 1, 1, 2, 1, 'ultimate : ldslkdl'),
(898, '2019-04-18 09:46:20', 1, 1, 1, 6, '4'),
(899, '2019-04-18 09:46:44', 1, 1, 0, 2, 'key  lance : /r 1d4 : 2'),
(900, '2019-05-17 13:27:21', 1, 1, 0, 4, '44'),
(901, '2019-05-17 13:59:45', 1, 1, 0, 4, '19'),
(902, '2019-05-22 09:34:03', 1, 1, 0, 4, '75'),
(903, '2019-05-22 09:34:41', 1, 1, 0, 4, '8'),
(904, '2019-05-22 09:34:55', 1, 1, 0, 4, '31'),
(905, '2019-05-22 09:35:07', 1, 1, 0, 4, '38'),
(906, '2019-05-22 09:36:01', 1, 1, 0, 4, '42'),
(907, '2019-05-22 09:36:19', 1, 1, 0, 4, '6'),
(908, '2019-05-22 09:38:23', 1, 1, 0, 4, '52'),
(909, '2019-05-22 09:39:13', 1, 1, 0, 4, '81'),
(910, '2019-05-22 09:39:38', 1, 1, 0, 4, '61'),
(911, '2019-05-22 09:42:06', 1, 1, 0, 4, '6'),
(912, '2019-05-22 09:42:18', 1, 1, 0, 4, '7'),
(913, '2019-05-23 08:03:53', 1, 1, 0, 4, '20'),
(914, '2019-05-23 08:53:09', 1, 1, 0, 4, '72'),
(915, '2019-06-21 07:54:40', 1, 1, 0, 4, '37'),
(916, '2019-06-21 07:58:47', 1, 1, 0, 4, '40'),
(917, '2020-07-01 08:45:22', 1, 1, 0, 4, '49'),
(918, '2020-07-01 09:08:30', 1, 1, 0, 4, '76'),
(919, '2020-07-03 11:47:06', 1, 1, 0, 4, '90'),
(920, '2020-07-03 11:47:26', 1, 1, 0, 1, 'salut'),
(921, '2020-07-08 16:06:45', 1, 1, 0, 4, '33'),
(922, '2020-07-08 16:06:52', 1, 1, 0, 4, '67'),
(923, '2020-07-13 15:45:38', 1, 1, 0, 4, '45'),
(924, '2020-07-13 15:46:05', 1, 1, 0, 4, '57'),
(925, '2020-07-13 15:48:01', 1, 1, 0, 4, '74'),
(926, '2020-07-14 09:50:31', 1, 1, 0, 4, '63'),
(927, '2020-07-14 09:50:33', 1, 1, 0, 4, '85'),
(928, '2020-07-14 09:50:37', 1, 1, 0, 4, '80'),
(929, '2020-07-14 09:51:29', 1, 1, 0, 4, '53'),
(930, '2020-07-14 09:51:31', 1, 1, 0, 4, '77'),
(931, '2020-07-14 10:08:57', 1, 1, 0, 4, '6'),
(932, '2020-07-14 10:09:04', 1, 1, 0, 4, '42'),
(933, '2020-07-14 10:09:05', 1, 1, 0, 4, '60'),
(934, '2020-07-14 10:09:08', 1, 1, 0, 4, '28'),
(935, '2020-07-14 10:09:15', 1, 1, 0, 4, '86'),
(936, '2020-07-14 10:21:22', 1, 1, 0, 4, '60'),
(937, '2020-07-14 10:22:10', 1, 1, 0, 4, '41'),
(938, '2020-07-14 10:35:15', 1, 1, 0, 4, '79');

-- --------------------------------------------------------

--
-- Structure de la table `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `template`
--

INSERT INTO `template` (`id`, `name`, `id_user`) VALUES
(1, 'Liber aventure', 0),
(3, 'Liber SF', 1),
(4, 'landa', 1),
(5, 'aaa', 1);

-- --------------------------------------------------------

--
-- Structure de la table `template_block`
--

CREATE TABLE `template_block` (
  `id` int(11) NOT NULL,
  `id_template` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `block` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `valeur` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `template_block`
--

INSERT INTO `template_block` (`id`, `id_template`, `name`, `block`, `type`, `valeur`) VALUES
(1, 1, 'Point de destin', 1, 1, '3'),
(2, 1, 'module_des', 2, 2, '1d100 + 1d10|5|96|100|10|0'),
(3, 1, 'Module posture', 3, 3, NULL),
(6, 1, 'module_avatar', 4, 4, NULL),
(7, 1, 'module_bouclier', 4, 5, NULL),
(8, 1, 'module_bouclier_sec', 4, 6, NULL),
(9, 1, 'PV', 5, 7, '#4CAF50'),
(10, 1, 'PSY', 5, 7, '#2196F3'),
(11, 1, 'module_tchat', 6, 8, NULL),
(12, 1, 'module_note', 7, 9, 'Scénario|Partie précédente |Note'),
(13, 1, 'module_fiche', 8, 10, NULL),
(14, 1, 'Histoire', 10, 12, NULL),
(15, 1, 'Statistique', 11, 11, 'Phy|Soc|Men'),
(16, 1, 'Competence', 20, 13, NULL),
(17, 1, 'Carateristique', 21, 16, NULL),
(18, 1, 'Arme', 21, 14, NULL),
(19, 1, 'Encombrement', 30, 15, NULL),
(20, 1, 'Inventaire', 31, 14, NULL),
(21, 1, 'Connaissance', 32, 16, NULL),
(22, 1, 'Dons', 40, 16, NULL),
(23, 1, 'Posture', 41, 17, NULL),
(24, 1, 'Avantage/desavantage', 42, 18, NULL),
(25, 1, 'Etat', 43, 19, NULL),
(26, 1, 'Editeur', 44, 20, NULL),
(29, 1, 'Règle du jeu', 50, 50, 'Le MJ a toujours raison, TGCM<br>\r\nLe Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.\r\n'),
(30, 1, 'module_doc', 51, 51, NULL),
(31, 1, 'module_pnj', 52, 52, NULL),
(32, 1, 'module_music', 53, 53, NULL),
(34, 1, 'Sous-statistique', 12, 21, 'For|Agi|Sag|Cha|Ast|Mem'),
(149, 4, 'module_des', 2, 2, '1d100+1d10|5|96|100||'),
(150, 4, 'module_avatar', 4, 4, ''),
(151, 4, 'PSY', 5, 7, '#0080c0'),
(152, 4, 'END', 5, 7, '#400040'),
(153, 4, 'module_tchat', 6, 8, ''),
(154, 4, 'PV', 5, 7, '#ff0000'),
(155, 4, 'module_note', 7, 9, '||'),
(156, 4, 'module_fiche', 8, 10, ''),
(157, 4, 'histoire', 10, 12, ''),
(158, 4, 'CompÃ©tence', 20, 16, ''),
(159, 4, 'Evenement', 30, 16, ''),
(160, 4, 'Inventaire', 40, 12, ''),
(161, 4, 'RÃ¨gle du jeu', 50, 50, ''),
(162, 4, 'module_doc', 51, 51, ''),
(163, 4, 'module_pnj', 52, 52, ''),
(164, 4, 'module_music', 53, 53, ''),
(217, 3, 'module_des', 2, 2, '1d100+1d10|||100||'),
(218, 3, 'module_avatar', 4, 4, ''),
(219, 3, 'PV', 5, 7, '#80ff00'),
(220, 3, 'AR', 5, 7, '#ff8080'),
(221, 3, 'module_tchat', 6, 8, ''),
(222, 3, 'CF', 5, 7, '#000000'),
(223, 3, 'module_note', 7, 9, '||'),
(224, 3, 'module_fiche', 8, 10, ''),
(225, 3, '', 11, 11, 'FOR|CON|DEX|INT|VOL|CHA'),
(226, 3, 'Histoire', 10, 12, ''),
(227, 3, 'CompÃ©tence', 20, 13, ''),
(228, 3, 'Arme', 31, 14, ''),
(229, 3, 'Invetaire', 32, 14, ''),
(230, 3, '', 42, 18, ''),
(231, 3, 'RÃ¨gle du jeu', 50, 50, ''),
(232, 3, 'module_doc', 51, 51, ''),
(233, 3, 'module_pnj', 52, 52, ''),
(234, 3, 'module_music', 53, 53, '');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mdp` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `name`, `mdp`, `email`) VALUES
(1, 'vharung', '9fcfea32bd28b16659d94bdc07f66a9b', 'contact@alexandre-crochet.fr'),
(2, 'ultimate', '9c8d9e1d80fc923ca7d532e46a3d606f', 'ulti@gmail.com'),
(3, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test@test.fr'),
(4, 'cleprez', '54875129380f7f9abe1c0698a7a94baa', 'cleprez@gmail.com');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `claque`
--
ALTER TABLE `claque`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `doc`
--
ALTER TABLE `doc`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fiche_block`
--
ALTER TABLE `fiche_block`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `groupe_liste`
--
ALTER TABLE `groupe_liste`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `liste`
--
ALTER TABLE `liste`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `musique`
--
ALTER TABLE `musique`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `plateau`
--
ALTER TABLE `plateau`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pnj`
--
ALTER TABLE `pnj`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tchat`
--
ALTER TABLE `tchat`
  ADD PRIMARY KEY (`id_tchat`);

--
-- Index pour la table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `template_block`
--
ALTER TABLE `template_block`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `claque`
--
ALTER TABLE `claque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `doc`
--
ALTER TABLE `doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `fiche_block`
--
ALTER TABLE `fiche_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=949;

--
-- AUTO_INCREMENT pour la table `groupe_liste`
--
ALTER TABLE `groupe_liste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `liste`
--
ALTER TABLE `liste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `musique`
--
ALTER TABLE `musique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `plateau`
--
ALTER TABLE `plateau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `pnj`
--
ALTER TABLE `pnj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tables`
--
ALTER TABLE `tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `tchat`
--
ALTER TABLE `tchat`
  MODIFY `id_tchat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=939;

--
-- AUTO_INCREMENT pour la table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `template_block`
--
ALTER TABLE `template_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
